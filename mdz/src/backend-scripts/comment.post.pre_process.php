<?php
/**
 * Created by PhpStorm.
 * User: hardtmedia
 * Date: 10.08.17
 * Time: 12:55
 */

/**
 *
 * Debugging?
 * Remember to activate the "Allow script to modify response payload".
 * Use https://marktplatz-sg.de:444/test_rest.html to debug.
 *
 *
 * */

$comment = $event['request']['payload']['resource'][0];
$text = $comment['text'];
$initiativeID = (int)$comment['initiative_id'];
$username = $comment['username'];

// link to show the inititaive
$link = 'href="https://www.marktplatz-sg.de/#/show/' . $initiativeID . '"';

// get the initiative title
$result = $platform['api']->get->__invoke("mysql/_table/initiative/$initiativeID");
$inititaiveTitle = $result['content']['title'];

// get the current session
$result = $platform['api']->get->__invoke('user/session');
$currentUserId = $result['content']['id'];

// check if this comment is an answer to a previous comment.
// If so, notify the original commenter about the anwser.
if (!is_null($comment['comment_id'])) {

    // get the original comment
    $parentCommentId = $comment['comment_id'];
    $result = $platform['api']->get->__invoke("mysql/_table/comment/$parentCommentId");
    $parentText = $result['content']['text'];

    // extract data to access the original commenter (user)
    $userId = $result['content']['user_id'];

    // If the commenter and the author of the original comment are the same, stop right there!
    // But if they are different, then continue.
    if ($currentUserId !== $userId) {

        $userURL = "system/user/$userId";

        // check if the user wants to receive a notification for the answer
        $notificationResource = "mysql/_table/notification_settings?filter=(user_id%3D$userId)and(initiative_id%3D$initiativeID)";
        $result = $platform['api']->get->__invoke($notificationResource);

        $resource = $result['content']['resource'];

        // stop notification, when the user does not want the notification.
        $wantsAnswerNotifications = $resource[0]['answers_on_comments'];
        if ($wantsAnswerNotifications) {

            // proceed, the user wants the notification
            // do the answer-notification

            // get the original commenters user data
            $result = $platform['api']->get->__invoke($userURL);

            $userData = $result['content'];
            $e['answer_user_data'] = $userData;
            $email = $userData['email'];
            $name = $userData['first_name'] . " " . $userData['last_name'];

            // send the notification
            $payload = [
                "template" => "new_comment_answer",
                "to" => [
                    [
                        "name" => $name,
                        "email" => $email
                    ]
                ],
                "recipient_name" => $name,
                "commenter_name" => $username,
                "initiativeName" => $inititaiveTitle,
                "comment" => $text,
                "original_comment" => $parentText,
                "link" => $link,
                "subject" => "$username hat auf deinen Kommentar geantwortet."


            ];
            $platform['api']->post->__invoke('mdzmailer', $payload);
        }
    }
}

// get the relevant notification settings
$notificationResource = "mysql/_table/notification_settings?filter=initiative_id%3D$initiativeID";
$result = $platform['api']->get->__invoke($notificationResource);

$resource = $result['content']['resource'];

foreach ($resource as $settings) {

    if ($settings['new_comments'] === true) {

        // don't notify the commenting user himself
        $userID = $settings['user_id'];
        if ($userID == $currentUserId) {
            continue;
        }

        // GET the email address
        $userURL = "system/user/$userID";
        $result = $platform['api']->get->__invoke($userURL);
        $userData = $result['content'];

        $email = $userData['email'];
        $name = $userData['first_name'] . " " . $userData['last_name'];

        // send the email
        $payload = [
            "template" => "new_comment",
            "to" => [
                [
                    "name" => $name,
                    "email" => $email
                ]
            ],
            "recipient_name" => $name,
            "commenter_name" => $username,
            "initiativeName" => $inititaiveTitle,
            "comment" => $text,
            "link" => $link,
            "subject" => "$username hat einen neuen Kommentar hinterlassen."


        ];
        $platform['api']->post->__invoke('mdzmailer', $payload);

    }
}