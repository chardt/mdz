<?php

$e = [];

$initiative = $event['request']['payload']['resource'][0];
$initiativeId = $initiative['id'];

$e['initiative_patch'] = $initiative;


// get current users name
$userURL = 'user/session';
$result = $platform['api']->get->__invoke($userURL);
$e['user'] = $result;

$userData = $result['content'];
$username = $userData['username'];// . " " . $userData['last_name'];

// get the name of the initiative
$initiativeURL = "mysql/_table/initiative/$initiativeId";
$result = $platform['api']->get->__invoke($initiativeURL);

$initiativeName = $result['content']['title'];
$e['initiative'] = $result;

// get the user settings
$notificationResource = "mysql/_table/notification_settings?filter=initiative_id%3D$initiativeId";
$result = $platform['api']->get->__invoke($notificationResource);

$resource = $result['content']['resource'];

$inititaiveLink = 'href="https://www.marktplatz-sg.de/#/show/'.$initiativeId.'""';

$mails = [];

foreach ($resource as $settings) {
    if ($settings['content_changes'] === true) {
        // GET the email address
        $userID = $settings['user_id'];
        if ($userID == $currentUserId) {
            continue;
        }

        $userURL = "system/user/$userID";
        $result = $platform['api']->get->__invoke($userURL);
        $userData = $result['content'];

        $email = $userData['email'];
        $name = $userData['first_name'] . " " . $userData['last_name'];
        // send the email
        $payload = [
            "to" => [
                [
                    "name" => $name,
                    "email" => "christopher@fly-hardt.de"//$email
                ]
            ],
            "template" => "content_changed",
            "link" => $inititaiveLink,
            "recipient_username" => $name,
            "username" => $name,
            "subject" => "$username hat den Inhalt der Initiative \"$initiativeName\" geändert.",
        ];

        array_push($mails, $payload);

//        $platform['api']->post->__invoke('mdzmailer', $payload);
    }
}

$e['mails'] = $mails;
$event['response']['content'] = $e;
return $event['response'];