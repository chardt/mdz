<?php
/**
 * Created by PhpStorm.
 * User: hardtmedia
 * Date: 08.11.17
 * Time: 13:52
 */

$verb = $event['request']['method'];

$payload = $event['request']['payload'];

$initiativeID = $payload['id'];
$emails = $payload['emails'];
$initiativeName = $payload['initiativeName'];
$inviterName = $payload['inviter'];
$message = $payload['message'];

$emailsArr = explode(',', $payload['emails']);

$validMails = [];
$invalidMails = [];

$inviteLink = 'href="https://www.marktplatz-sg.de/#/show/' . $initiativeID . '"';

if (sizeof($emailsArr) == 0) {
    return;
}

$alreadyInvitedMails = [];

foreach ($emailsArr as $mail) {

    // check if this email was already invited the the current initiative
    $test = 'mysql/_table/invitation?filter=(email%3D' . $mail . ')AND(initiative_id%3D' . $initiativeID . ')';
    $result = $platform['api']->get->__invoke($test);
    $resources = $result['content']['resource'];
    if (sizeof($resources) > 0) {
        array_push($alreadyInvitedMails, $mail);
        continue;
    }

    $mail = trim($mail);
    if (filter_var($mail, FILTER_VALIDATE_EMAIL)) {

        array_push($validMails, $mail);

        $response .= "Valid: " . $mail . "<br>";

        $payload = [
            "template" => "invite",
            "to" => [
                [
                    "name" => "nix",
                    "email" => $mail
                ]
            ],
            "link" => $inviteLink,
            "inviter" => $inviterName,
            "initiativeName" => $initiativeName,
            "msg" => $message,
            "debug" => $event
        ];
        $platform['api']->post->__invoke('mdzmailer', $payload);

        // store invitation in DB
        $invitation = [
            'resource' => [
                'initiative_id' => $initiativeID,
                'email' => $mail,
                'message' => $message
            ]
        ];
        $platform['api']->post->__invoke('mysql/_table/invitation', $invitation);


    } else {
        if (!empty($mail) > 0) {
            array_push($invalidMails, $mail);
        }
    }
}

// build the response, containing arrays for addresses that resulted in error and those that already were invited.
if (sizeof($invalidMails) > 0 || sizeof($alreadyInvitedMails) > 0) {
    $response = [
        "message" => "Einige E-Mails konnten nicht übertragen werden."
    ];

    if (sizeof($invalidMails) > 0) {
        $response['invalidMails'] = $invalidMails;
    }
    if (sizeof($alreadyInvitedMails) > 0) {
        $response['alreadyInvitedMails'] = $alreadyInvitedMails;
    }
    return json_encode($response);
}