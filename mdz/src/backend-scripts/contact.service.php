<?php
/**
 * Created by PhpStorm.
 * User: hardtmedia
 * Date: 08.11.17
 * Time: 13:52
 */

$payload = $event['request']['payload'];
$receiving_user = $payload['receiving_user'];
$message = $payload['message'];

// get receiver user email address
// get current users name
$userURL = 'user/session';
$result = $platform['api']->get->__invoke($userURL);
$result = $result['content'];
$sender_user = $userData['first_name'] . ' ' . $userData['last_name'];


// current user as other user, to get the username
$currentAsOtherURL = "system/user/".$result['id'];
$currentAsOtherResult = $platform['api']->get->__invoke($currentAsOtherURL);
$sender_username = $currentAsOtherResult['content']['username'];

// get email to send to
$otherUserURL = "system/user?filter=(username%3D$receiving_user)";
$otherUserResult = $platform['api']->get->__invoke($otherUserURL);
$otherUserData = $otherUserResult['content'];

$mail = $otherUserData['resource'][0]['email'];

// send message
$payload = [
    "template" => "contact_request",
    "to" => [
        [
            "name" => $receiving_user,
            "email" => $mail
        ]
    ],
    "sender_username" => $sender_username,
    "receiving_user" => $receiving_user,
    "sender_user" => $sender_user,
    "msg" => $message
];
return $mailresult = $platform['api']->post->__invoke('mdzmailer', $payload);
//return [
//    'mailresult' => $mailresult,
//    'config' => $userData,
//    'userurl' => $userURL,
//    'result' => $result,
//    '$currentAsOtherResult' => $currentAsOtherResult
//];

//return json_encode($payload);