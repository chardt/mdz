<?php
//ini_set('display_errors', 'On');
//error_reporting(E_ALL);

// Check if the visitor is facebook or human.
$facebook = false;
if (
    strpos($_SERVER["HTTP_USER_AGENT"], "facebookexternalhit/") !== false ||
    strpos($_SERVER["HTTP_USER_AGENT"], "Facebot") !== false
) {
    $facebook = true;
}

// It's a human, redirect to the human site.
if(!$facebook){
    $initiativeId = htmlspecialchars($_GET["id"]);
    header('Location: https://www.marktplatz-sg.de/#/show/'.$initiativeId);
    return;
}

$API_KEY = "31a7f8f2c2fdd5082811e22e02b58436e1e9b4b43330fd4397e388bda570f526";
$initiativeId = htmlspecialchars($_GET["id"]);

// set up the request with all relations
/** ATTENTION  - Do not enable https. Its not working and in this case it's okay.*/
$path = "http://www.marktplatz-sg.de:81/api/v2/mysql/_table/initiative/$initiativeId?";
$path .= "&related=ortsgemeinde_by_initiative_belongs_to_ortsgemeinde";
$path .= ",handlungsfeld_by_initiative_belongs_to_handlungsfeld";
$path .= ",initiative_has_image_by_initiative_id";

// GET the initiative data

//echo file_get_contents($path);
$ch = curl_init($path);
curl_setopt($ch, CURLOPT_HTTPGET, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'X-Dreamfactory-API-Key: '.$API_KEY
));

$response = curl_exec($ch);
$initiative = json_decode($response, true);
// close the cURL session
curl_close($ch);

// extract the hero image
$images = $initiative['initiative_has_image_by_initiative_id'];

$hero = null;
foreach($images as $image){
    if($image['is_hero'] == 1){
        $hero = $image['path'];
    }
}
echo var_export($hero, true);
//$imagePath = "https://mdz.mind-me.de:444/api/v2/files/$hero?api_key=$API_KEY";

// facebook won't recognize the image when it doesn't end with an image filetype.
// the image.php script will load the image (using the api_key) and returns just the image content.
// this can then be read by facebook.
$imagePath = "http://www.marktplatz-sg.de/static/image.php?i=$hero";

// strip html tags from the discription for the OG tag
$desc = htmlspecialchars(strip_tags($initiative['description']));
$title = $initiative['title'];
?>

<html>
<head>
<!--    <meta property="og:url" content="https://mdz.mind-me.de/#/show/--><?php //echo $initiativeId; ?><!--" />-->
    <meta property="og:image" content="<?php echo $imagePath; ?>" />
    <meta property="og:title" content="<?php echo $title; ?>" />
    <meta property="og:description" content="<?php echo $desc; ?>" />
</head>
<body>
<div class="images">
    <img src="<?php echo $imagePath; ?>" alt="">
</div>
<div class="description"><?php echo $initiative['description']; ?></div>
</body>
</html>
