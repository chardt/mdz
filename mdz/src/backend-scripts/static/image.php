<?php
//ini_set('display_errors', 'On');
//error_reporting(E_ALL);
$API_KEY = "31a7f8f2c2fdd5082811e22e02b58436e1e9b4b43330fd4397e388bda570f526";

$image = htmlspecialchars($_GET['i']);

/** ATTENTION  - Do not enable https. Its not working and in this case it's okay.*/
$imagePath = "http://www.marktplatz-sg.de:81/api/v2/files/$image";


$ch = curl_init($imagePath);
curl_setopt($ch, CURLOPT_HTTPGET, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'X-Dreamfactory-API-Key: '.$API_KEY
));

$response = curl_exec($ch);
curl_close($ch);

echo $response;