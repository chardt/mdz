<?php
/**
 * Uncomment the $e lines and the $mails lines to debug. Use REST debugger on https://marktplatz-sg.de:444/test_rest.html.
 */
$support = $event['request']['payload']['resource'][0];

//$e = [];

// get all settings for this initiative
$initiativeId = $support['initiative_id'];
$userId = $support['user_id'];

//$e['ini_id'] = $initiativeId;
//$e['user_id'] = $userId;

// get the new supporters information
$userURL = "system/user/$userId";
//$e['user_url'] = $userURL;

$result = $platform['api']->get->__invoke($userURL);
//$e['user_result'] = $result;

$userData = $result['content'];

$new_supporters_name = $userData['username'];//$userData['first_name']." ".$userData['last_name'];

$new_supporters_email = $userData['email']; // for later
// get the name of the initiative
$initiativeURL = "mysql/_table/initiative/$initiativeId";
//$e['initiative_url'] = $initiativeURL;

$result = $platform['api']->get->__invoke($initiativeURL);
//$e['initiative_result'] = $result;

$initiativeName = $result['content']['title'];

$notificationResource = "mysql/_table/notification_settings?filter=initiative_id%3D$initiativeId";
//$e['notification_resource'] = $notificationResource;

$result = $platform['api']->get->__invoke($notificationResource);
//$e['notification_resource_result'] = $result;

$resource = $result['content']['resource'];

//$mails = [];
$inititaiveLink = 'href="https://www.marktplatz-sg.de/#/show/'.$initiativeId.'""';
foreach($resource as $settings){
    // check if the user, whose settings those are, wants to know about the new supporter
    if($settings['supporter_joined']){
        // get the recipients data
        $recipient_userId = $settings['user_id'];

        // prevent from getting the notification myself
        if($recipient_userId === $userId){
            continue;
        }

        $userURL = "system/user/$recipient_userId";

        $result = $platform['api']->get->__invoke($userURL);

        $userData = $result['content'];

        $email = $userData['email'];
        $name = $userData['first_name']." ".$userData['last_name'];

        $check = true;
        if(empty($name) || sizeof($name) == 0){
            $name = "NAME NOT FOUND";
            $check = false;
        }

        if(empty($email) || sizeof($email) == 0){
            $email = "EMAIL NOT FOUND";
            $check = false;
        }

        if(empty($initiativeName) || sizeof($initiativeName) == 0){
            $initiativeName = "INITIATIVE NAME NOT FOUND";
            $check = false;
        }

        if($check){
            // send the notification
            $payload = [
                "to" => [
                    [
                        "name" => $name,
                        "email" =>$email
                    ]
                ],
                "template" => "new_supporter",
                "link" => $inititaiveLink,
                "initiativeName" => $initiativeName,
                "recipient_username" => $name,
                "new_supporter" => $new_supporters_name,
                "subject" => "$new_supporters_name unterstützt nun \"$initiativeName\".",
                "from_name" => "Marktplatz der Ideen",
                "from_email" => "system@marktplatz-sg.de"
            ];

            $platform['api']->post->__invoke('mdzmailer', $payload);

//            $d = [
//                'recipient_user_id' => $recipient_userId,
//                'user_url' => $userURL,
//                'user_data' => $userData,
//                'mail_config' => $payload
//            ];
//            array_push($mails, $d);
        }
    }
}


// close all open invitations for this user/initiative combination
$invitationURL = "mysql/_table/invitation?filter=((email%3D$new_supporters_email)and(initiative_id%3D$initiativeId))";
$invite_get_result = $platform['api']->get->__invoke($invitationURL);
$inviteId = $invite_get_result['content']['resource'][0]['id'];
$patch = [
    'open' => false
];

$invitationURL = "mysql/_table/invitation/$inviteId";
$result = $platform['api']->patch->__invoke($invitationURL, $patch);

//$responseContent = $event['response']['content'];
//$responseContent['debugInfo'] = [
//    'inviteUrl' => $invitationURL,
//    'invite_get_result' => $invite_get_result,
//    'patch_result' => $result,
//    'invite_id' => $inviteId,
//    'patch' => $patch
//];
//$e['mails'] = $mails;
//$event['response']['content'] = $responseContent;
//return $event['response'];