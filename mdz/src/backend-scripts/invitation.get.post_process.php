<?php
/**
 * Created by PhpStorm.
 * User: hardtmedia
 * Date: 13.12.17
 * Time: 13:06
 */

/**
 * Loops over the invitations and GETs the correlated initiative name.
 * The name is then put into the invitation object, so it's directly available to the client
 * without further traffic.
 */

$resource = $event['response']['content']['resource'];
$invitations = [];
foreach($resource as $invitation){
    $id = $invitation['initiative_id'];

    $inviteResource = "mysql/_table/initiative/$id";
    $result = $platform['api']->get->__invoke($inviteResource);

    $invitation['initiative_name'] = $result['content']['title'];
    array_push($invitations, $invitation);
}

$event['response']['content']['resource'] = $invitations;