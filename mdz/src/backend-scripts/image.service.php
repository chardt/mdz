<?php
/**
 * Created by PhpStorm.
 * User: hardtmedia
 * Date: 29.11.17
 * Time: 20:27
 */

$request = $event['request'];
$json = $request['content'];
$data = json_decode($json);

$resource = $data->resource[0];

$imageString = $resource->content;
$size = strlen(base64_decode($imageString));

$megabytes = $size / 1000000;

$response = $event['response'];

if ($megabytes > 2) {
    $response['status_code'] = 413;
    $response['content'] = "Das Bild ist zu groß. Bitte lade nur Bilder hoch, die kleiner als 2 MB sind.";
    return $response;
}

$target = $resource->target;
$new = [
    "resource" => [
        [
            "content" => $imageString,
            "is_base64" => true,
            "type" => "file",
            "name" => $resource->name
        ]
    ]
];

$api = $platform["api"];
$post = $api->post;
$url = 'files/initiatives';
return $post($url, json_encode($new));

//
//$payload = '{"resource":[{"content":"'.$imageString.'", "is_base64":true,"name":"test.png","type":"file"}]}';
//
//switch ($target) {
//    case 'initiative': {
//        return $platform['api']->post->__invoke('files/initiatives', $payload);
//    }
//        break;
//    case 'profile': {
//        return $platform['api']->post->__invoke('files/profiles/images', json_encode($new));
//    }
//        break;
//    default:{
//        $response['status_code'] = 404;
//        $response['content'] = "Kein Ziel angegeben".var_export($new, true);
//        return $response;
//    }
//}
