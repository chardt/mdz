<?php
/**
 * Created by PhpStorm.
 * User: hardtmedia
 * Date: 10.08.17
 * Time: 12:55
 */

/**
 *
 * Debugging?
 * Remember to activate the "Allow script to modify response payload".
 * Use https://marktplatz-sg.de:444/test_rest.html to debug.
 *
 *
 * */


$data = $event['request']['payload']['resource'][0];
$userId = $data['user_id'];
$initiativeId = $data['initiative_id'];

// get the current users name
$sessionUrl = 'user/session';
$currentUser = $platform['api']->get->__invoke($sessionUrl);
$username = $currentUser['content']['first_name'] . " " . $currentUser['content']['last_name'];
$currentUserId = $currentUser['content']['id'];

if($currentUserId === $userId){
    return;
}

// get (new admin) users name
$userURL = "system/user/$userId";
$result = $platform['api']->get->__invoke($userURL);

$userData = $result['content'];
$email = $userData['email'];
$recipient_name = $userData['first_name'] . " " . $userData['last_name'];

// get the initiative name
$initiativeUrl = 'mysql/_table/initiative/' . $initiativeId;
$result = $platform['api']->get->__invoke($initiativeUrl);
$initiativeName = $result['content']['title'];

// build initiative link
$link = 'href="https://www.marktplatz-sg.de/#/show/' . $initiativeId . '"';

// build message
$payload = [
    "template" => "new_admin",
    "to" => [
        [
            "name" => $recipient_name,
            "email" => $email
        ]
    ],
    "recipient_username" => $recipient_name,
    "initiative_name" => $initiativeName,
    "initiator_name" => $username,
    "link" => $link,
    "subject" => "$username hat dich zum Administrator von $initiativeName ernannt."
];

$platform['api']->post->__invoke('mdzmailer', $payload);

/**
 * Debugging data
 */
$e = [
    'request' => $event['request'],
    'user_url' => $userURL,
    'user_data' => $userData,
    'inititaive_url' => $initiativeUrl,
    'inititaive_data' => $result,
    'mail_request' => $payload
];
//$event['response']['content'] = $e;
// send message
