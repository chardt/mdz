import {Component, ViewContainerRef} from '@angular/core';
import {ToastsManager} from 'ng2-toastr/ng2-toastr';
import {OtherUsersService} from './user/other-users.service';
import {ProfileImageService} from './common/profile-image/profile-image.service';
import {SessionService} from './user/session/services/session.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [OtherUsersService, ProfileImageService]
})
export class AppComponent {
  constructor(public toastr: ToastsManager,
              vcr: ViewContainerRef,
              private sessionService: SessionService) {
    this.sessionService.recoverSession();
    this.toastr.setRootViewContainerRef(vcr);
  }
}
