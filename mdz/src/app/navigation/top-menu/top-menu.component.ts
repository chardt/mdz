import {Component, OnInit, OnDestroy, ViewChild, ElementRef} from '@angular/core';
import {SessionService} from '../../user/session/services/session.service';
import {User} from '../../user/model/user.model';
import {Subscription} from 'rxjs/Subscription';
import {RetinaService} from "../../common/retina.service";

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.scss']
})
export class TopMenuComponent implements OnInit, OnDestroy {

  /**
   * TRUE if the User is logged in.
   * @type {boolean}
   */
  userIsLoggedIn = false;

  /**
   * TRUE if the User is an Administrator.
   * @type {boolean}
   */
  userIsAdmin = false;

  /**
   * Used to show/hide the entire menu bar.
   * @type {boolean}
   */
  menu = true;

  /**
   * Subscribe Object that observes the User.
   */
  userSubscription: Subscription;

  /**
   * The current User, that is used to determine which menu should be displayed.
   * TODO: strong typing
   */
  user: any;

  /**
   * Height in pixels of the extended mobile menu.
   * @type {number}
   */
  private extendedMenuHeight = 200;

  /**
   * Flag to represent the state of the mobile menu.
   * @type {boolean}
   */
  private menuOpen = false;

  /**
   * Element Reference to the mobile menu container.
   */
  @ViewChild('mobileMenu') mobileMenu: ElementRef;

  constructor(private sessionService: SessionService,
              private retina: RetinaService) {

  }

  /**
   * Subscribes to the user on init.
   */
  ngOnInit() {
    this.subscribeToUser();
  }

  /**
   * Subscribes this component to the User data on the sessionService.
   * When the User changes, the menu should adapt to the users role.
   */
  subscribeToUser() {
    this.userSubscription = this.sessionService.user$.subscribe(
      (user: User) => {
        this.userIsLoggedIn = (user !== null);
        if (user !== null) {
          this.userIsAdmin = (user.role === 'mdz-admin');
          this.user = user;
        }
      }
    );
  }

  /**
   * When the component is destroyed, it should unsubscribe from the user to prevent memory leaks.
   */
  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  /**
   * Toggles the mobile menu.
   */
  private toggleMobileMenu() {

    if(this.mobileMenu.nativeElement.style.opacity === '1'){
      this.mobileMenu.nativeElement.style.opacity = '0';
      this.mobileMenu.nativeElement.style.top = '-600px';
    }else{
      this.mobileMenu.nativeElement.style.opacity = '1';
      this.mobileMenu.nativeElement.style.top = '-0';
    }
    this.menuOpen = !this.menuOpen;
  }
}
