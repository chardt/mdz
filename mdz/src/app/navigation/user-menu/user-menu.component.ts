import {Component, OnInit, EventEmitter, Output} from '@angular/core';
import {ProfileImageService} from '../../common/profile-image/profile-image.service';
import {DomSanitizer} from '@angular/platform-browser';
import {DEFAULT_USER_IMAGE_URL} from '../../config/constants';

@Component({
  // tslint:disable-next-line
  selector: '[app-user-menu]',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss']
})
export class UserMenuComponent implements OnInit {

  private userImageUrl: string;

  @Output('navigatedEventEmitter') navigatedEventEmitter: EventEmitter<any> = new EventEmitter();

  constructor(private profileImageService: ProfileImageService,
              private domSanitizer: DomSanitizer) {
  }

  private navigated() {
    this.navigatedEventEmitter.emit();
  }

  ngOnInit() {

    this.profileImageService.url$.subscribe(
      url => {
        this.userImageUrl = url;
      }
    );
  }

  /**
   * Bypasses CSS security.
   *
   * @param path
   * @returns {SafeStyle}
   */
  path(path: string) {
    if (!path) {
      return 'url(\'' + DEFAULT_USER_IMAGE_URL + '\')';
    }
    return this.domSanitizer.bypassSecurityTrustStyle('url(' + path + ')');
  }


}
