import {Component, Output, EventEmitter} from '@angular/core';

@Component({
  // tslint:disable-next-line
  selector: '[app-admin-menu]',
  templateUrl: './admin-menu.component.html',
  styleUrls: ['./admin-menu.component.css']
})
export class AdminMenuComponent {

  @Output('navigatedEventEmitter') navigatedEventEmitter: EventEmitter<any> = new EventEmitter();

  private navigated() {
    this.navigatedEventEmitter.emit();
  }

}
