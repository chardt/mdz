import {Component, OnInit, EventEmitter, Output} from '@angular/core';
import {CmsService} from '../../admin/cms/cms.service';
import {CmsPage} from '../../admin/cms/cms-page.model';
import {StateService} from '@uirouter/angular';

@Component({
  selector: 'cms-pages',
  templateUrl: './cms-pages.component.html',
  styleUrls: ['./cms-pages.component.scss'],
  providers: [CmsService]
})
export class CmsPagesComponent implements OnInit {

  /**
   * Array of the available CmsPages.
   */
  private cmsPages: CmsPage[];

  @Output('navigatedEventEmitter') navigatedEventEmitter: EventEmitter<any> = new EventEmitter();

  constructor(private cmsService: CmsService,
              private state: StateService) {
  }

  ngOnInit() {
    this.loadMenu();
  }

  /**
   * GETs the CmsPages from the API.
   */
  private loadMenu() {
    const options = {
      order: {
        by: 'id',
        direction: 'desc'
      },
      ids: [2, 1]
    };
    this.cmsService.getEntity(CmsPage.entity, options).subscribe(
      response => {
        this.cmsPages = CmsPage.arrayFromJson(response.json());
      }
    );
  }

  /**
   * Transitions to a state that shows the target page.
   * @param page
   */
  private navigate(page: CmsPage) {
    const pageId = page.getId();
    this.state.go('page', {url: pageId});
    this.navigatedEventEmitter.emit();
  }



}
