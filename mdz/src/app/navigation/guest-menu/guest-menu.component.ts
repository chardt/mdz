import {Component, Output, EventEmitter} from '@angular/core';

@Component({
  // tslint:disable-next-line
  selector: '[app-guest-menu]',
  templateUrl: './guest-menu.component.html',
  styleUrls: ['./guest-menu.component.scss']
})
export class GuestMenuComponent {

  @Output('navigatedEventEmitter') navigatedEventEmitter: EventEmitter<any> = new EventEmitter();

  private navigated() {
    this.navigatedEventEmitter.emit();
  }

}
