import {Component, OnInit} from '@angular/core';
import {CmsPage} from '../admin/cms/cms-page.model';
import {CmsService} from '../admin/cms/cms.service';
import {StateService} from '@uirouter/angular';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss'],
  providers: [CmsService]
})
export class PageComponent implements OnInit {

  private cmsPage: CmsPage;

  constructor(private cmsService: CmsService,
              private stateSevice: StateService,
              private sanitizer: DomSanitizer, ) {
  }

  ngOnInit() {
    const url = this.stateSevice.params.url;
    const options = {
      filter: {
        url: url
      }
    };
    this.cmsService.getEntity(CmsPage.entity, options).subscribe(
      response => {
        console.log(response);
        this.cmsPage = CmsPage.fromJson(response.json().resource[0]);
      }
    );
  }

  /**
   * Bypasses HTML so it can be displayed from inside a variable
   * @param html
   * @returns {SafeHtml}
   */
  public okHtml(html: string) {
    return this.sanitizer.bypassSecurityTrustHtml(html);
  }
}
