import {Component, OnInit} from '@angular/core';
import {CmsPage} from "../../admin/cms/cms-page.model";
import {StateService} from "@uirouter/angular";
import {CmsService} from "../../admin/cms/cms.service";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  /**
   * Array of the available CmsPages.
   */
  private cmsPages: CmsPage[];

  constructor(private cmsService: CmsService,
              private state: StateService) { }


  ngOnInit() {
    this.loadMenu();
  }


  /**
   * GETs the CmsPages from the API.
   */
  private loadMenu() {
    const options = {
      order: {
        by: 'id',
        direction: 'desc'
      },
      fields:[
        'title', 'url'
      ],
      ids: [2, 1, 4, 5]
    };
    this.cmsService.getEntity(CmsPage.entity, options).subscribe(
      response => {
        this.cmsPages = CmsPage.arrayFromJson(response.json());
      }
    );
  }
}
