import {Component, ViewChild} from '@angular/core';
import {StateService} from '@uirouter/angular';
import {InitiativeService} from '../services/initiative.service';
import {Initiative} from '../model/initiative.model';
import {SessionService} from '../../user/session/services/session.service';
import {ToastsManager} from 'ng2-toastr';
import {OrtsgemeindenService} from '../../admin/ortsgemeinden/ortsgemeinden.service';
import {HandlungsfelderService} from '../../admin/handlungsfelder/handlungsfelder.service';
import {FilterSettings} from '../discover/filters/filter.settings.model';
import {Md5} from 'ts-md5/dist/md5';
import {ImageUploadComponent} from '../../common/image-upload/image-upload.component';
import {TranstoastService} from '../../common/transtoast.service';
import {Deferred} from '../../common/Deferred';
import {SupporterService} from '../services/supporter.service';
import {NotificationSettingsService} from '../show/sidebar/notification-settings/notification-settings.service';
import {User} from '../../user/model/user.model';
import {ProfileImageService} from "../../common/profile-image/profile-image.service";
import {TIMEPICKER_DE} from "../../config/date-time-picker-locale";


@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
  providers: [InitiativeService, OrtsgemeindenService, HandlungsfelderService, SupporterService, NotificationSettingsService]
})
export class CreateComponent {

  /**
   * True while the initiative is being saved.
   * @type {boolean}
   */
  private creating = false;

  private de = TIMEPICKER_DE;
  /**
   * Database ID.
   */
  initiativeId: number;

  /**
   * Title of the new Initiative.
   */
  title: string;

  /**
   * The description as HTML String. Bound to a TinyMCE Instance.
   */
  description: string = '<p><i>Beschreibe Dein Vorhaben, sodass andere Benutzer direkt verstehen, worum es geht.</i></p>' +
    '<h2>Was willst Du erreichen?</h2>' +
    '<p><i>Erkläre Deine Ziele und wieso diese auch für andere erstrebenswert sind. Bewege Deine Mitmenschen dazu, Dich '
    + 'zu unterstützen.</i></p>'
    + '<h2>Was hast Du bisher getan?</h2>'
    + '<p><i>Lasse deine Mitstreiter wissen, was Du bereits geleistet hast.</i></p>'
    + '<p><b>Ich habe bereits folgendes organisiert:</b></p>'
    + '<ul><li>Punkt 1</li><li>Punkt 2</li></ul>'
    ;

  /**
   * Optional date that the event or initiative has or will occur.
   * @type {any}
   */
  private date: Date;

  /**
   * Optional time that the event or initiative has or will occur.
   * @type {any}
   */
  private time: String = null;

  /**
   * Latitude for the geo reference. Defaults to Sprendlingen.
   * @type {number}
   */
  latitude = 49.866667;

  /**
   * Longitude for the geo reference. Defaults to Sprendlingen.
   * @type {number}
   */
  longitude = 7.983333;

  /**
   * Latitude for the geo reference. Defaults to Sprendlingen.
   * @type {number}
   */
  markerLatitude: number = null; // 49.866667;

  /**
   * Longitude for the geo reference. Defaults to Sprendlingen.
   * @type {number}
   */
  markerLongitude: number = null; // 7.983333;

  /**
   * Zoom value for Google Map.
   * @type {number}
   */
  zoom = 12;

  /**
   * Comma separated List of E-Mail addresses that should receive an invitation.
   */
  private friends: string;

  /**
   * List of Usernames/emails that should be administrators for the initiative.
   * @type {Array}
   */
  private admins: User[] = [];

  /**
   * Variable for the next new admin.
   * @type {any}
   */
  private newAdmin: string = null;

  /**
   * Message to be sent to the invitees.
   */
  private message: string;

  /**
   * Counter for the finished image uploads.
   * @type {number}
   */
  private imagesUploaded = 0;

  /**
   * Flag to toggle when the Hero Image has been uploaded.
   * @type {boolean}
   */
  private heroUploaded = false;

  private heroDummy: string = null;
  private heroDummyBase64: string = null;
  /**
   * JSON object to store the generated file names for all the images in.
   * @type {{file-1: any; file-2: any; file-3: any; file-hero: any}}
   */
  private filenames: Object = {
    'file-1': null,
    'file-2': null,
    'file-3': null,
    'file-hero': null
  };

  /**
   * Defines wheter the inititaive is to be an inititative or an event.
   * @type {boolean}
   */
  private isEvent = false;

  /**
   * Step counter for the create wizard.
   * @type {number}
   */
  private currentStep = 0;

  /**
   * Stores the selection of Handlungsfelder and Ortsgemeinden.
   */
  filterSettings: FilterSettings;

  /**
   * ViewChild accessor for the first Gallery image.
   */
  @ViewChild('upload1') imageUploader1: ImageUploadComponent;

  /**
   * ViewChild accessor for the second Gallery image.
   */
  @ViewChild('upload2') imageUploader2: ImageUploadComponent;

  /**
   * ViewChild accessor for the third Gallery image.
   */
  @ViewChild('upload3') imageUploader3: ImageUploadComponent;

  /**
   * ViewChild accessor for the Initiative Hero.
   */
  @ViewChild('uploadHero') imageUploaderHero: ImageUploadComponent;

  /**
   * Count of the images that were replaced. When all have been uploaded, this is used do count the uploads against
   * so the user can be forwarded to the Initiative.
   */
  private forwardImageUploadLimit = 0;

  constructor(private initiativeService: InitiativeService,
              private sessionService: SessionService,
              private stateService: StateService,
              private toastr: TranstoastService,
              private supporterService: SupporterService,
              private notificationSettingsService: NotificationSettingsService,
              private profileImageService: ProfileImageService) {
  }

  /**
   * Creates a new Initiative Object and calls the creation method.
   */
  btnCreateInitiativePressed() {

    const newInitiative = {
      title: this.title,
      description: this.removeLinks(this.description),
      latitude: this.markerLatitude,
      longitude: this.markerLongitude,
      is_event: this.isEvent
    };

    // add options date
    if (this.date) {
      newInitiative['date'] = this.date.toISOString();
    }

    const i = Initiative.fromJson(newInitiative);
    this.createInitiative(i);
  }

  private noSubmit(event) {
    event.preventDefault();
  }

  private maybeNext(event) {
    if (event.keyCode === 13) {
      this.nextStep();
    }
  }

  private nextStep() {
    if (this.stepValidator()) {
      this.currentStep++;
    }
  }

  private getWhatItIs() {
    return (this.isEvent) ? 'Aktion' : 'Initiative';
  }

  private stepValidator(currentStep?: number): boolean {
    if (!currentStep) {
      currentStep = this.currentStep;
    }
    switch (currentStep) {
      case 1: {
        if (!this.title || this.title.length < 3) {
          this.toastr.info('Bitte gib deiner ' + this.getWhatItIs() + ' einen Namen.');
          return false;
        }
      }
        break;

      case 2: {
        if (!this.description || this.description.length < 3) {
          this.toastr.info('Bitte beschreibe deine ' + this.getWhatItIs() + '.');
          return false;
        }
      }
        break;

      case 3: {
        let ok = true;
        if (!this.filterSettings) {
          this.toastr.info('Bitte wähle mindestens eine Ortsgemeinde und ein Handlungsfeld aus.');
          ok = false;
        }

        if (this.filterSettings && this.filterSettings.handlungsfelder.length === 0) {
          this.toastr.info('Bitte wähle mindestens ein Handlungsfeld aus.');
          ok = false;
        }

        if (this.filterSettings && this.filterSettings.ortsgemeinden.length === 0) {
          this.toastr.info('Bitte wähle mindestens eine Ortsgemeinde aus.');
          ok = false;
        }
        return ok;

      }

      case 5: {
        if (!this.imageUploaderHero.filePresent() && !this.heroDummy) {
          this.toastr.info('Bitte wähle ein Titelbild.');
          return false;
        }
      }
        break;

      // case 6: {
      //   let imageCount = 0;
      //   if (this.imageUploader1.filePresent()) {
      //     imageCount++;
      //   }
      //   if (this.imageUploader2.filePresent()) {
      //     imageCount++;
      //   }
      //   if (this.imageUploader3.filePresent()) {
      //     imageCount++;
      //   }
      //   if (imageCount === 0) {
      //     this.toastr.info('Bitte füge ein Bild hinzu.');
      //     return false;
      //   }
      // }
      //   break;
    }

    return true;
  }

  private previousStep() {
    this.currentStep--;
  }

  /**
   * Action that defines wheter the new entity is an event or an initiative.
   * @param isEvent
   */
  private btnSelectTypePressed(isEvent) {
    this.isEvent = isEvent;
    this.nextStep();
  }

  /**
   * Registers a click on the map ad sets the lat/lon coordinate variables to the clicked point
   * @param event
   */
  mapClicked(event) {
    this.markerLatitude = event.coords.lat;
    this.markerLongitude = event.coords.lng;
  }

  /**
   * Clears the georeference
   */
  clearGeoreference() {
    this.markerLatitude = null;
    this.markerLongitude = null;
  }

  /**
   * Lazily creates file names for the images
   * @param number
   * @returns {any}
   */
  createFileName(number?: any) {
    let filename = this.filenames['file-' + number];
    if (!filename) {
      const rand = Math.random() + Date.now();
      filename = Md5.hashStr(rand + number + '');
      this.filenames['file-' + number] = filename;
    }
    return filename;
  }

  /**
   * Checks if all necessary data is present for the initiative to be successfully saved.
   * @returns {boolean}
   */
  private checkAll(): boolean {

    let ok = true;
    for (let i = 0; i < 8; i++) {
      if (!this.stepValidator(i)) {
        ok = false;
      }
    }
    return ok;
  }

  /**
   * POSTs the given initiative to the API.
   * When the Initiative Model was saved, it is linked to the ortsgemeinden,
   * handlungsfelder, the owner and the images are uploaded.
   *
   * @param initiative
   */
  createInitiative(initiative: Initiative) {


    if (!this.checkAll()) {
      return;
    }

    this.creating = true;

    this.initiativeService.postEntity(initiative).subscribe(
      (response) => {
        this.initiativeId = response.json().resource[0].id;
        // get users id
        const currentUser = this.sessionService.getCurrentUser();

        this.makeOwner(currentUser.id, this.initiativeId);
        this.addOrtsgemeinden();
        this.addHandlungsfelder();
        // support own initiative
        this.supporterService.support(this.initiativeId).then(
          resolved => {
            // prime the notification settings to auto-create my own notification settings for the new ini
            this.imageUploader1.uploadImage().then(
              () => {
                this.imageUploader2.uploadImage().then(
                  () => {
                    this.imageUploader3.uploadImage().then(
                      () => {

                        if (this.heroDummy && !this.imageUploaderHero.filePresent()) {
                          // upload the hero dummy instead
                          this.uploadHeroDummy().then(
                            () => {
                              // invite friends
                              this.iniviteFriends().then(() => {
                                // make the admins admins
                                this.addAdmins().then(() => {
                                  this.notificationSettingsService.prime(this.initiativeId);
                                  this.notificationSettingsService.createInitialSettings().then(() => {
                                    this.creating = false;
                                    this.stateService.current.params.saved = true;
                                    this.stateService.go('show', {initiativeId: this.initiativeId});
                                  });
                                });
                              });
                            }
                          );
                        } else {
                          // no dummy, upload the real image
                          this.imageUploaderHero.uploadImage().then(
                            () => {
                              // invite friends
                              this.iniviteFriends().then(() => {
                                // make the admins admins
                                this.addAdmins().then(() => {
                                  this.notificationSettingsService.prime(this.initiativeId);
                                  this.notificationSettingsService.createInitialSettings().then(() => {
                                    this.creating = false;
                                    this.stateService.current.params.saved = true;
                                    this.stateService.go('show', {initiativeId: this.initiativeId});
                                  });
                                });
                              });
                            }
                          );
                        }
                      }
                    );
                  }
                );
              }
            );
          });
      },
      (errorResponse) => {
        this.toastr.error(errorResponse.json().error.message);
      }
    );
  }

  /**
   * Sends invitations to all email addresses in this.friends.
   */
  private iniviteFriends() {
    const q = new Deferred();
    this.initiativeService.inviteFriends(this.friends, this.initiativeId, this.title, this.message).subscribe(
      (response) => {
        // check if the response contains information about invalid emails and display an error message.
        try {
          response = response.json();
          const message = response.message;
          const invalidMails = response.invalidMails;
          if (invalidMails) {
            this.toastr.error(invalidMails.toString(), message);
          }
          q.resolve();
        } catch (err) {
          q.resolve();
        }
      },
      () => {
        q.reject();
      }
    );
    return q.promise;
  }

  /**
   * Removes all the hyperlinks from the string.
   * @param string
   * @returns {string}
   */
  private removeLinks(string: string): string {

    if (string) {
      const regEx = /<a\s[^>]*>(.*?)<\/a>/gi;
      string = string.replace(regEx, '$1');
    }

    return string;
  }

  /**
   * Is triggered by the image upload Events, each time an image has been uploaded.
   * When all images were uploaded (3 images, 1 hero), it changes the state to "show".
   */
  private tryForward() {
    // increase, since one upload is complete
    this.imagesUploaded++;

    if (this.imagesUploaded < this.forwardImageUploadLimit) {
      return;
    }

    // this.stateService.go('show', {initiativeId: this.initiativeId});

  }

  /**
   * Increments the upload counter limit.
   */
  private increaseUploadCounter() {
    this.forwardImageUploadLimit++;
  }

  private heroFileInfoChanged() {
    this.increaseUploadCounter();
    this.heroDummy = null;
  }

  /**
   * Convenience Method for attachImage to attach an image as hero.
   * @param $event
   */
  attachHero($event) {
    this.attachImage($event, true);
  }

  /**
   * Event, triggered by the child ImageUpload. $event contains the POST response
   * of the uploaded image. The new model is then linked to the initiative.
   *
   * @param $event
   * @param hero
   */
  attachImage($event, hero?: boolean) {
    const payload = $event.json().resource[0];

    this.initiativeService.postImage(payload, this.initiativeId, hero).subscribe(
      () => {
        if (!hero) {
          this.imagesUploaded++;
        } else {
          this.heroUploaded = true;
        }

        this.tryForward();
      },
      errorResponse => {
        console.error('after attach', errorResponse.json());
      }
    );
  }

  /**
   * For each selected Ortsgemeinde, a connection model is formed and POSTed to the API.
   */
  addOrtsgemeinden() {
    //
    for (const og of this.filterSettings.ortsgemeinden) {
      const connection = {
        initiative_id: this.initiativeId,
        ortsgemeinde_id: og
      };

      this.initiativeService.postEntity(connection, 'initiative_belongs_to_ortsgemeinde').subscribe(
        response => {
          console.log(response);
        },

        errorResponse => {
          console.error(errorResponse);
        }
      );
    }
  }

  /**
   * For each selected Handlungsfeld, a connection model is formed and POSTed to the API.
   */
  addHandlungsfelder() {

    for (const hf of this.filterSettings.handlungsfelder) {

      const connection = {
        initiative_id: this.initiativeId,
        handlungsfeld_id: hf
      };

      this.initiativeService.postEntity(connection, 'initiative_belongs_to_handlungsfeld').subscribe(
        response => {
          console.log(response);
        },

        errorResponse => {
          console.log(errorResponse);
        }
      );
    }
  }

  /**
   * When the Ortsgemeinde/Handlungsfeld selection was changed, this method is called
   * to consume the change.
   * @param filterSettings
   */
  onFilterChanged(filterSettings) {
    this.filterSettings = filterSettings;
  }

  /**
   * Adds the current user as an owner to the current initiative.
   *
   * @param user_id
   * @param initiative_id
   */
  makeOwner(user_id: number, initiative_id: number) {

    const entity = {
      user_id: user_id,
      initiative_id: initiative_id
    };

    this.initiativeService.postEntity(entity, 'user_owns_initiative').subscribe(
      (response) => {
        console.log(response.json());

      },
      (errorResponse) => {
        this.toastr.error(errorResponse.json().error.message);
      }
    );
  }

  /**
   * Adds all Admins of the initiative.
   */
  private addAdmins(): Promise<any> {

    // count the admins to add
    let togo = this.admins.length;

    // promise the tell when all is done
    const d = new Deferred();

    if (this.admins.length === 0) {
      d.resolve();
      return d.promise;
    }
    // loop over the admins
    for (const admin of this.admins) {
      // prepare new entry
      const newAdminPayload = {
        user_id: admin.id,
        initiative_id: this.initiativeId
      };

      // add the single entry. In both cases (success and error) decrease togo and maybe resolve.
      this.initiativeService.postEntity(newAdminPayload, 'user_owns_initiative').subscribe(
        response => {
          togo--;
          if (togo === 0) {
            d.resolve();
          }
        },
        error => {
          togo--;
          if (togo === 0) {
            d.resolve();
          }
        }
      );
    }

    return d.promise;
  }

  /**
   * Removes the given admin. Cannot perform when the list of admins counts lower than two
   * and warns the user to first add another admin.
   * @param index
   */
  btnRemoveAdminPressed(index: number) {
    this.admins.splice(index, 1);
  }


  /**
   * Adds a new admin. Read inline comments for further documentation.
   */
  btnAddAdminPressed() {
    if (this.newAdmin) {

      const options = {
        filter: {
          email: this.newAdmin
        },
        fields: ['id']
      };
      const systemCall = true;


      this.initiativeService.getEntity('/system/user', options, systemCall).subscribe(
        response => {
          const addy = response.json().resource[0];
          if (addy) {
            this.admins.push(User.fromJson(addy));
            this.newAdmin = null;
          } else {
            let msg = 'Diesen Benutzer kennen wir nicht (';
            msg += this.newAdmin;
            msg += '). Versichere dich, dass dies die korrekte E-Mail Adresse des Benutzers ist.';
            this.toastr.info(msg);
          }
        }
      );
    }
  }

  private heroDummyKlicked(event) {
    const target = event.target;
    const id = target.id;
    const file = 'https://www.marktplatz-sg.de/assets/images/' + id + '.jpg';
    // const file = 'http://192.168.42.42:4200/assets/images/' + id + '.jpg';

    this.heroDummy = file;

    this.convertFileToDataURLviaFileReader(this.heroDummy,
      result => {
        this.heroDummyBase64 = result;
      }
    );

    // remove selected class from others
    document.getElementById('dummy-1').className = "";
    document.getElementById('dummy-2').className = "";
    document.getElementById('dummy-3').className = "";

    // mark clicked one
    document.getElementById(id).className = "selected";
  }

  private convertFileToDataURLviaFileReader(url, callback) {
    let xhr = new XMLHttpRequest();
    xhr.onload = function () {
      let reader = new FileReader();
      reader.onloadend = function () {
        const search = 'data:image/jpeg;base64,';
        const erg = reader.result.replace(search,'');
        callback(erg);
      };
      reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
  }

  private heroDummyHashName() {

    const hashBase = this.sessionService.getCurrentUser().id.toString() + this.heroDummy;
    let hash = Md5.hashStr(hashBase);

    hash += ('.jpg');

    return hash + '';
  }

  private uploadHeroDummy() {

    const q = new Deferred();

    const uploadPackage = {
      'resource': [
        {
          name: this.heroDummyHashName(),
          type: 'file',
          is_base64: true,
          content: this.heroDummyBase64
        }
      ]
    };

    const target = '/files/initiatives/';
    this.profileImageService.postImage(
      uploadPackage,
      this.profileImageService.endpoint(target))
      .subscribe(
        response => {
          this.attachHero(response);
          q.resolve();
        },
        errorResponse => {
          this.toastr.error(errorResponse.json().error.message);
          this.attachHero(errorResponse);
          q.resolve();
        }
      );

    return q.promise;
  }
}
