import {Component, OnInit, ViewChild, AfterContentInit} from '@angular/core';
import {StateService} from '@uirouter/angular';
import {Initiative} from '../model/initiative.model';
import {InitiativeService} from '../services/initiative.service';
import {ImageUploadComponent} from '../../common/image-upload/image-upload.component';
import {DREAMFACTORY_API_KEY, DREAMFACTORY_INSTANCE_URL} from '../../config/constants';
import {TranstoastService} from '../../common/transtoast.service';
import {HandlungsfelderService} from "../../admin/handlungsfelder/handlungsfelder.service";
import {OrtsgemeindenService} from "../../admin/ortsgemeinden/ortsgemeinden.service";
import {FilterSettings} from "../discover/filters/filter.settings.model";
import {FilterEntryOG} from "../discover/filters/filter.entryog.model";
import {FiltersComponent} from "../discover/filters/filters.component";
import {FilterEntryHF} from "../discover/filters/filter.entryhf.model";
import {Ortsgemeinde} from "../../admin/ortsgemeinden/ortsgemeinde.model";
import {TIMEPICKER_DE} from "../../config/date-time-picker-locale";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  providers: [InitiativeService, HandlungsfelderService, OrtsgemeindenService]
})
export class EditComponent implements OnInit, AfterContentInit {

  /**
   * Initiative to be edited
   */
  initiative: Initiative;

  /**
   * Username of a User that will be added to the admin list
   */
  newAdmin: string;

  /**
   * Connected .date and .time attributes of the initiative.
   */
  private datetime: Date;

  /**
   * Latitude of the georeference
   */
  lat = 49.866667;

  /**
   * Longitude of the georeference
   */
  lon = 7.983333;

  /**
   * Stores the selection of Handlungsfelder and Ortsgemeinden.
   */
  filterSettings: FilterSettings;

  /**
   * ViewChild accessor for the first Gallery image.
   */
  @ViewChild('upload1') imageUploader1: ImageUploadComponent;

  /**
   * ViewChild accessor for the second Gallery image.
   */
  @ViewChild('upload2') imageUploader2: ImageUploadComponent;

  /**
   * ViewChild accessor for the third Gallery image.
   */
  @ViewChild('upload3') imageUploader3: ImageUploadComponent;

  /**
   * ViewChild accessor for the Initiative Hero.
   */
  @ViewChild('uploadHero') imageUploaderHero: ImageUploadComponent;

  /**
   * ViewChild accessor fot the Filters
   */
  @ViewChild('filters') filters: FiltersComponent;

  private saving = false;
  /**
   * Filename for the Hero Image.
   */
  private heroFileName: string;

  /**
   * Path for the Hero Image up up/download.
   */
  private heroFilePath: string;

  /**
   * Original file type of the hero image.
   */
  private heroFileType: string;

  /**
   * Flag to control whether the image uploader clears the existing hero, or not.
   * @type {boolean}
   */
  private heroMustBeCleared = false;

  /**
   * Filename for Image #1.
   */
  private image1Name: string;

  /**
   * Filename for Image #2.
   */
  private image2Name: string;

  /**
   * Filename for Image #3.
   */
  private image3Name: string;

  /**
   * File path for Image #1.
   */
  private image1Path: string = null;

  /**
   * File path for Image #2.
   */
  private image2Path: string = null;

  /**
   * File path for Image #3.
   */
  private image3Path: string = null;

  /**
   * File extension of the original Image 1.
   */
  private image1FileType: string;

  /**
   * File extension of the original Image 2.
   */
  private image2FileType: string;

  /**
   * File extension of the original Image 3.
   */
  private image3FileType: string;

  /**
   * Flag to control whether the image uploader should delete the old image first.
   */
  private image1MustBeCleared = false;

  /**
   * Flag to control whether the image uploader should delete the old image first.
   */
  private image2MustBeCleared = false;

  /**
   * Flag to control whether the image uploader should delete the old image first.
   */
  private image3MustBeCleared = false;

  private image1Id: number;
  private image2Id: number;
  private image3Id: number;
  private heroId: number;
  /**
   * Counter for the finished image uploads.
   * @type {number}
   */
  private imagesUploaded = 0;

  /**
   * Flag to toggle when the Hero Image has been uploaded.
   * @type {boolean}
   */
  private heroUploaded = false;

  /**
   * TimeStamp that is going to be create when the component is initialized.
   * Used to seed image URLs with a cache-killing query string.
   * @type {any}
   */
  private cacheKiller: string = null;

  private de = TIMEPICKER_DE;

  /**
   * Count of the images that were replaced. When all have been uploaded, this is used do count the uploads against
   * so the user can be forwarded to the Initiative.
   */
  private forwardImageUploadLimit = 0;

  constructor(private stateService: StateService,
              private initiativeService: InitiativeService,
              private toastr: TranstoastService) {
    this.cacheKiller = Date.now() + '';
  }

  /**
   * Loads the initiative on init
   */
  ngOnInit() {
  }

  ngAfterContentInit() {
    this.loadFullInitiative();
  }

  /**
   * When the Ortsgemeinde/Handlungsfeld selection was changed, this method is called
   * to consume the change.
   * @param filterSettings
   */
  onFilterChanged(filterSettings) {
    this.filterSettings = filterSettings;
  }

  /**
   * Loads the entire Initiative with all the relevant related data.
   * It takes the Initiative ID from the params, forms an options object with the
   * requested relations and GETs the list from the API.
   */
  loadFullInitiative() {
    const id = this.stateService.params.initiativeId;
    const options = {
      id: id,
      related: [
        Initiative.entityRelationOwners,
        Initiative.entityRelationSupporters,
        Initiative.entityRelationOrtsgemeindenEntity,
        Initiative.entityRelationHandlungsfelderEntity,
        Initiative.entityRelationImages
      ]
    };
    this.initiativeService.getEntity(Initiative.entity, options).subscribe(
      (response) => {
        const body = response.json();

        this.initiative = Initiative.fromJson(body);

        // expressively cast to :number
        this.initiative.latitude = +this.initiative.latitude;
        this.initiative.longitude = +this.initiative.longitude;

        if (this.initiative.latitude) {
          this.lat = this.initiative.latitude;
        }

        if (this.initiative.longitude) {
          this.lon = this.initiative.longitude;
        }

        // get images into the editor
        this.extractImages();

        // now wait a minute!
        setTimeout(() => {
          this.extractOrtsgemeinden();
          this.extractHandlungsfelder();
        }, 1000);

      },
      (errorResponse) => {
        this.toastr.error(errorResponse.json().error.message);
      }
    );
  }

  /**
   * Takes the Ortsgemeinden from the Initiative and pre selects them in the filter bar.
   */
  private extractOrtsgemeinden() {
    let ogsArr: FilterEntryOG[] = [];
    for (const og of this.initiative.getOrtsgemeinden()) {
      const a: any = og;
      ogsArr.push(new FilterEntryOG(a.id, a.name));
    }
    this.filters.preselectOGs(ogsArr);
  }

  /**
   * Takes the Handlungsfelder from the Initiative and pre selects them in the filter bar.
   */
  private extractHandlungsfelder() {
    let hfsArr: FilterEntryHF[] = [];
    for (const hf of this.initiative.getHandlungsfelder()) {
      const a: any = hf;
      hfsArr.push(new FilterEntryHF(a.id, a.name));
    }
    this.filters.preselectHFs(hfsArr);
  }

  /**
   * Takes the image paths from the initiatives images an sorts them into the Hero Image or normal Image path variables.
   */
  private extractImages() {

    const basePath = DREAMFACTORY_INSTANCE_URL + '/api/v2/files/';
    const key_appendix = '?ts=' + this.cacheKiller + '&api_key=' + DREAMFACTORY_API_KEY;

    let path;
    let pathElements;
    let name;
    let extension;

    // get the Hero path - only if the hero exists (is not undefined)
    if (this.initiative.getHero()) {

      const image = this.initiative.getHero();
      path = image.getPath();
      pathElements = path.split('/');
      name = pathElements[pathElements.length - 1];
      extension = null;

      // in case a file type was appended - remove that.
      pathElements = name.split('.');
      if (pathElements.length > 1) {
        name = pathElements[0];
        extension = pathElements[1];
      }


      this.heroFilePath = basePath + path + key_appendix;
      this.heroFileType = extension;
      this.heroFileName = name;
      this.heroMustBeCleared = true;
      this.heroId = image.getId();
      // this.imageUploaderHero.setFilePresent();
    }

    // get the normal images paths
    for (const image of this.initiative.getImages()) {


      path = image.getPath();
      pathElements = path.split('/');
      name = pathElements[pathElements.length - 1];
      extension = null;


      // in case a file type was appended - remove that.
      pathElements = name.split('.');
      if (pathElements.length > 1) {
        name = pathElements[0];
        extension = pathElements[1];
      }

      let set = false;

      if (!this.image1Path) {
        this.image1Path = basePath + path + key_appendix;
        this.image1Name = name;
        // this.imageUploader1.fileName = name;
        this.image1FileType = extension;
        this.image1MustBeCleared = true;
        this.image1Id = image.getId();
        set = true;
        // this.imageUploader1.setFilePresent();
        // continue;
      }

      if (this.image1Path && !this.image2Path && !set) {
        this.image2Path = basePath + path + key_appendix;
        this.image2Name = name;
        // this.imageUploader2.fileName = name;
        this.image2FileType = extension;
        this.image2MustBeCleared = true;
        this.image2Id = image.getId();
        set = true;
        // this.imageUploader2.setFilePresent();
        // continue;
      }

      if (this.image1Path && this.image2Path && !this.image3Path && !set) {
        this.image3Path = basePath + path + key_appendix;
        this.image3Name = name;
        // this.imageUploader3.fileName = name;
        this.image3FileType = extension;
        this.image3MustBeCleared = true;
        this.image3Id = image.getId();
        set = true;
        // this.imageUploader3.setFilePresent();
      }
    }

    // console.warn('---------AFTER EXTRACT BEGIN--------');
    // console.log(this.imageUploader1, this.imageUploader2, this.imageUploader3);
    // console.warn('---------1--------');
    // console.warn('path',this.image1Path);
    // console.warn('name',this.image1Name);
    // console.warn('type',this.image1FileType);
    // console.warn('must be cleared',this.image1MustBeCleared);
    // console.warn('id',this.image1Id);
    // console.warn('---------2--------');
    // console.warn('path',this.image2Path);
    // console.warn('name',this.image2Name);
    // console.warn('type',this.image2FileType);
    // console.warn('must be cleared',this.image2MustBeCleared);
    // console.warn('id',this.image2Id);
    // console.warn('---------3--------');
    // console.warn('path',this.image3Path);
    // console.warn('name',this.image3Name);
    // console.warn('type',this.image3FileType);
    // console.warn('must be cleared',this.image3MustBeCleared);
    // console.warn('id',this.image3Id);
    // console.warn('---------AFTER EXTRACT END--------');
  }

  /**
   * Convenience Method for attachImage to attach an image as hero.
   * @param $event
   */
  attachHero($event) {
    this.attachImage($event, true);
  }

  /**
   * Returns a string depending on whether the initiative is an event or not.
   * @returns {string|string}
   */
  private getWhatItIs() {
    if (this.initiative) {
      return (this.initiative.is_event) ? 'Aktion' : 'Initiative';
    }
  }

  /**
   * Increments the upload counter limit.
   */
  private increaseUploadCounter() {
    this.forwardImageUploadLimit++;
  }

  /**
   * Event, triggered by the child ImageUpload. $event contains the POST response
   * of the uploaded image. The new model is then linked to the initiative.
   *
   * @param $event
   * @param hero
   */
  attachImage($event, hero?: boolean) {

    const payload = $event.json().resource[0];
    console.warn('attachImage', $event, hero, payload);

    this.initiativeService.postImage(payload, this.initiative.id, hero).subscribe(
      () => {
        // this.tryForward();
      },
      errorResponse => {
        console.error('after attach', errorResponse.json());
      }
    );
  }

  /**
   * Is triggered by the image upload Events, each time an image has been uploaded.
   * When all images were uploaded (3 images, 1 hero), it changes the state to "show".
   */
  // private tryForward() {
  //
  //   console.log('tryForward');
  //
  //   // increase, since one upload is complete
  //   this.imagesUploaded++;
  //
  //   if (this.imagesUploaded < this.forwardImageUploadLimit) {
  //     return;
  //   }
  //
  //   this.toastr.success('Alle Änderungen wurden gespeichert!');
  //   this.stateService.go('show', {initiativeId: this.initiative.id});
  //
  // }

  /**
   * Removes all the hyperlinks from the string.
   * @param string
   * @returns {string}
   */
  private removeLinks(string: string): string {

    const regEx = /<a\s[^>]*>(.*?)<\/a>/gi;
    string = string.replace(regEx, '$1');

    return string;
  }

  private removeGeoReference() {
    this.initiative.latitude = null;
    this.initiative.longitude = null;
  }

  /**
   * Registers a click on the Google Map and takes the coordinates as the new georeference
   * @param event
   */
  mapClicked(event) {
    this.initiative.latitude = event.coords.lat;
    this.initiative.longitude = event.coords.lng;
  }


  /**
   * Checks if all necessary data is present for the initiative to be successfully saved.
   * @returns {boolean}
   */
  private check(): boolean {
    let ok = true;

    // let imageCount = 3;
    // if (!this.image1Path || this.image1Path.length === 0) {
    //   imageCount--;
    // }
    // if (!this.image2Path || this.image2Path.length === 0) {
    //   imageCount--;
    // }
    // if (!this.image3Path || this.image3Path.length === 0) {
    //   imageCount--;
    // }
    //
    // if (imageCount === 0) {
    //   this.toastr.error('Bitte füge ein Bild hinzu.');
    //   ok = false;
    // }

    if (!this.heroFilePath && !this.imageUploaderHero.filePresent()) {
      this.toastr.error('Bitte wähle ein Titelbild.');
      ok = false;
    }

    if (!this.initiative.description) {
      this.toastr.error('Bitte beschreibe die Initiative.');
      ok = false;
    }

    if (!this.initiative.title) {
      this.toastr.error('Gib deiner Initiative einen Titel.');
      ok = false;
    }
    //
    // if (!this.filterSettings) {
    //   this.toastr.error('Bitte wähle mindestens eine Ortsgemeinde und ein Handlungsfeld aus.');
    //   ok = false;
    // }
    //
    // if (this.filterSettings && this.filterSettings.handlungsfelder.length == 0) {
    //   this.toastr.error('Bitte wähle mindestens ein Handlungsfeld aus.');
    //   ok = false;
    // }
    //
    // if (this.filterSettings && this.filterSettings.ortsgemeinden.length == 0) {
    //   this.toastr.error('Bitte wähle mindestens eine Ortsgemeinde aus.');
    //   ok = false;
    // }


    return ok;
  }


  /**
   * Requests to update the Initiative via the InitiativeService
   */
  btnSaveInitiativePressed() {

    if (!this.check()) {
      return;
    }

    this.saving = true;

    this.initiative.description = this.removeLinks(this.initiative.description);
    this.initiativeService.updateInitiative(this.initiative);

    this.saveFilters();

    // this.imageUploader3.uploadImage(this.image3MustBeCleared, this.image3Id).then(() => {
    //   this.saving = false;
    //   this.stateService.current.params.saved = true;
    //
    // });
    this.imageUploader1.uploadImage(this.image1MustBeCleared, this.image1Id).then(() => {
        this.imageUploader2.uploadImage(this.image2MustBeCleared, this.image2Id).then(() => {
          this.imageUploader3.uploadImage(this.image3MustBeCleared, this.image3Id).then(() => {
            this.imageUploaderHero.uploadImage(this.heroMustBeCleared, this.heroId).then(() => {
              this.saving = false;
              this.stateService.current.params.saved = true;
              this.stateService.go('show', {initiativeId: this.initiative.id});
            });
          });
        });
      }
    );
  }

  /**
   * Analyzes the changes to the filters and stores them.
   */
  private saveFilters() {

    // step 1 - get the changes.
    const ortsgemeinden = this.getChangedFilters(
      this.initiative.getOrtsgemeinden(),
      this.filterSettings.ortsgemeinden);

    const handlungsfelder = this.getChangedFilters(
      this.initiative.getHandlungsfelder(),
      this.filterSettings.handlungsfelder
    );

    // step 2 - tell the api what to do.
    // 2.1 - OG DELETE
    for (const change of ortsgemeinden.toDelete) {
      this.initiativeService.removeFilter(this.initiative.id, change.id, Initiative.entityRelationOrtsgemeinde);
    }
    // 2.2 - OG ADD
    for (const change of ortsgemeinden.toAdd) {
      this.initiativeService.addFilter(this.initiative.id, change.id, Initiative.entityRelationOrtsgemeinde);
    }
    // 2.3 - HF DELETE
    for (const change of handlungsfelder.toDelete) {
      this.initiativeService.removeFilter(this.initiative.id, change.id, Initiative.entityRelationHandlungsfeld);
    }
    // 2.4 - HF ADD
    for (const change of handlungsfelder.toAdd) {
      this.initiativeService.addFilter(this.initiative.id, change.id, Initiative.entityRelationHandlungsfeld);
    }
  }

  /**
   * Compares the original filters of an Initiative with those, that the user changed while editing.
   * Returns the changes.
   *
   * @param originals
   * @param changed
   * @returns {{toAdd: any[], toDelete: any[]}}
   */
  private getChangedFilters(originals: any, changed: any) {

    // check which filters were removed. DELETE those.
    let deleted: any[] = [];

    // loop through original data and check if anything is missing in the changed data.
    for (const o of originals) {
      const original: any = o;
      // cross check with the OGs/HFs in the filter.
      let stillPresent = false;
      for (const change of changed) {
        // IDs match -> entry is still selected.
        if (original.id === +change) {
          stillPresent = true;
          break;
        }
      }
      if (!stillPresent) {
        deleted.push(original);
      }
    }

    // this time the other way around. loop through the filter and compare to the original.
    // if its not in the original it is new.
    let added: any[] = [];
    for (const change of changed) {
      let isNew = true;
      for (const o of originals) {
        const og: any = o;
        if (og.id === +change) {
          isNew = false;
          break;
        }
      }
      if (isNew) {
        added.push({id: +change});
      }
    }

    return {
      toAdd: added,
      toDelete: deleted
    }
  }

  /**
   * Fired by the visibility switch.
   * Toggles the Initiatives visibility by inverting the current visibility and requesting
   * to publish the initiative anew.
   */
  onPublishedChanged() {
    this.initiativeService.publishInitiative(this.initiative, !this.initiative.published).subscribe(
      () => {
        this.toastr.success('Die Sichtbarkeit der Initiative wurde geändert.');
      },
      errorResponse => {
        this.toastr.error(errorResponse.json().error.message);
      }
    );
  }

  /**
   * Removes the given admin. Cannot perform when the list of admins counts lower than two
   * and warns the user to first add another admin.
   * @param adminId
   */
  btnRemoveAdminPressed(adminId: number) {

    if (this.initiative.getOwners().length < 2) {
      this.toastr.warning('Füge zunächst einen anderen Benutzer hinzu. Anschließend kannst du diesen Benutzer entfernen.');
      return;
    }
    /**
     * Attention, adminId is NOT the ID of the user, but the Database ID of the ENTRY.
     * */
    this.initiativeService.deleteEntity({id: adminId}, 'user_owns_initiative').subscribe(
      response => {
        this.initiative.removeTemporaryOwner(response.json().id);
      },
      errorResponse => {
        this.toastr.error(errorResponse.json().message);
      }
    );
  }

  /**
   * Adds a new admin. Read inline comments for further documentation.
   */
  btnAddAdminPressed() {

    // get ID of the new admin
    const options = {
      filter: {
        email: this.newAdmin
      },
      fields: ['id']
    };
    const systemCall = true;

    this.initiativeService.getEntity('/system/user', options, systemCall).subscribe(
      response => {
        try {
          const resource = response.json().resource;
          if (resource.length === 0) {
            this.toastr.warning('Dieser Benutzer ist uns nicht bekannt. Lade deinen Freund ein, ' +
              'sich beim Marktplatz der Ideen anzumelden.');
            return;
          }
          // get ID from response
          const newAdminId = response.json().resource[0].id;

          // prepare new entry
          const newAdminPayload = {
            user_id: newAdminId,
            initiative_id: this.initiative.id
          };

          // check if entry is already present
          const options2 = {
            filter: newAdminPayload
          };

          // GET the new entry to check if it already exists
          this.initiativeService.getEntity('user_owns_initiative', options2).subscribe(
            response2 => {

              // resource length is 0, this entry must be new, allow the post
              if (response2.json().resource.length === 0) {

                // POST the new entry
                this.initiativeService.postEntity(newAdminPayload, 'user_owns_initiative').subscribe(
                  () => {

                    // add to local list
                    this.initiative.addTemporaryOwner(newAdminPayload);

                    // clear text input
                    this.newAdmin = '';
                  },
                  errorResponse2 => {
                    this.toastr.error(errorResponse2.json().message);
                  }
                );
              } else {
                this.toastr.info('Dieser Benutzer ist bereits eingetragen.');
              }
            },
            errorResponse => {
              this.toastr.error(errorResponse.json().message);
            }
          );
        } catch (exception) {
          console.error(exception);
          // this.toastr.error('kacke' + exception.message);
        }
      }
    );
  }
}
