import {Component, Input} from '@angular/core';
import {Initiative} from '../../model/initiative.model';
import {InitiativeService} from '../../services/initiative.service';
import {Deferred} from '../../../common/Deferred';
import {FilterSettings} from "../../discover/filters/filter.settings.model";

@Component({
  selector: 'my-own-initiatives',
  templateUrl: './my-own-initiatives.component.html',
  styleUrls: ['./my-own-initiatives.component.scss']
})
export class MyOwnInitiativesComponent {

  /**
   * Defines whether the component is in "owns" or "supports" mode.
   * Support mode will load the supported initiatives, instead of the owned ones.
   *
   * @type {boolean}
   */
  @Input() supportedInitiatives = false;

  /**
   * List of inititaives.
   * @type {Array}
   */
  private initiatives: Initiative[] = [];

  /**
   * True, if the user has no Initiatives.
   * @type {boolean}
   */
  private haveNone = false;

  private filterIsSet = false;

  /**
   * Stores the selection of Handlungsfelder and Ortsgemeinden.
   */
  filterSettings: FilterSettings;

  private filterList: number[] = [];
  /**
   * To trigger to loading indicator.
   * @type {boolean}
   */
  private loading = false;

  private searchString: string = null;

  private pagination = {
    offset: 0,
    limit: 5,
    page: 0,
    pages: 0,
    next: () => {
      if (this.pagination.page < this.pagination.pages) {
        this.pagination.offset += this.pagination.limit;
        this.fetch();
      }
    },
    previous: () => {
      if (this.pagination.page > 1) {
        this.pagination.offset -= this.pagination.limit;
        this.fetch();
      }
    },
    update: (meta) => {
      if (meta) {
        this.pagination.pages = Math.ceil(meta.count / this.pagination.limit);
        this.pagination.page = this.pagination.offset / this.pagination.limit + 1;
      }
    }
  };

  constructor(private initiativeService: InitiativeService) {
  }

  /**
   * Starts the loading process and the loading indicator.
   * @returns {Promise<T>}
   */
  public startLoading() {
    this.loading = true;
    return this.getMyInitiatives();
  }


  /**
   * Fetches a list of Initiative IDs from the API, stores it in the InitiativeService.
   * @returns {Promise<T>}
   */
  private getMyInitiatives() {
    const q = new Deferred();
    this.initiativeService.getMyInitiatives(this.supportedInitiatives).then(
      observable => {
        observable.subscribe(
          idsResponse => {
            // contains an array of the relation objects
            // create an array of just the initiative ids from that
            const entries = idsResponse.json().resource;
            const ids: number[] = [];
            for (const resource of entries) {
              // remove possible duplications
              let unique = true;
              for (const id of ids) {
                if (id === resource.initiative_id) {
                  unique = false;
                  break;
                }
              }

              // remove initiatives that are already in the list.
              if (unique) {
                ids.push(resource.initiative_id);
              }
            }

            // i have no initiatives, finish.
            if (ids.length === 0) {
              this.haveNone = true;
              this.loading = false;
              q.resolve();
              return q.promise;
            }

            // report to service
            if (this.supportedInitiatives) {
              this.initiativeService.addInititaiveIdToSupported(ids);
            } else {
              this.initiativeService.addInititaiveIdToOwned(ids);
            }

            q.resolve();

            this.fetch();
          }
        );
      }
    );
    return q.promise;
  }

  /**
   * EventListener for the search bar.
   * @param event
   */
  private onKeyUp(event) {
    // load on enter
    if (event.keyCode === 13) {
      this.pagination.offset = 0;
      this.fetch();
    } else {
      // also load when NOT enter, but search string is empty
      if (this.searchString.length === 0) {
        this.pagination.offset = 0;
        this.fetch();
      }
    }
  }

  /**
   * When the Ortsgemeinde/Handlungsfeld selection was changed, this method is called
   * to consume the change.
   * @param filterSettings
   */
  onFilterChanged(filterSettings) {
    this.filterSettings = filterSettings;

    if (filterSettings.handlungsfelder.length > 0 || filterSettings.ortsgemeinden.length > 0) {
      this.fetchFilteredList().then(
        () => {
          this.fetch();
        }
      );
    } else {
      this.filterList = [];
      this.fetch(); // without filters!
    }
  }

  private fetchFilteredList(): Promise<any> {
    const q = new Deferred();
    this.initiativeService.initiativeIDsByFilter(this.filterSettings, true)
      .then(
        list => {
          this.filterList = list;
          q.resolve()
        },
        error => {
          this.filterList = [];
          q.reject();
        }
      );
    return q.promise;
  }

  /**
   * Gets the relevant list of IDs from the InititativeService and then loads the Initiatives.
   */
  private fetch(success?: boolean) {

    this.filterIsSet = (
      (this.filterSettings) &&
      (this.filterSettings.handlungsfelder.length > 0 || this.filterSettings.ortsgemeinden.length > 0)
    );

    let ids;
    if (this.supportedInitiatives) {
      ids = this.initiativeService.getMySupportedInitiativeIDs().slice(0);
    } else {
      ids = this.initiativeService.getMyInititaiveIDs().slice(0);
    }

    // merge ids list with filter results
    if (this.filterIsSet) {
      ids = this.intersect(ids.slice(0), this.filterList.slice(0));
    }

    // When the list is empty, don't even try to load.
    // It will result in _all_ inititaives being loaded, as the empty array is interpreted as "no restriction there".
    if (ids.length === 0) {
      this.loading = false;
      this.haveNone = true;

      this.initiatives = [];
      return;
    }

    let options = {
      limit: this.pagination.limit,
      offset: this.pagination.offset,
      includeCount: true,
      related: [
        Initiative.entityRelationOwners,
        Initiative.entityRelationSupporters,
        Initiative.entityRelationOrtsgemeindenEntity,
        Initiative.entityRelationHandlungsfelderEntity,
        Initiative.entityRelationImages
      ],
      ids: ids,
      order: {
        by: 'successfull',
        direction: 'asc'
      }
      // filter: {
        // successfull: true,
        // closed: false
      // }
    };

    if (this.supportedInitiatives) {
      options['filter'] = {
        visible : true,
        published : true
      };
    }

    if (this.searchString && this.searchString.length > 0) {
      options['search'] = {
        title: this.searchString,
        description: this.searchString
      };
    }

    this.initiativeService.getEntity(Initiative.entity, options).subscribe(
      getResponse => {

        // update pagination
        const meta = getResponse.json().meta;
        this.pagination.update(meta);

        const resources = getResponse.json().resource;

        // flush old initiatives
        this.initiatives = [];

        for (const entry of resources) {
          const initiative = Initiative.fromJson(entry);

          // mark only those that are my own my own
          if (!this.supportedInitiatives) {
            {
              initiative.iAmAnOwner = true;
            }
          }
          // if(!initiative.closed){
          this.initiatives.push(initiative);
          // }
        }
        this.loading = false;
      }
    );
  }

  private intersect(a, b) {
    let result = [];

    for (let i of a) {
      for (let j of b) {
        if (i == j) {
          let unique = true;
          for (let k of result) {
            if (k === i) {
              unique = false;
            }
          }
          if (unique) {
            result.push(i);
          }
        }
      }
    }

    return result;
  }
}
