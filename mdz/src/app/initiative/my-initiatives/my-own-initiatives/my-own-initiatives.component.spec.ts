import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyOwnInitiativesComponent } from './my-own-initiatives.component';

describe('MyOwnInitiativesComponent', () => {
  let component: MyOwnInitiativesComponent;
  let fixture: ComponentFixture<MyOwnInitiativesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyOwnInitiativesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyOwnInitiativesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
