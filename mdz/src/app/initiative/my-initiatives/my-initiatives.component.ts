import {Component, OnInit, ViewChild, AfterContentInit} from '@angular/core';
import {InitiativeService} from '../services/initiative.service';
import {OrtsgemeindenService} from '../../admin/ortsgemeinden/ortsgemeinden.service';
import {HandlungsfelderService} from '../../admin/handlungsfelder/handlungsfelder.service';
import {MyOwnInitiativesComponent} from './my-own-initiatives/my-own-initiatives.component';

@Component({
  selector: 'app-my-initiatives',
  templateUrl: './my-initiatives.component.html',
  styleUrls: ['./my-initiatives.component.scss'],
  providers: [InitiativeService, OrtsgemeindenService, HandlungsfelderService]
})
export class MyInitiativesComponent implements AfterContentInit {

  /**
   * ViewChild for the list of Initiatives, that the user owns.
   */
  @ViewChild('ownInitiatives') ownInitiatives: MyOwnInitiativesComponent;

  /**
   * ViewChild for the list of Initiatives, that the user supports.
   */
  @ViewChild('supportedInitiatives') supportedInitiatives: MyOwnInitiativesComponent;

  private showMineTab = true;
  /**
   * Toggles the UI block, that tells the user he has no Initiatives started or supported.
   * @type {boolean}
   */
  private userDidntDoAnything = false;

  constructor(private inititativeService: InitiativeService) {
  }

  /**
   * Get the users Initiatives on init
   */
  ngAfterContentInit() {
    this.ownInitiatives.startLoading().then(
      () => {
        this.supportedInitiatives.startLoading().then(
          () => {
            // both ID lists are now loaded. Check in the service if there are IDs.
            // If not, the user has not done anything, yet.
            const myIDs = this.inititativeService.getMyInititaiveIDs();
            const supportedIDs = this.inititativeService.getMySupportedInitiativeIDs();

            this.userDidntDoAnything = (myIDs.length === 0 && supportedIDs.length === 0);
          }
        );
      }
    );
  }
}
