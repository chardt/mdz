import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import {StateService} from '@uirouter/angular';
import {Initiative} from '../model/initiative.model';
import {SupporterService} from '../services/supporter.service';
import {ShowService} from '../show/show.service';
import {DomSanitizer} from '@angular/platform-browser';
import {DREAMFACTORY_API_KEY, DREAMFACTORY_INSTANCE_URL} from '../../config/constants';
import {SessionService} from '../../user/session/services/session.service';

@Component({
  selector: 'app-single-initiative',
  templateUrl: './single-initiative.component.html',
  styleUrls: ['./single-initiative.component.scss'],
  providers: [SupporterService]
})
export class SingleInitiativeComponent implements OnInit {

  /**
   * Amount of characters to be left when a string is truncated.
   * @type {number}
   */
  private truncateLength = 800;

  /**
   * Used to prevend images from being cached in the browser.
   */
  private cacheKiller: string;

  private dateIsValid = false;
  private timeIsValid = false;

  /**
   * The current Initiative
   *
   * @type {Initiative}
   */
  @Input() initiative: Initiative;

  /**
   * Flag that should be TRUE when the Initiative is displayed to the admin.
   *
   * @type {boolean}
   */
  @Input() admin: boolean;

  /**
   * Shows a tiny representation of the initiative card.
   */
  @Input() tiny = false;

  /**
   * TRUE when the initiative is shown on the users dashboard.
   */
  @Input() dashboard: boolean;

  /**
   * Will fire when the Initiative has been marked as "Demo" to display it on the landing page.
   *
   * @type {EventEmitter}
   */
  @Output() onMarkAsDemo: EventEmitter<Initiative> = new EventEmitter();

  /**
   * Will fire when the Initiative will be blocked from public access or released to it.
   * @type {EventEmitter}
   */
  @Output() onBlock: EventEmitter<Initiative> = new EventEmitter();

  constructor(private sanitizer: DomSanitizer,
              private stateService: StateService,
              private initiativeShowService: ShowService,
              private sessionService: SessionService) {
    this.cacheKiller = Date.now() + '';
  }

  ngOnInit() {
    this.dateIsValid = !isNaN(this.initiative.date.getDate());
    this.timeIsValid = !isNaN(this.initiative.date.getHours());

    if (this.dateIsValid) {
      const year = this.initiative.date.getFullYear();
      this.dateIsValid = (year > 2016);
    }

    if(this.timeIsValid){
      const hours = this.initiative.date.getHours();
      const minutes = this.initiative.date.getMinutes();
      if(hours == minutes && hours == 0){
        this.timeIsValid = false;
      }
    }
  }

  /**
   * Bypasses an HTML String, that would otherwise be blocked.
   * @param html
   * @returns {SafeHtml}
   */
  public okHtml(html: string) {
    html = this.truncateDescription(html);
    return this.sanitizer.bypassSecurityTrustHtml(html);
  }

  private stateIsMyInitiatives() {
    return (this.stateService.current.name === 'my-initiatives');
  }

  /**+
   * Bypasses CSS styles, that would otherwise be blocked.
   * @param style
   * @returns {SafeStyle}
   */
  public okCss(style: string) {
    return this.sanitizer.bypassSecurityTrustStyle(style);
  }

  /**
   * Takes the Hero InitiativeImage path and composes an absolute URI with api access data.
   * @returns {any}
   */
  getHeroPath() {
    const hero = this.initiative.getHero();
    if (hero) {
      const fullPath =
        DREAMFACTORY_INSTANCE_URL + '/api/v2/files/' + hero.getPath() +
        '?ts_=' + this.cacheKiller + '&api_key=' + DREAMFACTORY_API_KEY +
        '&session_token=' + this.sessionService.getToken();
      return 'url(' + fullPath + ')';
    }

    return '';
  }

  /**
   * Sets the current Initiative on the InitiativeShowService and changes the state.
   */
  openInitiative() {
    if (this.admin || this.initiative.blocked) {
      return;
    }
    this.initiativeShowService.currentInitiative = this.initiative;
    this.stateService.go('show', {initiativeId: this.initiative.id});
  }

  /**
   * Goes to the initiative edit moduile
   */
  editInitiative() {
    if (this.admin) {
      return;
    }
    // this.initiativeShowService.currentInitiative = this.initiative;
    this.stateService.go('edit', {initiativeId: this.initiative.id});
  }


  /**
   * Sets the isDemo flag and emits the onMarkAsDemo Emitter.
   *
   * @param mark
   */
  markAsDemo(mark: boolean) {
    if (!this.admin) {
      return;
    }
    this.initiative.isDemo = mark;
    this.onMarkAsDemo.emit(this.initiative);
  }

  /**
   * Blocks the Initiative from being displayed to the public.
   */
  block(mark: boolean) {
    if (!this.admin) {
      return;
    }
    this.initiative.blocked = mark;
    this.onBlock.emit(this.initiative);
  }

  /**
   * Strips the given String to a maximum of 800 characters.
   * If stripped, '...' are appended.
   *
   * @param text
   * @returns {string}
   */
  private truncateDescription(text: string) {
    if (text) {
      const length = (this.dashboard) ? this.truncateLength / 2 : this.truncateLength;
      if (text.length > length) {
        text = text.substring(0, length) + ' ...';
      }
      return text;
    }
  }
}
