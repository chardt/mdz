import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleInitiativeComponent } from './single-initiative.component';

describe('SingleInitiativeComponent', () => {
  let component: SingleInitiativeComponent;
  let fixture: ComponentFixture<SingleInitiativeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleInitiativeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleInitiativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
