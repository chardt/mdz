export class FilterSettings {

  constructor(public handlungsfelder: any[],
              public ortsgemeinden: any[]) {
  }

  /**
   * Creates a string that conforms the the Dreamfactory Filter scheme.
   * Links all present Ortsgemeinden with "or" and returns the filter string.
   * @returns {string}
   */
  public getOrtsgemeindenFilter(): string {
    let filterString = '';
    for (const ortsgemeinde of this.ortsgemeinden) {
      if (filterString !== '') {
        filterString += 'or';
      }
      filterString += '(ortsgemeinde_id=' + ortsgemeinde + ')';
    }
    // filterString += "and(closed=false)and(published=true)&continue=true";
    return filterString;
  }

  /**
   * Creates a string that conforms the the Dreamfactory Filter scheme.
   * Links all present Handlungsfelder with "or" and returns the filter string.
   * @returns {string}
   */
  public getHandlungsfelderFilter(): string {
    let filterString = '';
    for (const handlungsfeld of this.handlungsfelder) {
      if (filterString !== '') {
        filterString += 'or';
      }
      filterString += '(handlungsfeld_id=' + handlungsfeld + ')';
    }
    // filterString += "and(closed=false)and(published=true)&continue=true";
    return filterString;
  }
}
