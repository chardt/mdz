export class FilterEntryOG {

  public id: string;

  constructor(id: number,
              public name: string) {
    this.id = "cb_og_"+id;
  }

  public toJson() {
    return {
      id: this.id,
      name: this.name
    }
  }
}