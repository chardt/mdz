import {Component, OnInit, Output, EventEmitter, ViewChild} from '@angular/core';
import {HandlungsfelderService} from '../../../admin/handlungsfelder/handlungsfelder.service';
import {OrtsgemeindenService} from '../../../admin/ortsgemeinden/ortsgemeinden.service';
import {Ortsgemeinde} from '../../../admin/ortsgemeinden/ortsgemeinde.model';
import {Handlungsfeld} from '../../../admin/handlungsfelder/handlungsfeld.model';
import {FilterSettings} from './filter.settings.model';
import {FilterEntryOG} from "./filter.entryog.model";
import {FilterEntryHF} from "./filter.entryhf.model";

@Component({
  selector: 'initiative-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {

  /**
   * ViewChild of the Ortsgemeinden Selector.
   * Needed to trigger internal methods.
   */
  @ViewChild('hms_ogs') hms_ogs_element;

  /**
   * ViewChild of the Handlungsfelder Selector.
   * Needed to trigger internal methods.
   */
  @ViewChild('hms_hfs') hms_hfs_element;

  /**
   * The available Ortsgemeinden
   * @type {any}
   */
  private ortsgemeinden: Ortsgemeinde[] = null;

  /**
   * The available Handlungsfelder
   * @type {any}
   */
  private handlungsfelder: Handlungsfeld[] = null;

  /**
   * The Ortsgemeinden that were selected by the user
   * @type {Array}
   */
  private selectedOrtsgemeinden = [];

  /**
   * The Handlungsfelder that were selected by the user
   * @type {Array}
   */
  private selectedHandlungsfelder = [];

  /**
   * Ortsgemeinden that are coming from the HyperSelect <select>
   * @type {Array}
   */
  private hyperInboundOGs = [];

  /**
   * Handlungsfelder that are coming from the HyperSelect <select>
   * @type {Array}
   */
  private hyperInbounHFs = [];

  private showOrtsgemeindenClose = false;
  private showHandlunsgfelderClose = false;
  /**
   * Emitted when the selection was changed
   * @type {EventEmitter}
   */
  @Output() filterChanged: EventEmitter<FilterSettings> = new EventEmitter();

  constructor(private handlungsfelderService: HandlungsfelderService,
              private ortsgemeindenService: OrtsgemeindenService) {
  }

  /**
   * Loads the Handlungsfelder and Ortsgemeinden, that are available, on Init
   */
  ngOnInit() {
    this.handlungsfelderService.list().then(
      (hf: Handlungsfeld[]) => {
        this.handlungsfelder = hf;

        for (const h of hf) {
          const id = 'cb_hf_' + h.getId();
          const value = h.getName();

          this.hyperInbounHFs.push({id: id, name: value});
        }
      }
    );
    this.ortsgemeindenService.list().then(
      (og: Ortsgemeinde[]) => {
        this.ortsgemeinden = og;

        for (const o of og) {
          const id = 'cb_og_' + o.getId();
          const value = o.getName();
          this.hyperInboundOGs.push({id: id, name: value});
        }
      }
    );
  }

  public preselectHandlungsfelder() {

  }

  /**
   * Triggers the collapse method of both hypermultiselects
   */
  collapse() {
    this.hms_ogs_element.collapse(true);
    this.hms_hfs_element.collapse(true);

    this.showOrtsgemeindenClose = false;
    this.showHandlunsgfelderClose = false;
  }

  public preselectOGs(arrayOfEntries: FilterEntryOG[]) {
    // clear the selected items
    this.hms_ogs_element.selectedItems = [];

    // loop through the entries to add
    for (const entry of this.hms_ogs_element.inboundSelector) {
      for (const preselect of arrayOfEntries) {
        if (entry.id === preselect.id) {
          entry['hyperMSSelected'] = true;
          this.hms_ogs_element.selectedItems.push(entry);
        }
      }
    }
    // emit the outbound selection
    this.hms_ogs_element.outbound.emit(this.hms_ogs_element.selectedItems);
  }

  public preselectHFs(arrayOfEntries: FilterEntryHF[]) {
    // clear the selected items
    this.hms_hfs_element.selectedItems = [];

    // loop through the entries to add
    for (const entry of this.hms_hfs_element.inboundSelector) {
      for (const preselect of arrayOfEntries) {
        if (entry.id === preselect.id) {
          entry['hyperMSSelected'] = true;
          this.hms_hfs_element.selectedItems.push(entry);
        }
      }
    }
    // emit the outbound selection
    this.hms_hfs_element.outbound.emit(this.hms_hfs_element.selectedItems);
  }

  private opened(hyperselect) {
    if (hyperselect.emptyTitle === 'Ortsgemeinden') {
      this.showOrtsgemeindenClose = true;
    }

    if (hyperselect.emptyTitle === 'Handlungsfelder') {
      this.showHandlunsgfelderClose = true;
    }
  }

  private closed(hyperselect) {
    if (hyperselect.emptyTitle === 'Ortsgemeinden') {
      this.showOrtsgemeindenClose = false;
    }

    if (hyperselect.emptyTitle === 'Handlungsfelder') {
      this.showHandlunsgfelderClose = false;
    }
  }

  /**
   * Fired by the HyperSelect when the selection was changed.
   * $event contains the selected Handlungsfelder.
   * The selection is mirrored in the selectedHandlungsfelder Array and Emitted to the parent by {@link changeSettings()}
   * @param $event
   */
  hyperOutboundHF($event) {
    this.selectedHandlungsfelder = [];
    for (const entry of $event) {
      this.selectedHandlungsfelder[entry.id] = true;
    }
    this.changeSettings();
  }

  /**
   * Fired by the HyperSelect when the selection was changed.
   * $event contains the selected Ortsgemeinden.
   * The selection is mirrored in the selectedOrtsgemeinden Array and Emitted to the parent by {@link changeSettings()}
   * @param $event
   */
  hyperOutboundOG($event) {
    this.selectedOrtsgemeinden = [];
    for (const entry of $event) {
      this.selectedOrtsgemeinden[entry.id] = true;
    }
    this.changeSettings();
  }

  /**
   * Takes an associative array and makes a new indexed array from the keys.
   * @param array with associative mapping
   * @returns {Array} with indexed mapping
   */
  keysToArray(array) {
    const ret = [];
    const keys = Object.keys(array);
    for (const key of keys) {
      ret.push(key.replace('cb_og_', '').replace('cb_hf_', ''));
    }
    return ret;
  }

  /**
   * Creates FilterSettings from the local selection and emits the Settings to the parent
   */
  changeSettings() {
    // get arrays with the selected checkbox ids
    const ogs = this.keysToArray(this.selectedOrtsgemeinden);
    const hfs = this.keysToArray(this.selectedHandlungsfelder);

    const filterSettings = new FilterSettings(hfs, ogs);

    // tell the others
    this.filterChanged.emit(filterSettings);
  }
}
