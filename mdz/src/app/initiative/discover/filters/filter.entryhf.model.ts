import {FilterEntryOG} from "./filter.entryog.model";
export class FilterEntryHF extends FilterEntryOG{

  constructor(id: number,
              public name: string) {
    super(id, name);
    this.id = "cb_hf_"+id;
  }
}