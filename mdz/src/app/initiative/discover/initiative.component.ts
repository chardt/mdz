import {Component, OnInit} from '@angular/core';
import {InitiativeService} from '../services/initiative.service';
import {OrtsgemeindenService} from '../../admin/ortsgemeinden/ortsgemeinden.service';
import {HandlungsfelderService} from '../../admin/handlungsfelder/handlungsfelder.service';
import {FilterSettings} from './filters/filter.settings.model';
import {Initiative} from "../model/initiative.model";

@Component({
  selector: 'app-initiative',
  templateUrl: './initiative.component.html',
  styleUrls: ['./initiative.component.scss'],
  providers: [InitiativeService, OrtsgemeindenService, HandlungsfelderService]
})
export class InitiativeComponent implements OnInit {

  /**
   * Title of the initiative.
   */
  private title: string;

  /**
   * HTML formatted description.
   */
  private description: string;

  /**
   * Georeference, latitude.
   */
  private latitude: string;

  /**
   * Georeference, longitude.
   */
  private longitude: string;

  /**
   * Offset for pagination.
   *
   * @type {number}
   */
  private offset = 0;

  /**
   * Limit for pagination.
   *
   * @type {number}
   */
  private limit = 5;

  /**
   * Flag to switch between paginated queries and unpaginated queries.
   * @type {boolean}
   */
  private usePagination = true;

  /**
   * Last set of FilterSettings to use with the infinite scroll directive.
   * @type {any}
   */
  private lastFilterSettings: FilterSettings = null;

  /**
   * Phases to search.
   * @type {any}
   */
  private searchString: string = null;

  /**
   * Number of available pages.
   *
   * @type {number}
   */
  private pages = 0;

  /**
   * Number of the current page.
   *
   * @type {number}
   */
  private page = 0;

  private showHistory = false;

  private history: Initiative[] = [];

  constructor(private initiativeService: InitiativeService) {
  }

  /**
   * Lists the initiatives on init.
   */
  ngOnInit() {
    this.initiativeService.foundEntries.subscribe(
      numberFound => {
        this.pages = Math.ceil(numberFound / this.limit);
        this.page = this.offset / this.limit + 1;
      }
    );
    this.list();
  }

  /**
   * Calls the initiative service to list the Initiatives.
   */
  list() {
    const paginationOptions = {
      offset: this.offset,
      limit: this.limit
    };

    // possibly override pagination settings
    // if (!this.usePagination) {
    //   paginationOptions.offset = 0;
    //   paginationOptions.limit = 0;
    // }

    let searchOptions = null;
    if (this.searchString) {
      searchOptions = {
        title: this.searchString,
        description: this.searchString
      };

      // when searching, reset pagination
      // paginationOptions.offset = 0;
    }

    this.initiativeService.list(this.lastFilterSettings, paginationOptions, searchOptions);
  }

  private loadHistory() {
    this.showHistory = true;
    if (this.history.length > 0) {
      return;
    }

    this.initiativeService.getHistory().subscribe(
      historyResponse => {
        const body = historyResponse.json();
        if (body && body.resource) {
          for (const entry of body.resource) {
            this.history.push(Initiative.fromJson(entry));
          }
        }
      },
      error => {

      }
    )
  }

  /**
   * Pagination method, use to load the next batch of Initiatives.
   */
  private next() {
    // if (!this.usePagination) return;
    if (this.page < this.pages) {
      this.offset += this.limit;
      this.list();
    }
  }

  /**
   * Pagination method, use to load the next batch of Initiatives.
   */
  private prev() {
    // if (!this.usePagination) return;
    if (this.page > 1) {
      this.offset -= this.limit;
      this.list();
    }
  }

  /**
   * When the Ortsgemeinden/Handlungsfelder Filter was changed the selection is sent to the initiativeService
   * to request a new list that reflects the filter settings.
   * @param settings
   */
  onFilterChanged(settings: FilterSettings) {

    // disable pagination when filter is used
    // this.usePagination = (settings.handlungsfelder.length === 0 && settings.ortsgemeinden.length === 0);

    this.lastFilterSettings = settings;

    this.offset = 0;

    this.list();
  }

  private onKeyUp(event) {
    // load on enter
    if (event.keyCode === 13) {
      this.offset = 0;
      this.list();
    } else {
      // also load when NOT enter, but search string is empty
      if (this.searchString.length === 0) {
        this.offset = 0;
        this.list();
      }
    }
  }
}
