import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {User} from '../../user/model/user.model';
import {OtherUsersService} from '../../user/other-users.service';
import {ProfileImageService} from '../../common/profile-image/profile-image.service';
import {DomSanitizer} from '@angular/platform-browser';
import {StateService} from '@uirouter/angular';
import {DEFAULT_USER_IMAGE_URL} from '../../config/constants';

@Component({
  selector: 'initiative-supporter',
  templateUrl: './supporter.component.html',
  styleUrls: ['./supporter.component.scss']
})
export class SupporterComponent implements OnInit {

  /**
   * The User that supports an Initiative
   */
  @Input() user: User;

  /**
   * Optional, used as entity ID instead of that from the User object.
   * Makes the Component a little mor versatile.
   */
  @Input() overrideId: number;

  /**
   * True, if the User is the Creator or an Admin of the Initiative.
   */
  @Input() isInitiator: boolean;

  /**
   *
   * @type {EventEmitter}
   */
  @Output() userInactive: EventEmitter<number> = new EventEmitter();

  /**
   * URL to the profile image
   */
  private profileImage: string;

  constructor(private otherUserService: OtherUsersService,
              private profileImageService: ProfileImageService,
              private statesService: StateService,
              private domSanitizer: DomSanitizer) {
  }

  /**
   * Gets the User data on init
   */
  ngOnInit() {
    this.getData();
  }


  /**
   * Bypasses CSS security.
   *
   * @param path
   * @returns {SafeStyle}
   */
  path(path: string) {
    return this.domSanitizer.bypassSecurityTrustStyle('url(' + path + ')');
  }

  /**
   * Transitions to the supporting Users public profile.
   */
  private openProfile() {
    this.statesService.go('user', {username: this.user.username});
  }

  /**
   * GETs the User from the API.
   * If the user is active, the image is loaded.
   * It it is marked inactive, an Event is emitted to tell the parent component.
   */
  private getData() {

    // username not present, need to download the user data
    if (!this.user.username) {

      // decide if we need to use the override id or just the normal user id
      const id = (this.overrideId) ? this.overrideId : this.user.id;

      this.otherUserService.getUserById(id).subscribe(
        response => {
          const user = User.fromJson(response.json());
          if (user.is_active) {
            this.user = user;
            this.user.isInitiator = this.isInitiator;
            this.getImage();
          } else {
            this.userInactive.emit(user.id);
          }
        }
      );
    }
  }

  /**
   * GETs the Users Image path from the Profile, composes the absolute URI and sets it in the Supporter model.
   */
  private getImage() {
    // get the profile
    this.otherUserService.getProfile(this.user.id).subscribe(
      response => {
        const profile = response.json().resource[0];
        let imagePath = profile.image;

        if (profile.avatar_id) {
          imagePath = profile.avatar_id;
        }
        if (imagePath) {

          this.profileImage = this.profileImageService.composePath(imagePath);
        } else {
          this.profileImage = DEFAULT_USER_IMAGE_URL;
        }
      }
    );
  }

}
