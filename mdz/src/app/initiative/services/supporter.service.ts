import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import * as constants from '../../config/constants';
import {Deferred} from '../../common/Deferred';
import {BaseHttpService} from '../../common/services/BaseHttp.service';
import {SessionService} from '../../user/session/services/session.service';
import {Observable} from 'rxjs/Observable';
import {TranstoastService} from '../../common/transtoast.service';

@Injectable()
export class SupporterService extends BaseHttpService {

  /**
   * Reference to the Session Service
   */
  private sessionService;

  constructor(http: Http,
              toastr: TranstoastService,
              sessionService: SessionService) {
    super(http, toastr);
    this.sessionService = sessionService;
  }

  /**
   * To get the Supporters of one specific initiative. Requests a table with only the initiative ID and the supporting
   * User IDs.
   *
   * @param initiativeId
   * @returns {Observable<Response>}
   */
  public getSupporters(initiativeId: number): Observable<any> {
    const endpoint = this.entityFor('user_supports_initiative?fields=id,user_id&filter=initiative_id=' + initiativeId);
    return this.http.get(endpoint, this.getDefaultHttpOptions());
  }

  /**
   * Lets the current User support an initiative.
   *
   * @param initiativeId
   * @returns {Observable<Response>}
   */
  public support(initiativeId: number): Promise<any> {

    const d = new Deferred();

    const combination = {
      user_id: this.sessionService.getCurrentUser().id,
      initiative_id: initiativeId
    };

    this.checkIfSupportIsPossible(combination).then(
      () => {
        this.postEntity(combination, 'user_supports_initiative')
          .subscribe(
            response => {
              d.resolve(response);
            },
            rejected => {
              d.reject(rejected);
            }
          );
      },
      () => {
        d.reject('Du unterstützt diese Initiative Bereits.');
      }
    );

    return d.promise;
  }

  /**
   * GETs the combination of user/initiative from the supporters table.
   * If there are none, the user can support the inititative.
   * Otherwise the promise is rejected.
   *
   * @param combination
   * @returns {Promise<T>}
   */
  private checkIfSupportIsPossible(combination: any): Promise<any> {
    const d = new Deferred();

    const options = {
      filter: combination
    };

    this.getEntity('user_supports_initiative', options).subscribe(
      response => {
        const supporters = response.json().resource;
        if (supporters.length === 0) {
          d.resolve();
        } else {
          d.reject();
        }
      },
      error => {
        d.reject(error);
      }
    );

    return d.promise;
  }

  /**
   * DELETEs the support relation between the current User and the Initiative by deleting the relation entry itself.
   * @param supportId
   * @returns {Observable<Response>}
   */
  public stopSupport(supportId: number): Observable<any> {
    const endpoint = '/api/v2/mysql/_table/user_supports_initiative/' + supportId;
    return this.http.delete(constants.DREAMFACTORY_INSTANCE_URL + endpoint, this.getDefaultHttpOptions());
  }

}
