import { TestBed, inject } from '@angular/core/testing';

import { InitiativeService } from './initiative.service';

describe('InitiativeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InitiativeService]
    });
  });

  it('should ...', inject([InitiativeService], (service: InitiativeService) => {
    expect(service).toBeTruthy();
  }));
});
