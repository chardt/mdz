import {Injectable} from '@angular/core';
import {Initiative} from '../model/initiative.model';
import {Http} from '@angular/http';
import {SessionService} from '../../user/session/services/session.service';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Deferred} from '../../common/Deferred';
import {BaseHttpService} from '../../common/services/BaseHttp.service';
import {FilterSettings} from '../discover/filters/filter.settings.model';
import {InitiativeImage} from '../initiative-image.model';
import {TranstoastService} from '../../common/transtoast.service';

@Injectable()
export class InitiativeService extends BaseHttpService {

  /**
   * Currently loaded initiatives
   * @type {Array}
   */
  initiatives: Initiative[] = [];

  /**
   * A list of IDs, that is used when Initiatives are fetched. If specified, only Initiatives with these IDs will be returned.
   * @type {any}
   */
  filterIDs: number[] = null;

  /**
   * List of numbers, which are the IDs of the Initiatives, that the user owns.
   * @type {Array}
   */
  private myInititaiveIDs: number[] = [];

  /**
   * List of numbers, which are the IDs of the Initiatives, that the user supports.
   * @type {Array}
   */
  private supportedInititaiveIDs: number[] = [];

  /**
   * Indicates whether more initiatives, that fit the current filter, are available.
   * @type {boolean}
   */
  public moreAvailable = true;

  public entries: BehaviorSubject<number> = new BehaviorSubject(null);
  public foundEntries: Observable<number> = this.entries.asObservable();

  /**
   * Store for the pagination settings with limit and offset. The Service will call list() on it's own without parameters.
   * In this case, the users pagination settings must be used, thus they are preserved here until overwritten.
   * @type {any}
   */
  private paginationOptions: any = null;


  constructor(protected http: Http,
              protected toastr: TranstoastService,
              protected sessionService: SessionService) {
    super(http, toastr);
  };

  /**
   * Adds a single ID or an array of IDs to the list of Initiative IDs that the user owns.
   * @param id
   */
  public addInititaiveIdToOwned(id: number|number[]) {
    if (typeof id === 'number') {
      this.addUniqueInititaiveIdToOwned(id);
    } else {
      id.forEach((entry) => {
        this.addUniqueInititaiveIdToOwned(entry);
      });
    }
  }

  /**
   * Makes sure that the ID is not already in the list and adds it, if possible.
   * @param id
   */
  private addUniqueInititaiveIdToOwned(id: number) {
    for (const entry of this.myInititaiveIDs) {
      if (entry === id) {
        // not unique, cancel.
        return;
      }
    }
    this.myInititaiveIDs.push(id);
  }

  /**
   * Adds a single ID or an array of IDs to the list of Initiative IDs that the user owns.
   * @param id
   */
  public addInititaiveIdToSupported(id: number|number[]) {
    if (typeof id === 'number') {
      this.addUniqueInititaiveIdToSupported(id);
    } else {
      id.forEach((entry) => {
        this.addUniqueInititaiveIdToSupported(entry);
      });
    }
  }

  /**
   * Makes sure that the ID is not already in the list and adds it, if possible.
   * @param id
   */
  private addUniqueInititaiveIdToSupported(id: number) {
    let add = true;

    // test if already in the array
    for (const entry of this.supportedInititaiveIDs) {
      if (entry === id) {
        add = false;
        break;
      }
    }

    // test if it is in the "owned" array
    if (add) {
      for (const entry of this.myInititaiveIDs) {
        if (entry === id) {
          return;
        }
      }
    } else {
      return;
    }

    // when the method has not exited, the id is unique and not owned, so add it.
    this.supportedInititaiveIDs.push(id);
  }

  /**
   * Getter for myInititaiveIDs.
   * @returns {number[]}
   */
  public getMyInititaiveIDs() {
    return this.myInititaiveIDs;
  }

  /**
   * Getter for supportedInititaiveIDs.
   * @returns {number[]}
   */
  public getMySupportedInitiativeIDs() {
    return this.supportedInititaiveIDs;
  }

  /**
   * Initiatives can be connected to Handlungsfelder and Ortsgemeinden.
   * Connections of that kind hold the connected Initiative ID. Here it is extracted and the list of IDs is returned.
   * @param resource
   * @returns {Array}
   */
  private initiativeIDsFromRelationResource(resource: any) {
    const ids = [];
    for (const relation of resource) {
      ids.push(+relation.initiative_id);
    }
    // return [16,18,19,23,24,29,30,34];
    return ids;
  }

  /**
   * Fetches the IDs of all Initiatives, that are connected to the Ortsgemeinden of the given FilterSettings.
   * It then requests a list()
   * @param filter
   */
  private initiativeIDsByOrtsgemeindenFilter(filter: FilterSettings, q?: Deferred<any>) {

    // reset filter IDs
    this.filterIDs = [];

    const options = {
      filter: filter.getOrtsgemeindenFilter()
    };
    this.getEntity(Initiative.entityRelationOrtsgemeinde, options)
      .subscribe(
        response => {
          // save globally
          this.filterIDs = this.initiativeIDsFromRelationResource(response.json().resource);
          if (q) {
            q.resolve(this.filterIDs);
          } else {
            this.list();
          }
        },
        errorResponse => {
          this.toastr.info(errorResponse.json().message);
          if (q) {
            q.reject(errorResponse.json().message);
          }
        }
      );
    if (q) {
      return q.promise;
    }
  }

  /**
   * Fetches the IDs of all Initiatives, that are connected to the Handlungsfelder of the given FilterSettings.
   * It then requests a list()
   * @param filter
   */
  private initiativeIDsByHandlungsfelderFilter(filter: FilterSettings, q?: Deferred<any>) {

    // reset filter IDs
    this.filterIDs = [];

    const options = {
      filter: filter.getHandlungsfelderFilter()
    };
    this.getEntity(Initiative.entityRelationHandlungsfeld, options)
      .subscribe(
        response => {
          // save globally
          this.filterIDs = this.initiativeIDsFromRelationResource(response.json().resource);
          if (q) {
            q.resolve(this.filterIDs);
          } else {
            this.list();
          }
        },
        errorResponse => {
          this.toastr.info(errorResponse.json().message);
          if (q) {
            q.reject(errorResponse.json().message);
          }
        }
      );
    if (q) {
      return q.promise;
    }
  }

  /**
   * GETs the IDs of all Initiatives that are connected to both, the Ortsgemeinden and the Handlungsfelder from the
   * FilterSettings. It loads the relations after one another, merges the resulting lists of IDs and lists those Initiatives.
   * @param filter
   */
  private initiativeIDsByBothFilters(filter: FilterSettings, q?: Deferred<any>) {

    // reset filter IDs
    this.filterIDs = [];

    const options = {
      filter: filter.getOrtsgemeindenFilter()
    };

    this.getEntity(Initiative.entityRelationOrtsgemeinde, options)
      .subscribe(
        response => {

          this.filterIDs = this.initiativeIDsFromRelationResource(response.json().resource);

          const hfOptions = {
            filter: filter.getHandlungsfelderFilter()
          };

          this.getEntity(Initiative.entityRelationHandlungsfeld, hfOptions)
            .subscribe(
              hfResponse => {

                const hfIds = this.initiativeIDsFromRelationResource(hfResponse.json().resource);
                this.filterIDs = this.mergeIDLists(this.filterIDs, hfIds);

                if (q) {
                  q.resolve(this.filterIDs);
                } else {
                  this.list();
                }

              },
              errorResponse2 => {
                this.toastr.info(errorResponse2.json().message);
                if (q) {
                  q.reject(errorResponse2.json().message);
                }
              }
            );
        },
        errorResponse => {
          this.toastr.info(errorResponse.json().message);
          if (q) {
            q.reject(errorResponse.json().message);
          }
        }
      );
    if (q) {
      return q.promise;
    }
  }

  /**
   * Merges two arrays of numbers and makes the members unique.
   * @param listOne
   * @param listTwo
   * @returns {number[]}
   */
  private mergeIDLists(listOne, listTwo): number[] {
    // merge both ID lists
    const merged: number[] = [];
    // let firstBatch = this.filterIDs;
    for (const id of listOne) {
      let match = false;
      for (const id2 of listTwo) {
        if (id === id2) {
          match = true;
          // ensure no duplicates
          if (merged.indexOf(id) < 0) {
            merged.push(id);
          }
          break;
        }
      }
    }

    return merged;
  }

  /**
   * Analyzes the FilterSettings and loads either the Handlungsfelder Relation, the Ortsgemeinden Relation or both.
   * It directly calls list() without any filters applied, when the FilterSettings do not contain any filters.
   * @param filter
   * @param idsOnly When true, it does not call list() but only returns the IDs
   * @returns {undefined}
   */
  public initiativeIDsByFilter(filter: FilterSettings, idsOnly?: boolean) {

    // when the IDs only are needed, create deferred that is resolved, when the magic has happened.
    let q = null;
    if (idsOnly) {
      q = new Deferred();
    }

    // when the filter is actually empty, just list.
    if (filter.ortsgemeinden.length === 0 && filter.handlungsfelder.length === 0) {
      this.filterIDs = null;
      if (idsOnly) {
        return q.promise;
      }
      this.list();
    }

    // check which filter is active
    const ortsgemeindenFilterIsActive: boolean = (filter.ortsgemeinden.length > 0);
    const handlugnsfelderFilterIsActive: boolean = (filter.handlungsfelder.length > 0);

    if (ortsgemeindenFilterIsActive && !handlugnsfelderFilterIsActive) {
      // list with only ortsgemeinden filter
      return this.initiativeIDsByOrtsgemeindenFilter(filter, q);
    }

    if (!ortsgemeindenFilterIsActive && handlugnsfelderFilterIsActive) {
      // list with only handlungsfelder
      return this.initiativeIDsByHandlungsfelderFilter(filter, q);
    }

    if (ortsgemeindenFilterIsActive && handlugnsfelderFilterIsActive) {
      // list with both
      return this.initiativeIDsByBothFilters(filter, q);
    }
  }

  /**
   * Lists Initiatives
   *
   * @param keyword
   * @param paginationSettings
   * @returns {Observable<any>}
   */
  search(keyword?: string, paginationSettings?: any) {

    if (keyword) {
      paginationSettings['search'] = {
        title: keyword,
        description: keyword
      };
    }

    paginationSettings['includeCount'] = true;

    return this.getEntity(Initiative.entity, paginationSettings);
  }

  /**
   * GETs all Initiatives that have the is_demo flag from the API
   * @returns {Observable<any>}
   */
  getFeatured() {
    const options = {
      filter: {
        is_demo: true
      },
      related: [
        Initiative.entityRelationImages
      ]
    };

    return this.getEntity(Initiative.entity, options);
  }

  /**
   * Retrieves one Inititaive by it's ID and only when it's published.
   * @param id
   * @returns {Observable<any>}
   */
  public publishedInitiativesByID(id: number) {
    const options = {
      ids: [id],
      filter: {
        published: true
      }

    };
    return this.getEntity(Initiative.entity, options);
  }

  /**
   * Lists the Initiatives. If FilterSettings are specified it passes the request to initiativeIDsByFilter(), first.
   * The final GET Request has the _continue_ option, which leads to the error message holding Initiatives in case of
   * a backend error, where not all requested Initiatives could be fetched, due to access restrictions.
   * In that case, the Error Response hold all Initiatives, that *could* be fetched and is thus consumed.
   *
   * @param settings
   * @returns {undefined|undefined|undefined|undefined}
   */
  list(settings?: FilterSettings, paginationOptions?: any, searchOptions?: any) {
    // if present, store the pagination settings for the next run.
    // they are used until overwritten from outside.

    if (!paginationOptions) {
      paginationOptions = this.paginationOptions;
    } else {
      this.paginationOptions = paginationOptions;
    }

    if (settings) {
      return this.initiativeIDsByFilter(settings);
    }

    const options = {
      related: [
        Initiative.entityRelationOwners,
        Initiative.entityRelationSupporters,
        Initiative.entityRelationOrtsgemeindenEntity,
        Initiative.entityRelationHandlungsfelderEntity,
        Initiative.entityRelationImages
      ],
      limit: 0,
      offset: 0,
      filter: {
        closed: false,
        published: true,
        blocked: false
      },
      // continue: true,
      includeCount: true
    };

    if (searchOptions) {
      options['search'] = searchOptions;
    }

    if (paginationOptions) {
      options.limit = paginationOptions.limit;
      options.offset = paginationOptions.offset;
    }

    if (options.offset === 0) {
      this.initiatives = [];
    }

    if (this.filterIDs && this.filterIDs.length > 0) {
      options['ids'] = this.filterIDs.join(',');
    }

    if (this.filterIDs && this.filterIDs.length === 0) {
      this.toastr.info('Für diese Kombination sind keine Initiativen verfügbar. Legen Sie eine an!');
      this.initiatives = [];
      return;
    }

    options['order'] = {
      by: 'id',
      direction: 'desc'
    };

    // console.log('Listing with these pagination settings', paginationOptions);
    this.getEntity(Initiative.entity, options).subscribe(
      response => {
        // this.initiatives = [];
        const body = response.json();
        if (body.meta) {
          this.entries.next(body.meta.count);
        }
        this.initiatives = [];
        for (const obj of body.resource) {

          const i: Initiative = Initiative.fromJson(obj);
          if (i.published && !i.closed) {
            this.initiatives.push(i);
          }
          // else{
          //   console.log('Initiative is not published or closed', i);
          // }
        }
      },
      error => {
        // extract initiative from error message when there are any
        const err = error.json();
        // this.initiatives = [];
        if (err.error.context !== undefined) {
          if (err.error.context.resource !== undefined) {
            for (const entry of err.error.context.resource) {
              if (entry.code === undefined) {
                // console.log('Found:', entry);
                const i: Initiative = Initiative.fromJson(entry);
                if (i.published && !i.closed) {
                  this.initiatives.push(i);
                  // console.log('pushed');
                }
                // else{
                //   console.log('but didnt use it');
                // }
              }
              // else {
              // this.toastr.error(entry.message);
              // console.log('discard:', entry);
              // }
            }
          }
        }
      }
    );
  }

  /**
   * Waits for a User to be present. Then loads the Users Initiative IDs and resolves the promise to deliver those.
   * Can be configure to fetch the Initiatives the user owns or those he supports, by setting _support_ to true.
   *
   * @returns {Promise<T>}
   */
  public getMyInitiatives(support?: boolean, userId?: number): Promise<any> {
    const d = new Deferred();

    // subscribe to the user data
    this.sessionService.user$.subscribe((user) => {

      // as soon as the user has been delivered (-> no null)
      if (user != null) {
        const options = {
          filter: {
            user_id: (userId) ? userId : user.id,
          }
        };

        let table = 'user_owns_initiative';
        if (support) {
          table = 'user_supports_initiative';
        }

        const observable = this.getEntity(table, options);
        d.resolve(observable);
      }
    });
    return d.promise;
  }

  /**
   * Invites all the friends to the Initiative.
   * @param emails
   * @param initiativeId
   */
  public inviteFriends(emails: string, initiativeId: number, initiativeName: string, message: string): Observable<any> {

    const user = this.sessionService.getCurrentUser();
    const inviter = user.first_name + ' ' + user.last_name;
    const endpoint = this.endpoint('/invite');
    const options = this.getDefaultHttpOptions();

    const payload = {
      emails: emails,
      id: initiativeId,
      initiativeName: initiativeName,
      inviter: inviter,
      message: message
    };

    return this.http.post(endpoint, payload, options);
  }

  private filterRequestOptions(initiativeId: number, filterId: number, connection: string, forGET?: boolean) {
    // first GET the connection ID
    const details = {
      initiative_id: initiativeId
    };

    switch (connection) {
      case Initiative.entityRelationOrtsgemeinde: {
        details['ortsgemeinde_id'] = filterId;
      }
        break;
      case Initiative.entityRelationHandlungsfeld: {
        details['handlungsfeld_id'] = filterId;
      }
        break;
    }

    if (forGET) {
      return {
        filter: details
      }
    } else {
      return details;
    }
  }

  /**
   * Posts a new connection between an Initiative and an Ortsgemeinden/Handlungsfeld filter.
   * @param initiativeId
   * @param filterId
   * @param connection
   */
  public addFilter(initiativeId: number, filterId: number, connection: string) {
    const options = this.filterRequestOptions(initiativeId, filterId, connection);

    this.postEntity(options, connection).subscribe(
      postResponse => {
        console.log(postResponse.json());
      }
    );
  }

  /**
   * Removes a connection between an Initiative and a Ortsgemeinden/Handlungsfeld filter.
   * @param initiativeId
   * @param filterId
   * @param connection
   */
  public removeFilter(initiativeId: number, filterId: number, connection: string) {

    // first GET the connection ID
    const options = this.filterRequestOptions(initiativeId, filterId, connection, true);

    this.getEntity(connection, options).subscribe(
      getResponse => {
        const resource = getResponse.json().resource;
        if (resource && resource.length > 0) {
          const id = resource[0].id;
          this.deleteEntity({id: id}, connection).subscribe(
            deleteResponse => {
              console.log(deleteResponse.json());
            }
          );
        }
      }
    );
  }

  /**
   * GETs the ownership relation of a given Initiative.
   * @param initiative
   * @returns {Observable<any>}
   */
  getOwnersOfInitiative(initiative: Initiative): Observable<any> {
    const options = {
      filter: {
        initiative_id: initiative.id
      }
    };
    return this.getEntity('user_owns_initiative', options);
  }

  /**
   * PATCHes an initiative so it is no longer published and closed, effectively hiding it from all Users.
   * @param id
   */
  closeInitiative(id: number, successfull?: boolean, sohoDelete?: boolean) {
    const endpoint = this.entityFor('initiative/');
    const options = this.getDefaultHttpOptions();

    const payload = {
      resource: [
        {
          closed: true,
          id: id
        }
      ]
    };

    if(sohoDelete){
      payload.resource[0]['visible'] = false;
    }

    if (successfull) {
      payload.resource[0]['successfull'] = true;
    }

    this.http.patch(endpoint, payload, options)
      .subscribe(
        (response) => {
          this.toastr.success('Die Initiative wurde geschlossen.');
        },
        (errorResponse) => {
          this.toastr.error(errorResponse.json().error.message);
        }
      );
  }

  /**
   * GETs all the initiatives that are no longer published AND successfull.
   * It's the history of all the successfull initiatives.
   * @returns {Observable<any>}
   */
  getHistory() {
    const options = {
      related: [
        Initiative.entityRelationOwners,
        Initiative.entityRelationSupporters,
        Initiative.entityRelationOrtsgemeindenEntity,
        Initiative.entityRelationHandlungsfelderEntity,
        Initiative.entityRelationImages
      ],
      filter: {
        successfull: true,
        published: true,
        blocked: false
      },
      sort: {
        by: 'updated',
        dir: 'DESC'
      }
    };
    return this.getEntity(Initiative.entity, options);
  }

  /**
   * POSTs a new InitiativeImage Model to the API.
   *
   * @param image
   * @param initiativeId
   * @param asHero
   * @returns {Observable<any>}
   */
  postImage(image: any, initiativeId: number, asHero: boolean = false) {

    console.error('postImage', image);
    const path = image.path;
    const ii = new InitiativeImage(path, initiativeId, asHero);

    return this.postEntity(ii.toJson(), InitiativeImage.entity);
  }

  /**
   * PATCHes the given Initiative to be either published or unpublished.
   * @param initiative
   * @param publish
   * @returns {Observable<any>}
   */
  publishInitiative(initiative: Initiative, publish: boolean) {
    const patch = {
      published: publish
    };

    return this.patchEntity(patch, Initiative.entity + '/' + initiative.id);
  }

  /**
   * PATCHes the given Initiative
   * @param initiative
   */
  updateInitiative(initiative: Initiative) {

    // take only the base data
    const patch = new Initiative(
      initiative.id,
      initiative.title,
      initiative.latitude,
      initiative.longitude,
      initiative.description,
      initiative.date,
      initiative.isDemo,
      initiative.closed,
      initiative.published,
      initiative.blocked,
      initiative.is_event,
      initiative.successfull
    ).toJson();

    delete patch.iAmAnOwner;

    this.patchEntity(patch, Initiative.entity + '/' + patch.id).subscribe(
      () => {
        // this.toastr.success('Änderungen gespeichert');
      },
      errorResponse => {
        this.toastr.error(errorResponse.json().error.message);
      }
    );
  }
}
