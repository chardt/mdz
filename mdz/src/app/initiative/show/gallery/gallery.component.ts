import {Component, Input} from '@angular/core';
import {Initiative} from '../../model/initiative.model';
import {DomSanitizer} from '@angular/platform-browser';
import {SessionService} from '../../../user/session/services/session.service';
import {DREAMFACTORY_INSTANCE_URL, DREAMFACTORY_API_KEY} from '../../../config/constants';
import {InitiativeImage} from '../../initiative-image.model';

@Component({
  selector: 'initiative-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent {

  /**
   * URL of the image, that should be shown in the lightbox.
   * @type {any}
   */
  private lightboxImageUrl: string = null;

  /**
   * Index of the image, currently shown in the lightbox.
   * @type {any}
   */
  private currentIndex: number = null;


  /**
   * The current Initiative.
   */
  @Input() initiative: Initiative;

  /**
   * TimeStamp that is going to be create when the component is initialized.
   * Used to seed image URLs with a cache-killing query string.
   * @type {any}
   */
  private cacheKiller: string = null;

  constructor(private domSanitizer: DomSanitizer,
              private sessionService: SessionService) {

    this.cacheKiller = '' + Date.now();
  }

  /**
   * Takes a relative API path and composes an absoulte API path.
   * Adds the token dynamically.
   *
   * @param path
   * @param notForCss
   * @returns {string}
   */
  private fullPathFromPath(path: string, notForCss?: boolean) {
    const token = this.sessionService.getToken();
    let fullPath = DREAMFACTORY_INSTANCE_URL + '/api/v2/files/' + path + '?ts_=' + this.cacheKiller + '&api_key=' + DREAMFACTORY_API_KEY;
    if (token) {
      fullPath += '&session_token=' + token;
    }
    if (!notForCss) {
      fullPath = 'url(' + fullPath + ')';
    }
    return fullPath;
  }

  /**
   * Bypasses CSS security.
   *
   * @param path
   * @returns {SafeStyle}
   */
  path(path: string) {
    return this.domSanitizer.bypassSecurityTrustStyle(this.fullPathFromPath(path));
  }

  /**
   * Loads the previous image to the lighbox, except when there is none.
   */
  prev() {
    if (this.currentIndex !== null) {
      // reduce index
      this.currentIndex--;
      // set to 0 if it already was the fist
      if (this.currentIndex < 0) {
        this.currentIndex = 0;
      }

      this.open(this.currentIndex);
    }
  }

  /**
   * Loads the next image to the lightbox, except when there is none.
   */
  next() {
    if (this.currentIndex !== null) {
      // increase index
      this.currentIndex++;
      // set to last, if it already was the last
      if (this.currentIndex > this.initiative.getImages().length - 1) {
        this.currentIndex = this.initiative.getImages().length - 1;
      }

      this.open(this.currentIndex);
    }
  }

  /**
   * Opens the image with the given index in the lightbox.
   * @param index
   */
  open(index) {

    this.currentIndex = index;
    const ii: InitiativeImage = InitiativeImage.fromJson(this.initiative.getImages()[index]);
    this.lightboxImageUrl = this.fullPathFromPath(ii.getPath(), true);
  }

  /**
   * Closes the lightbox.
   *
   * @param $event
   */
  close($event) {
    // close always when the argument is undefined (not given)
    // also also when user clicks ob the lightbox backdrop
    if ($event === undefined || $event.target.className === 'lightbox') {
      this.lightboxImageUrl = null;
    }
  }

}
