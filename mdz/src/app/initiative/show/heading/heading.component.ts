import {Component, OnInit, Input} from '@angular/core';
import {Initiative} from '../../model/initiative.model';
import {DomSanitizer} from '@angular/platform-browser';
import {InitiativeService} from '../../services/initiative.service';
import {ToastsManager} from 'ng2-toastr';
import {DREAMFACTORY_INSTANCE_URL, DREAMFACTORY_API_KEY, COMMUNE_OUTLINES} from '../../../config/constants';
import {SessionService} from '../../../user/session/services/session.service';
import {FacebookService, InitParams} from 'ngx-facebook';
import {TranstoastService} from '../../../common/transtoast.service';
import {CmsService} from '../../../admin/cms/cms.service';

@Component({
  selector: 'initiative-heading',
  templateUrl: './heading.component.html',
  styleUrls: ['./heading.component.scss']
})
export class HeadingComponent implements OnInit {

  private outlines = null;
  /**
   * Latitude part of the initiatives coordinates.
   * @type {number}
   */
  private lat = 40;

  /**
   * Longitude part of the initiatives coordinates.
   * @type {number}
   */
  private lon = 7;

  private georeferenceIsManual = false;

  private dateIsValid = false;
  private timeIsValid = false;
  /**
   * The current initiative.
   */
  @Input() initiative: Initiative;

  /**
   * TimeStamp that is going to be create when the component is initialized.
   * Used to seed image URLs with a cache-killing query string.
   * @type {any}
   */
  private cacheKiller: string = null;

  constructor(private sanitizer: DomSanitizer,
              private initiativeService: InitiativeService,
              private toastr: TranstoastService,
              private sessionService: SessionService,
              private facebookService: FacebookService) {

    this.cacheKiller = '' + Date.now();

    const initParams: InitParams = {
      appId: '',
      xfbml: true,
      version: 'v2.8'
    };
    facebookService.init(initParams);
  }

  /**
   * Makes sure the lat and lon variables are really of type number.
   */
  ngOnInit() {

    if (
      !this.initiative.latitude || !this.initiative.longitude ||
      +this.initiative.latitude === 0 ||
      +this.initiative.longitude === 0) {
      this.georeferenceIsManual = true;
    }

    if (this.georeferenceIsManual) {
      this.lat = 49.866667;
      this.lon = 7.983333;
      this.getOrtsgemeindenToDisplay();
    } else {
      this.lat = +this.initiative.latitude;
      this.lon = +this.initiative.longitude;
    }

    // check date validity
    this.dateIsValid = !isNaN(this.initiative.date.getDate());
    this.timeIsValid = !isNaN(this.initiative.date.getHours());
    if (this.dateIsValid) {
      const year = this.initiative.date.getFullYear();
      this.dateIsValid = (year > 2016);
    }

    if(this.timeIsValid){
      const hours = this.initiative.date.getHours();
      const minutes = this.initiative.date.getMinutes();
      if(hours == minutes && hours == 0){
        this.timeIsValid = false;
      }
    }
  }

  /**
   * Requests to publish the Initiative on the API.
   */
  onPublishPressed() {
    this.initiativeService.publishInitiative(this.initiative, true).subscribe(
      () => {
        const kind = (this.initiative.is_event) ? 'Aktion' : 'Initiative';
        this.toastr.success('Die ' + kind + ' ist nun veröffentlicht');
        this.initiative.published = true;
      },
      errorResponse => {
        this.toastr.error(errorResponse.json().error.message);
      }
    );
  }

  /**
   * Takes the outlines of the communes to display and stores them in the initiative.
   */
  private getOrtsgemeindenToDisplay() {
    const communes = [];
    for (const commune of this.initiative.getOrtsgemeinden()) {
      const c: any = commune; // allow transpiler to overlook the typing...
      for (const outline of COMMUNE_OUTLINES) {
        if (c.name === 'Verbandsgemeinde') {
          // just show them all
          this.outlines = COMMUNE_OUTLINES;
          return;
        }
        if (outline.name === c.name) {
          communes.push(outline);
        }
      }
    }
    this.outlines = communes;
  }

  /**
   * Bypasses html strings security.
   * @param html
   * @returns {SafeHtml}
   */
  public okHtml(html: string) {
    return this.sanitizer.bypassSecurityTrustHtml(html);
  }

  /**
   * Bypasses css security.
   * @param style
   * @returns {SafeStyle}
   */
  public okCss(style: string) {
    return this.sanitizer.bypassSecurityTrustStyle(style);
  }

  /**
   * Composes the absolute image path for the hero image.
   * @returns {string}
   */
  getHeroPath() {
    const token = this.sessionService.getToken();
    let fullPath =
      DREAMFACTORY_INSTANCE_URL + '/api/v2/files/' +
      this.initiative.getHero().getPath() + '?ts_=' +
      this.cacheKiller + '&api_key=' + DREAMFACTORY_API_KEY;

    if (token) {
      fullPath += '&session_token=' + this.sessionService.getToken();
    }
    return 'url(' + fullPath + ')';
  }

}
