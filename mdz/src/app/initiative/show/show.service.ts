import { Injectable } from '@angular/core';
import {Initiative} from '../model/initiative.model';

@Injectable()
export class ShowService {

  public currentInitiative: Initiative;
  constructor() { }

}
