import {Injectable} from '@angular/core';
import {BaseHttpService} from '../../../common/services/BaseHttp.service';
import {Initiative} from '../../model/initiative.model';
import {SessionService} from '../../../user/session/services/session.service';
import {ToastsManager} from 'ng2-toastr';
import {Http} from '@angular/http';
import {Comment} from './comment/model/comment.model';
import {Deferred} from '../../../common/Deferred';
import {TranstoastService} from '../../../common/transtoast.service';

@Injectable()
export class CommentsService extends BaseHttpService {

  constructor(protected http: Http,
              protected toastr: TranstoastService,
              protected sessionService: SessionService) {
    super(http, toastr);
  };

  /**
   * GETs all the comments for the given Initiative and delivers them by promise.
   *
   * @param initiative
   * @returns {Promise<Comment[]>}
   */
  getCommentsForInitiative(initiative: Initiative): Promise<Comment[]> {

    const d = new Deferred();

    const options = {
      filter: {
        initiative_id: initiative.id,
        is_answer: false
      },
      order: {
        by: 'created',
        direction: 'DESC'
      }

    };

    this.getEntity(Comment.entity, options).subscribe(
      response => {
        d.resolve(Comment.fromResponse(response));
      }
    );

    return d.promise;
  }

  public getNewestCommentAfterCommentForInitiative(commentId: number, initiativeId: number): Promise<Comment[]> {

    const d = new Deferred();
    const options = {
      filter: {
        initiative_id: initiativeId,
        id: {
          type: '>',
          value: commentId
        }
      }
    };
    this.getEntity(Comment.entity, options).subscribe(
      response => {
        d.resolve(Comment.fromResponse(response));
      }
    );
    return d.promise;
  }

  /**
   * Will GET all comments, that are answers to the given Comment.
   *
   * @param comment
   * @returns {Promise<Comment[]>}
   */
  getCommentsForComment(comment: Comment): Promise<Comment[]> {
    const d = new Deferred();
    const options = {
      filter: {
        comment_id: comment.getId()
      },
      order: {
        by: 'created',
        direction: 'DESC'
      }
    };

    this.getEntity(Comment.entity, options).subscribe(
      (response) => {
        d.resolve(Comment.fromResponse(response));
      }
    );
    return d.promise;
  }

  /**
   * Creates a new Comment with the given text for the given Initiative.
   * It delivers this new Comment by Promise and, in parralel, POSTs the comment to the API.
   * @param initiative
   * @param text
   * @returns {Promise<Comment>}
   */
  postComment(initiative: Initiative, text: string): Promise<Comment> {
    const d = new Deferred();

    // only work when the user data is available
    const user = this.sessionService.getCurrentUser();
    if (!user) {
      return;
    }

    const comment: Comment = Comment.fromJson({
      text: text,
      initiative_id: initiative.id,
      username: user.first_name
      // user_id: user.id
    });

    this.makePost(comment, d);

    return d.promise;
  }

  /**
   * Will POST an answer to an existing comment.
   *
   * @param comment
   * @param text
   * @param initiative_id
   * @returns {Promise<Comment>}
   */
  answerComment(comment: Comment, text: string, initiative_id?: number): Promise<Comment> {
    const d = new Deferred();

    // only work when the user data is available
    const user = this.sessionService.getCurrentUser();
    if (!user) {
      return;
    }

    const commentJson = {
      text: text,
      comment_id: comment.getId(),
      username: user.first_name,
    };
    if (initiative_id) {
      commentJson['initiative_id'] = initiative_id;
    }

    const answer: Comment = Comment.fromJson(commentJson);

    this.makePost(answer, d);
    return d.promise;
  }


  /**
   * Takes a Comment and a deferred promise, which will be resolved, when the Comment has been successfully POSTed
   * to the API.
   *
   * @param comment
   * @param d
   */
  private makePost(comment: Comment, d: Deferred<any>) {
    // post the comment
    this.postEntity(comment).subscribe(
      // response has the ID only
      response => {
        const options = {
          id: response.json().resource[0].id
        };

        // load the full comment
        this.getEntity(Comment.entity, options).subscribe(
          // resolve the promise to deliver the comment
          response2 => {
            const postedComment = Comment.fromResponse(response2);
            d.resolve(postedComment);
          },
          errorResponse2 => {
            this.error(errorResponse2);
            d.reject();
          }
        );

      }, errorResponse => {
        this.error(errorResponse);
        d.reject();
      }
    );
  }

}
