export class Comment {

  public static entity = 'comment';

  public answers: Comment[] = [];

  public static fromJson(json: any) {

    const comment = new Comment(
      json.id,
      json.text,
      null,  // initiative_id
      null,  // comment_id,
      false, // is_answer
      json.username,
      json.user_id);

    if (json.comment_id) {
      comment.comment_id = json.comment_id;
      comment.is_answer = true;
    }

    if (json.initiative_id) {
      comment.initiative_id = json.initiative_id;
    }

    if (json.moderated) {
      comment.moderated = json.moderated;
    }

    if (json.created) {
      comment.created = json.created;
    }

    return comment;

  }

  public static fromResponse(json: any) {

    // return single comment
    if (json.json().resource === undefined) {
      const comment = Comment.fromJson(json.json());
      return comment;
    }

    // return array of comments
    const a: Comment[] = [];
    for (const comment of json.json().resource) {
      a.push(Comment.fromJson(comment));
    }
    return a;
  }

  constructor(private id: number,
              private text: string,
              private initiative_id: number,
              private comment_id?: number,
              private is_answer?: boolean,
              private username?: string,
              private user_id?: number,
              private moderated?: boolean,
              private created?: string) {
  }

  public toJson() {
    return {
      // entityName: Comment.entity(),
      id: this.id,
      text: this.text,
      username: this.username,
      user_id: this.user_id,
      initiative_id: this.initiative_id,
      is_answer: this.is_answer,
      comment_id: this.comment_id,
      moderated: this.moderated,
      created: this.created
    };
  }

  public getId() {
    return this.id;
  }

  public getUserId() {
    return this.user_id;
  }

  public getCreated() {
    return this.created;
  }

  public setUsername(username) {
    this.username = username;
  }

  public getParentCommentId() {
    return this.comment_id;
  }


  public setAnswers(comments: Comment[]) {
    this.answers = comments;
  }

  public addAnswer(comment: Comment) {
    // loop though the answers and test if the comment is already present.
    for(const answer of this.answers){
      if(answer.id === comment.id){
        // stop if the comment is already added.
        return;
      }
    }
    // add the comment.
    this.answers.splice(0, 0, comment);
  }
}
