import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Comment} from './model/comment.model';
import {CommentsService} from '../comments.service';
import {ReportedUser} from '../../../../user/ReportUser.model';
import {SessionService} from '../../../../user/session/services/session.service';
import {ReportUserService} from '../../../../user/report-user.service';
import swal from 'sweetalert2';
import {OtherUsersService} from '../../../../user/other-users.service';
import {ProfileImageService} from '../../../../common/profile-image/profile-image.service';
import {DomSanitizer} from '@angular/platform-browser';
import {DEFAULT_USER_IMAGE_URL} from '../../../../config/constants';
import {StateService} from "@uirouter/angular";

@Component({
  selector: 'initiative-comment',
  templateUrl: 'comment.component.html',
  styleUrls: ['comment.component.scss']
})
export class CommentComponent implements OnInit {

  private answer: string = null;
  private profileImage: string;

  private answered = false;

  @Input() comment: Comment;
  @Input() initiative_id: number;
  @Output('receivedanswerid') receivedanswerid: EventEmitter<number> = new EventEmitter();

  @Input() answerable: boolean;

  private userIsDeleted = false;

  constructor(private commentsService: CommentsService,
              private reportedUserService: ReportUserService,
              private sessionService: SessionService,
              private otherUsersService: OtherUsersService,
              private profileImageService: ProfileImageService,
              private domSanitizer: DomSanitizer,
              private stateService: StateService) {
  }

  ngOnInit() {
    this.getCommentsComments();
    this.getCommenterUsername();
  }

  getCommenterUsername() {
    const id = this.comment.getUserId();
    this.otherUsersService.getUserById(id).subscribe(
      response => {
        const user = response.json();
        if (user.is_active) {
          this.comment.setUsername(user.username);
          this.getCommenterProfileImage();
        } else {
          this.userIsDeleted = true;
          this.comment.setUsername('Gelöschter Benutzer');
        }
      }
    );
  }

  getCommenterProfileImage() {
    const id = this.comment.getUserId();
    this.otherUsersService.getProfile(id).subscribe(
      response => {
        const profile = response.json().resource[0];
        let imagePath = profile.image;

        if (profile.avatar_id) {
          imagePath = profile.avatar_id;
        }

        if (imagePath) {
          this.profileImage = this.profileImageService.composePath(imagePath);
        } else {
          this.profileImage = DEFAULT_USER_IMAGE_URL;
        }
      }
    );
  }

  getCommentsComments() {
    this.commentsService.getCommentsForComment(this.comment).then(
      (answers: Comment[]) => {
        if (answers && answers.length > 0) {

          this.comment.setAnswers(answers);
          // report newest comment ID to parent
          const newestId = answers[answers.length - 1].getId();
          this.receivedanswerid.emit(newestId);
        }
      }
    );
  }

  postAnswer() {
    if (!this.answered) {
      this.answered = true;
      this.commentsService.answerComment(this.comment, this.answer, this.initiative_id).then(
        (answer) => {
          this.comment.addAnswer(answer);
          this.answer = null;
          this.answered = false;
        }, error => {
          this.answered = false;
        }
      );
    }
  }


  /**
   * Bypasses CSS security.
   *
   * @param path
   * @returns {SafeStyle}
   */
  path(path: string) {
    return this.domSanitizer.bypassSecurityTrustStyle('url(' + path + ')');
  }

  keyUp($event) {
    if ((!$event.altKey && !$event.shiftKey) && ($event.keyCode === 13 || $event.code === 'Enter' || $event.key === 'Enter')) {
      this.postAnswer();
    }
  }

  performReportWithReason(reason: string) {
    if (!reason && reason.length < 10) {
      swal({
        title: 'Bitte gib eine Begründung an.',
        type: 'error'
      });
      return;
    }

    const myId = this.sessionService.getCurrentUser().id;
    console.log(this.stateService.params.initiativeId);
    const report: ReportedUser = ReportedUser.fromJson(
      {
        comment_id: this.comment.getId(),
        user_id: this.comment.getUserId(),
        message: reason,
        reported_by: myId,
        initiative_id: this.stateService.params.initiativeId
      }
    );

    this.reportedUserService.postEntity(report, ReportedUser.entity).subscribe(
      response => {
        swal({
          type: 'info',
          title: 'Vielen Dank, wir werden das überprüfen.'
        });
      },
      errorResponse => {
        console.log(errorResponse.json());
      }
    );

  }

  report() {
    swal({
      title: 'Warum möchtest du den Benutzer melden?',
      input: 'text',
      type: 'error',
      showCancelButton: true,
      cancelButtonText: 'Abbrechen',
      confirmButtonText: 'Melden'
    }).then(
      value => {
        this.performReportWithReason(value);
      },
      () => {/*noop*/
      }
    );
  }

}
