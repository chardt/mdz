import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import {Initiative} from '../../model/initiative.model';
import {Comment} from './comment/model/comment.model';
import {CommentsService} from './comments.service';
import {Deferred} from '../../../common/Deferred';
import {COMMENTS_POLL_TIME} from '../../../config/constants';

@Component({
  selector: 'initiative-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit, OnDestroy {

  /**
   * The current Initiative.
   */
  @Input() initiative: Initiative;

  /**
   * All the Comments of the Initiative.
   */
  private comments: Comment[];

  /**
   * This text is going to be the next Comment.
   */
  private commentText: string;

  private polling;

  private lastCommentId: number = null;

  constructor(private commentsService: CommentsService) {
  }

  /**
   * Will load all Comments for the current Initiative on init.
   */
  ngOnInit() {
    // this.poll();
    this.loadComments().then(
      () => {
        this.polling = setInterval(() => {
          if (this.comments) {
            this.poll();
          }
        }, COMMENTS_POLL_TIME);
      }
    );
  }

  /**
   * End polling for new comments when the component is destroyed.
   */
  ngOnDestroy() {
    clearInterval(this.polling);
  }

  /**
   * Loads the Top-Level comments of the current Initiative.
   *
   * @returns {Promise<T>}
   */
  private loadComments(): Promise<any> {
    const d = new Deferred();
    this.commentsService
      .getCommentsForInitiative(this.initiative)
      .then(
        (comments: Comment[]) => {
          this.comments = comments;

          // find the newest top level comment by finding the biggest ID so far.
          for (const c of this.comments) {
            this.increaseLastCommentId(c.getId());
          }
          d.resolve();
        });

    return d.promise;
  }

  private increaseLastCommentId(id: number) {
    if (id > this.lastCommentId) {
      this.lastCommentId = id;
    }
  }

  /**
   * May be called by Comment Child-Components when they have fetched answers to themselves.
   * Reports the newest ID to this component.
   *
   * @param id
   */
  private childHasReceivedAnswerId(id: number) {
    this.increaseLastCommentId(id);
  }

  /**
   * Either loads the Top-Level comments or the answers to those comments, depending of whether there are already
   * some comments loaded.
   */
  private poll() {
    // there are no comments yet, check if there are
    if (this.lastCommentId) {
      this.getNewestComments();
    } else {
      this.loadComments();
    }
  }

  /**
   * Will fetch every new comment, by asking the API for any comment with a greater ID than the las known biggest ID.
   */
  private getNewestComments() {
    // get only the new comments
    this.commentsService.getNewestCommentAfterCommentForInitiative(
      this.lastCommentId,
      this.initiative.id).then(
      newComments => {
        for (const newComment of newComments) {

          // add new top level comment
          if (!newComment.getParentCommentId()) {
            this.comments.splice(0, 0, newComment);
          } else {
            // find parent comment and add as new child
            for (const topLevelComment of this.comments) {
              // console.log('comparing', topLevelComment, newComment);
              if (topLevelComment.getId() === newComment.getParentCommentId()) {
                // console.log('adding comment', newComment);
                topLevelComment.addAnswer(newComment);
              }
            }
          }

          // store the newest comment id
          if (newComment.getId() > this.lastCommentId) {
            this.lastCommentId = newComment.getId();
          }
        }
        // console.log('Last comment ID (2)', this.lastCommentId);
      }
    );
  }

  /**
   * POSTs a new comment to the Initiative.
   */
  post() {
    this.commentsService
      .postComment(this.initiative, this.commentText)
      .then((comment: Comment) => {
        this.comments.splice(0, 0, comment);
        this.commentText = '';
      });
  }

  /**
   * EventListener for the Text input, which lets the user post a comment by pressing the enter key on the keyboard.
   * @param $event
   */
  keyUp($event) {
    if ((!$event.altKey && !$event.shiftKey) && ($event.keyCode === 13 || $event.code === 'Enter' || $event.key === 'Enter')) {
      this.post();
    }
  }
}
