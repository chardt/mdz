import {Component, OnInit} from '@angular/core';
import {StateService} from '@uirouter/angular';
import {Initiative} from '../model/initiative.model';
import {ShowService} from './show.service';
import {CommentsService} from './comments/comments.service';
import {InitiativeService} from '../services/initiative.service';
import {SupporterService} from '../services/supporter.service';
import {ReportUserService} from '../../user/report-user.service';
import {SessionService} from '../../user/session/services/session.service';
import {TranstoastService} from '../../common/transtoast.service';
import {NotificationSettingsService} from './sidebar/notification-settings/notification-settings.service';

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.scss'],
  providers: [CommentsService, InitiativeService, SupporterService, ReportUserService, NotificationSettingsService]
})
export class ShowComponent implements OnInit {

  /**
   * The initiative to SHOW
   */
  initiative: Initiative;

  constructor(private stateService: StateService,
              private initiativeShowService: ShowService,
              private initiativeService: InitiativeService,
              private toastr: TranstoastService,
              private sessionService: SessionService) {
  }

  /**
   * Gets the currently shown initiative from the initiative Service on init.
   * If there is none, it loads the entire Initiative with the ID out of the state params.
   */
  ngOnInit() {
    if (!this.initiative) {
      this.loadFullInitiative();
    }
  }

  /**
   * Takes the initiative ID from the state Params and GETs the initiative with all relevant related data.
   * Will only fetch the allowed subset if the user is not logged in.
   */
  loadFullInitiative() {

    // check if the user is logged in
    const loggedIn = (this.sessionService.getToken() !== null);

    const id = this.stateService.params.initiativeId;
    const options = {
      id: id,
      related: [
        Initiative.entityRelationOwners,
        Initiative.entityRelationSupporters,
        Initiative.entityRelationOrtsgemeindenEntity,
        Initiative.entityRelationHandlungsfelderEntity,
        Initiative.entityRelationImages,
      ]
    };

    if (!loggedIn) {
      options.related = [
        Initiative.entityRelationOrtsgemeindenEntity,
        Initiative.entityRelationHandlungsfelderEntity,
        Initiative.entityRelationImages
      ];
    }

    this.initiativeService.getEntity(Initiative.entity, options).subscribe(
      (response) => {
        const body = response.json();

        this.initiative = Initiative.fromJson(body);
      },
      (errorResponse) => {
        this.toastr.error(errorResponse.json().error.message);
      }
    );
  }
}
