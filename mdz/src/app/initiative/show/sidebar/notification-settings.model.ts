export class NotificationSettings {

  public static entity = 'notification_settings';

  public static fromJSON(json: any) {
    if(!json) return;
    return new NotificationSettings(
      json.initiative_id,
      json.user_id,
      json.id,
      json.new_comments,
      json.content_changes,
      json.answers_on_comments,
      json.supporter_joined
    );
  }

  constructor(public initiative_id?,
              public user_id?,
              public id?,
              public new_comments?,
              public content_changes?,
              public answers_on_comments?,
              public supporter_joined?) {

  }

  public toJson() {
    return {
      'initiative_id': this.initiative_id,
      'user_id': this.user_id,
      'id': this.id,
      'new_comments': this.new_comments,
      'content_changes': this.content_changes,
      'answers_on_comments': this.answers_on_comments,
      'supporter_joined': this.supporter_joined
    };
  }
}
