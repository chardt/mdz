import {Component, OnInit, Input} from '@angular/core';
import {Initiative} from '../../model/initiative.model';
import {InitiativeService} from '../../services/initiative.service';
import {SupporterService} from '../../services/supporter.service';
import {User} from '../../../user/model/user.model';
import {SessionService} from '../../../user/session/services/session.service';
import swal from 'sweetalert2';
import {StateService} from '@uirouter/angular';
import {NotificationSettingsService} from './notification-settings/notification-settings.service';
import {TranstoastService} from '../../../common/transtoast.service';

@Component({
  selector: 'initiative-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  /**
   * The current Initiative.
   */
  @Input() initiative: Initiative;

  /**
   * Those are the Users, whom support the current Initiative.
   * @type {Array}
   */
  private supporters: User[] = [];

  /**
   * List of emails to send invites to
   * @type {any}
   */
  private friendsToInvite: string = null;

  /**
   * Message for the invites
   * @type {any}
   */
  private messageToInviteFriendsWith: string = null;

  /**
   * Show/Hide the invite popup
   * @type {boolean}
   */
  private showInvite = false;

  /**
   * The Database ID of the record, that is the one that links the initiative and the current user.
   */
  private mySupportId: number;
  private itsMyInitiative = false;
  private showSupporter = true;


  constructor(private initiativeService: InitiativeService,
              private supporterService: SupporterService,
              private sessionService: SessionService,
              private stateService: StateService,
              private notificationSettingsService: NotificationSettingsService,
              private toastr: TranstoastService) {
  }

  /**
   * Will load the supporters and check the ownership on init.
   */
  ngOnInit() {
    this.getSupporters();
    this.getOwnerShip();
  }

  /**
   * Toggles the visibility of the settings tab.
   * @param show
   */
  showSettingsTab(show: boolean) {
    this.showSupporter = !show;
  }

  btnEndInitiativePressed(initiative) {
    const type = (initiative.is_event)?'die Aktion':'die Initiative';
    swal({
      title: 'Möchtest du '+type+' wirklich beenden?',
      showCancelButton: true,
      cancelButtonText: 'Nein, lieber nicht.',
      confirmButtonText: 'Ja, beenden.',
      type: 'question'
    }).then(
      () => {
        // Iquire if the inititiave was successfull
        swal({
          title: 'War '+type+' erfolgreich?',
          text: 'Wenn sie erfolgreich war, geht sie in die Geschichte des Marktplatzes des Ideen ein!',
          showCancelButton: true,
          cancelButtonText: 'Leider nicht :(',
          confirmButtonText: 'Ja!',
          type: 'question'
        }).then(
          () => {
            this.initiativeService.closeInitiative(initiative.id, true);
            swal({
              title: 'Wunderbar!',
              text: 'Vielen Dank für Dein Engagement.',
              type: 'success'
            }).then(() => {
              this.stateService.go('my-initiatives');
            });
          },
          () => {
            this.initiativeService.closeInitiative(initiative.id);
            swal({
              title: 'Vielen Dank für Dein Engagement!',
              type: 'success'
            }).then(() => {
              this.stateService.go('my-initiatives');
            });
          }
        );
      },
      () => {
        // noop.
      }
    );

  }

  /**
   * Will close the Initiative.
   * Asks the user before and redirects to the users Initiatives unpon success.
   * @param initiative
   */
  btnDeletePressed(initiative) {
    swal({
      title: 'Initiative löschen?',
      text: 'Bist du sicher, dass du die Initiative löschen möchtest?',
      showCancelButton: true,
      cancelButtonText: 'Nein, nicht löschen',
      confirmButtonText: 'Ja, löschen',
      type: 'warning'
    }).then(
      () => {
        this.initiativeService.closeInitiative(initiative.id, undefined, true);
        swal({
          title: 'Die Initiative wurde gelöscht.',
          type: 'success'
        }).then(() => {
          this.stateService.go('my-initiatives');
        });
      },
      () => {
        swal({
          title: 'Puh, die Initiative wurde nicht gelöscht.',
          type: 'success'
        });
      }
    );
  }

  /**
   * Lets the current User declare support for the Initiative.
   */
  btnSupportPressed() {
    this.supporterService.support(this.initiative.id).then(
      response => {
        this.mySupportId = response.json().resource[0].id;
        const me = this.sessionService.getCurrentUser();
        this.supporters.splice(0, 0, me);

        // prime the notification service and get the settings.
        // this will create the default settings.
        this.notificationSettingsService.prime(this.initiative.id);
        this.notificationSettingsService.createInitialSettings();
      },
      rejected => {
        this.toastr.info(rejected);
      }
    );
  }

  /**
   * Revokes the Users support for this Initiative.
   */
  btnStopSupportPressed() {
    this.supporterService.stopSupport(this.mySupportId).subscribe(
      () => {
        // delete the notification settings
        this.notificationSettingsService.deletePrimedSettings();

        // remove user from the UI
        let i = 0;
        const myId = this.sessionService.getCurrentUser().id;
        for (const sup of this.supporters) {
          if (sup.id === myId) {
            break;
          }
          i++;
        }
        this.supporters.splice(i, 1);
        this.mySupportId = null;

        this.showSupporter = true;
      }
    );
  }

  /**
   * Opens a Popup to invite friends via email.
   */
  private btnShowInviteFriendsPopUp() {
    this.showInvite = true;
  }

  /**
   * Checks if email and message data for invites are given.
   */
  private checkInviteData(): boolean {
    let aOk = true;

    if (!this.friendsToInvite) {
      this.toastr.error('Du hast keine E-Mail Adressen angegeben. Bitte gib die E-Mail Adressen deiner Freunde ein.');
      aOk = false;
    }

    if (!this.messageToInviteFriendsWith) {
      this.toastr.error('Bitte personalisiere deine Einladung mit einer Nachricht.');
      aOk = false;
    }

    return aOk;
  }

  /**
   * Checks for valid email/message data and eventually sends the emails.
   */
  private btnSendInvitesPressed() {

    if (!this.checkInviteData()) {
      return;
    }

    this.initiativeService.inviteFriends(
      this.friendsToInvite,
      this.initiative.id,
      this.initiative.title,
      this.messageToInviteFriendsWith)
      .subscribe(
        response => {
          // check if the response contains information about invalid emails and display an error message.
          try {
            response = response.json();
            const message = response.message;
            const invalidMails = response.invalidMails;
            if (invalidMails) {
              this.toastr.error(invalidMails.toString(), message);
              this.inviteSuccess();
            }
            const alreadyInvitedMails = response.alreadyInvitedMails;
            if (alreadyInvitedMails) {
              this.toastr.info(alreadyInvitedMails.toString(), 'An diese E-Mails Adressen wurden bereits Einladungen verschickt.');
            }
          } catch (err) {
            this.inviteSuccess();
          }
        },
        () => {
          this.inviteSuccess();
        }
      );
  }

  /**
   * Shows a SWAL hint and closes the invite popup.
   */
  private inviteSuccess() {
    swal({
      title: 'Klasse!',
      text: 'Deine Freunde wurden benachrichtigt.',
      type: 'success'
    }).then(
      () => {
        this.showInvite = false;
        this.friendsToInvite = null;
        this.messageToInviteFriendsWith = null;
      }
    );
  }

  private btnCancelInvitePressed() {
    this.friendsToInvite = null;
    this.messageToInviteFriendsWith = null;
    this.showInvite = false;
  }

  /**
   * Just changes the state to "edit".
   */
  btnEditPressed() {
    this.stateService.go('edit', {initiativeId: this.initiative.id});
  }

  /**
   * Gets the Initiatives supporters from the Initiative and looks up the current users support entry ID.
   */
  getSupporters() {
    this.supporters = this.initiative.getSupporters();
    if (!this.supporters) {
      return;
    }
    // get the ID of my own support entry, if it exists

    const myId = this.sessionService.getCurrentUser().id;
    for (const supporter of this.supporters) {
      if (supporter.id === myId) {
        this.mySupportId = supporter.support_id;
      }
    }
  }


  /**
   * Will remove the User with the given id from the local supporters array.
   * Is called by each supporter when it has been loaded.
   * Effectively filters the supporters.
   *
   * @param id
   */
  removeInactiveUserWithId(id: number) {
    let search = 0;
    for (const supporter of this.supporters) {
      if (supporter.id === id) {
        this.supporters.splice(search, 1);
        break;
      }
      search++;
    }
  }

  /**
   * GETs the owners from the API and checks if the current User is an owner, too.
   */
  getOwnerShip() {
    this.initiativeService.getOwnersOfInitiative(this.initiative).subscribe(
      response => {
        const myId = this.sessionService.getCurrentUser().id;
        const entries = response.json().resource;
        this.itsMyInitiative = false;
        for (const entry of entries) {
          if (entry.user_id === myId) {
            this.itsMyInitiative = true;
            break;
          }
        }

        this.markInitiators(entries);
      }
    );

  }

  /**
   * Puts a mark on those supporters, that are also owners.
   */
  private markInitiators(owners: any) {
    for (const guy of owners) {
      for (const supporter of this.supporters) {
        if (supporter.id === guy.user_id) {
          supporter.isInitiator = true;
        }
      }
    }
  }
}
