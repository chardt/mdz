import {Injectable} from '@angular/core';
import {BaseHttpService} from '../../../../common/services/BaseHttp.service';
import {NotificationSettings} from '../notification-settings.model';
import {Deferred} from '../../../../common/Deferred';
import {Http} from '@angular/http';
import {SessionService} from '../../../../user/session/services/session.service';
import {TranstoastService} from '../../../../common/transtoast.service';

@Injectable()
export class NotificationSettingsService extends BaseHttpService {

  /**
   * Notifications settings holder.
   * @type {any}
   */
  private notificationSettings: NotificationSettings = null;

  /**
   * Initiative ID holder.
   */
  private initiativeId: number = null;

  /**
   * User ID holder.
   */
  private userId: number = null;

  /**
   * Reference to the Session Service
   */
  private sessionService;

  constructor(protected http: Http,
              protected toastr: TranstoastService,
              sessionService: SessionService) {
    super(http, toastr);
    this.sessionService = sessionService;
  }

  /**
   * Primes the service by settings the initiativeID and userId.
   * @param initiativeId
   */
  public prime(initiativeId: number) {
    this.initiativeId = initiativeId;
    this.userId = this.sessionService.getCurrentUser().id;
  }

  /**
   * Takes the settings which are currently loaded and deletes them from the API.
   */
  public deletePrimedSettings() {
    this.deleteEntity(this.notificationSettings, NotificationSettings.entity).subscribe(
      () => {
      },
      error => {
        console.error(error.json());
      }
    );
  }

  /**
   * Returns the API options object for all the API calls.
   * @returns {{filter: {initiative_id: number, user_id: number}}}
   */
  private getOptions() {
    return {
      filter: {
        initiative_id: this.initiativeId,
        user_id: this.userId
      }
    };
  }

  /**
   * Creates a default NotificationSettings object with all notifications enabled.
   * @returns {NotificationSettings}
   */
  private getDefaultSettings() {
    return NotificationSettings.fromJSON({
      initiative_id: this.initiativeId,
      user_id: this.userId,
      new_comments: true,
      content_changes: true,
      answers_on_comments: true,
      supporter_joined: true
    });
  }


  /**
   * Creates the default settings and saves them.
   */
  public createInitialSettings(): Promise<NotificationSettings> {
    this.notificationSettings = this.getDefaultSettings();
    return this.save();
  }

  /**
   * GETs the notification settings from the API.
   * @returns {Promise<T>}
   */
  public getNotificationSettings(): Promise<NotificationSettings> {

    const q = new Deferred();
    // Settings object
    const options = this.getOptions();

    this.getEntity(NotificationSettings.entity, options).subscribe(
      response => {
        if (response.json().resource) {
          this.notificationSettings = NotificationSettings.fromJSON(response.json().resource[0]);
        }

        q.resolve(this.notificationSettings);
      }
    );

    return q.promise;
  }

  /**
   * Saves the settings and promises to return the updated settings.
   */
  public save(settings?: NotificationSettings): Promise<NotificationSettings> {

    if (!settings) {
      settings = this.notificationSettings;
    }

    const q = new Deferred();

    if (settings.id) {
      // PATCH
      this.patchEntity(settings, NotificationSettings.entity).subscribe(
        () => {
          this.notificationSettings = settings;
          q.resolve(this.notificationSettings);
        }
      );
    } else {
      // POST
      this.postEntity(settings, NotificationSettings.entity).subscribe(
        response => {
          const id = response.json().resource[0].id;
          settings.id = id;

          this.notificationSettings = settings;
          q.resolve(this.notificationSettings);
        }
      );
    }
    return q.promise;
  }
}
