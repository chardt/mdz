import { TestBed, inject } from '@angular/core/testing';

import { NotificationSettingsServiceService } from './notification-settings-service.service';

describe('NotificationSettingsServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NotificationSettingsServiceService]
    });
  });

  it('should be created', inject([NotificationSettingsServiceService], (service: NotificationSettingsServiceService) => {
    expect(service).toBeTruthy();
  }));
});
