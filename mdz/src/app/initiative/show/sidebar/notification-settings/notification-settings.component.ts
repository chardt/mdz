import {Component, OnInit, Input} from '@angular/core';
import {NotificationSettings} from '../notification-settings.model';
import {SessionService} from '../../../../user/session/services/session.service';
import {Initiative} from '../../../model/initiative.model';
import {NotificationSettingsService} from './notification-settings.service';

@Component({
  selector: 'initiative-notification-settings',
  templateUrl: './notification-settings.component.html',
  styleUrls: ['./notification-settings.component.scss']
})
export class NotificationSettingsComponent implements OnInit {

  private notificationSettings: NotificationSettings = null;
  @Input() initiative: Initiative;

  constructor(private notificationSettingsService: NotificationSettingsService) {
  }

  ngOnInit() {
    this.getNotificationSettings();
  }


  /**
   * Handles Changes to the settings by PUTing them up the API.
   */
  onNotificationSettingsChanged() {
    // Delay the event until the animation has finished.
    // Otherwise the data has not actually changed but it will appear to have happened in the browser logs.
    // Databinding affects the displayed objects on the console. This is why the console shows correct data,
    // but the request contains the old data.
    setTimeout(() => {
      this.notificationSettingsService.save(this.notificationSettings).then(
        (settings: NotificationSettings) => {
          this.notificationSettings = settings;
        }
      );
    }, 400);
  }

  /**
   * Gets the settings from the service.
   */
  getNotificationSettings() {

    // prime the service
    this.notificationSettingsService.prime(this.initiative.id);

    // get the notification settings from the service
    this.notificationSettingsService.getNotificationSettings().then(
      (settings: NotificationSettings) => {
        this.notificationSettings = settings;
      }
    );

  }
}
