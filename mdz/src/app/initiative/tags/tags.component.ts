import {Component, Input, OnInit} from '@angular/core';
import {CmsService} from '../../admin/cms/cms.service';
import {DomSanitizer, SafeHtml} from '@angular/platform-browser';

@Component({
  selector: 'initiative-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss']
})
export class TagsComponent implements OnInit {

  /**
   * A JSON Object that describes a TAG
   */
  @Input() tags: any[];

  /**
   * Flag that controls the display of the tag. (round or not)
   */
  @Input() round: boolean;

  private popupContent: SafeHtml = null;
  /**
   * Toggle for the popup visibility.
   */
  private popup: boolean;

  constructor(private cmsService: CmsService,
              private domSanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.cmsService.gethHandlungsFeldInfoPopupPage().then(
      page => {
        const content = page.getContent();
        this.popupContent = this.domSanitizer.bypassSecurityTrustHtml(content);
      }
    );
  }

  /**
   * Returns a string, based on the "round" flag.
   * @returns {String}
   */
  private getClass() {
    if (this.round) {
      return 'og';
    }
    return 'handlungsfeld';
  }

  /**
   * Gets the parent of the clicked element. If it is of class "handlungsfeld", show the popup.
   * @param event
   */
  private showPopup(event) {
    ;
    let parentClasses;
    if (event.srcElement) {
      parentClasses = event.srcElement.parentElement.className;
    } else {
      parentClasses = event.originalTarget.parentElement.className;
    }

    const check = parentClasses.indexOf('handlungsfeld');
    if (check > -1) {
      this.popup = true;
    }
  }


}
