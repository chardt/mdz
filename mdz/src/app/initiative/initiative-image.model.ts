export class InitiativeImage {

  /**
   * API Endpoint
   * @type {string}
   */
  public static entity = 'initiative_has_image';

  /**
   * Database ID of the model
   */
  private id: number;

  /**
   * Factory, that creates a new InitiativeImage from a json object.
   *
   * @param json
   * @returns {InitiativeImage}
   */
  public static fromJson(json: any) {
    const ii = new InitiativeImage(
      json.path,
      json.initiative_id,
      json.is_hero
    );

    if (json.id) {
      ii.id = json.id;
    }

    return ii;
  }

  constructor(private path: string,
              private initiativeId: number,
              private isHero: boolean) {

  }

  /**
   * Getter for the path
   * @returns {string}
   */
  public getPath() {
    return this.path;
  }

  /**
   * Getter for the entity ID.
   * @returns {number}
   */
  public getId() {
    return this.id;
  }

  /**
   * Getter for the hero flag. True, when the image is the initiative hero
   * @returns {boolean}
   */
  public getIsHero(): boolean {
    return this.isHero;
  }

  /**
   * Makes a json object or json formatted string from the InitiativeImages data.
   * @param stringify
   * @returns {any}
   */
  public toJson(stringify?: boolean) {
    const json = {
      path: this.path,
      ini_id: this.initiativeId,
      is_hero: this.isHero,
      id: this.id
    };

    if (!stringify) {
      return json;
    }
    return JSON.stringify(json);

  }

}
