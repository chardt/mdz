import {User} from '../../user/model/user.model';
import {Ortsgemeinde} from '../../admin/ortsgemeinden/ortsgemeinde.model';
import {Handlungsfeld} from '../../admin/handlungsfelder/handlungsfeld.model';
import {InitiativeImage} from '../initiative-image.model';
export class Initiative {

  /**
   * API Endpoint
   * @type {string}
   */
  public static entity = 'initiative';

  /**
   * Entity Relation for Ortsgemeinden connections
   * @type {string}
   */
  public static entityRelationOrtsgemeinde = 'initiative_belongs_to_ortsgemeinde';

  /**
   * Entity Relation for Handlungsfelder connections
   * @type {string}
   */
  public static entityRelationHandlungsfeld = 'initiative_belongs_to_handlungsfeld';


  /**
   * Entity Relation for actual Ortsgemeinden via the connection
   * @type {string}
   */
  public static entityRelationOrtsgemeindenEntity = 'ortsgemeinde_by_initiative_belongs_to_ortsgemeinde';

  /**
   * Entity Relation for actual Handlungsfelder via the connection
   * @type {string}
   */
  public static entityRelationHandlungsfelderEntity = 'handlungsfeld_by_initiative_belongs_to_handlungsfeld';

  /**
   * Entity Relation for Users that are allowed to edit the Initiative
   * @type {string}
   */
  public static entityRelationOwners = 'user_owns_initiative_by_initiative_id';

  /**
   * Entity Relation for Users that support the Initiative
   * @type {string}
   */
  public static entityRelationSupporters = 'user_supports_initiative_by_initiative_id';

  /**
   * Entity Relation for Comments of that Initiative
   * @type {string}
   */
  public static entityRelationComment = 'comment_by_initiative_id';

  /**
   * Entity Relation for connected Images
   * @type {string}
   */
  public static entityRelationImages = 'initiative_has_image_by_initiative_id';

  /**
   * The supporting Users
   */
  private supporter: User[];

  // TODO: check usage
  private supporterIDs: number[];

  /**
   * Array of the the Users that are Admins
   */
  private owners: User[];

  /**
   * Array of connected Ortsgemeinden
   */
  private ortsgemeinden: Ortsgemeinde[];

  /**
   * Array of connected Handlungsfelder
   */
  private handlungsfelder: Handlungsfeld[];

  /**
   * Array of InitiativeImage Models
   */
  private images: InitiativeImage[];

  /**
   * Single InitiativeImage Model that is used as the Hero
   */
  private hero: InitiativeImage;

  /**
   * Flag that reflects ownership. Is determined by logic. Admins/Owners will see UI
   * to change the Initiative, based on this flag.
   * @type {boolean}
   */
  public iAmAnOwner = false;

  /**
   * Factory, creates an Initiative from a JSON object.
   * Checks for related data and sets the appropriate attributes if data is present.
   *
   * @param json
   * @returns {any}
   */
  static fromJson(json: any) {
    if (!json) {
      return;
    }

    const i = new Initiative(
      json.id,
      json.title,
      json.latitude,
      json.longitude,
      json.description,
      new Date(json.date),
      json.is_demo,
      json.closed,
      json.published,
      json.blocked,
      json.is_event,
      json.successfull
    );

    // Check for Admins/Owners
    if (json.user_owns_initiative_by_initiative_id) {
      i.setOwners(json.user_owns_initiative_by_initiative_id);
    }

    // Check for connected Ortsgemeinden
    if (json.ortsgemeinde_by_initiative_belongs_to_ortsgemeinde) {
      i.setOrtsgemeinden(json.ortsgemeinde_by_initiative_belongs_to_ortsgemeinde);
    }

    // Check for connected Handlungsfelder
    if (json.handlungsfeld_by_initiative_belongs_to_handlungsfeld) {
      i.setHandlungsfelder(json.handlungsfeld_by_initiative_belongs_to_handlungsfeld);
    }

    // Check for connected Images
    if (json.initiative_has_image_by_initiative_id) {
      i.setImages(json.initiative_has_image_by_initiative_id);
    }

    // Check for Supporters
    if (json.user_supports_initiative_by_initiative_id) {

      const users = [];
      for (const support of json.user_supports_initiative_by_initiative_id) {

        const user = User.fromJson({id: support.user_id, support_id: support.id});
        users.push(user);
      }

      i.setSupporters(users);
    }

    return i;
  }

  constructor(public id: number,
              public title: string,
              public latitude: number,
              public longitude: number,
              public description: string,
              public date: Date,
              public isDemo: boolean,
              public closed: boolean,
              public published: boolean,
              public blocked: boolean,
              public is_event: boolean,
              public successfull: boolean) {
  }


  /**
   * Takes the basic Initiative data and creates a plain JSON object from that.
   * Related data is not included.
   *
   * @param stringify
   * @returns {string|{id: number, title: string, latitude: number, longitude: number,
   * description: string, is_demo: boolean, closed: boolean, published: boolean}}
   */
  public toJson(stringify?: boolean): any {
    const obj = {
      id: this.id,
      title: this.title,
      latitude: this.latitude,
      longitude: this.longitude,
      description: this.description,
      date: this.date.toISOString(),
      is_demo: this.isDemo,
      closed: this.closed,
      published: this.published,
      blocked: this.blocked,
      is_event: this.is_event,
      successfull: this.successfull
    };

    return stringify ? JSON.stringify({resource: [obj]}) : obj;
  }

  /**
   * Setter for the owners Array
   * @param users
   */
  public setOwners(users: User[]) {
    this.owners = users;
  }

  /**
   * Getter for the owners Array
   * @returns {User[]}
   */
  public getOwners(): User[] {
    return this.owners;
  }

  /**
   * Takes a User object and pushes it to the local owner array.
   * The model is NOT saved on the API. This is just to simulate the effect to spare a reload.
   * @param json
   */
  public addTemporaryOwner(json: any) {
    this.owners.push(json);
  }

  /**
   * Removes a user with the given ID from the local Admins/Owners array.
   * @param id
   */
  public removeTemporaryOwner(id: number) {
    for (let i = 0; i < this.owners.length; i++) {
      const owner = this.owners[i];
      if (owner.id === id) {
        this.owners.splice(i, 1);
      }
    }

  }

  /**
   * Returns the Array of Supporters, which are Users
   * @returns {User[]}
   */
  public getSupporters(): any {
    return this.supporter;
  }

  /**
   * Sets the users TODO: restrict typing
   * @param users
   */
  public setSupporters(users) {
    this.supporter = users;
  }

  /**
   * Takes an Array of InitiativeImages, fishes out the Hero and sets the rest as normal Images.
   * @param images
   */
  public setImages(images: InitiativeImage[]) {
    const imgs: InitiativeImage[] = [];
    for (const image of images) {
      const img = InitiativeImage.fromJson(image);
      if (!img.getIsHero()) {
        imgs.push(img);
      } else {
        this.hero = img;
      }
    }
    this.images = imgs;

  }

  /**
   * Returns the InitiativeImage Model of the Hero
   * @returns {InitiativeImage}
   */
  public getHero() {
    return this.hero;
  }

  /**
   * Returns all the InitiativeImage Models which are NOT the Hero
   * @returns {InitiativeImage[]}
   */
  public getImages() {
    return this.images;
  }

  /**
   * Sets the Ortsgemeinden
   * @param ortsgemeinden
   */
  public setOrtsgemeinden(ortsgemeinden: Ortsgemeinde[]) {
    this.ortsgemeinden = ortsgemeinden;
  }

  /**
   * Gets the Ortsgemeinden
   * @returns {Ortsgemeinde[]}
   */
  public getOrtsgemeinden() {
    return this.ortsgemeinden;
  }

  /**
   * Sets the Handlungsfelder
   * @param handlungsfelder
   */
  public setHandlungsfelder(handlungsfelder: Handlungsfeld[]) {
    this.handlungsfelder = handlungsfelder;
  }

  /**
   * Gets the Handlungsfelder
   * @returns {Handlungsfeld[]}
   */
  public getHandlungsfelder() {
    return this.handlungsfelder;
  }
}
