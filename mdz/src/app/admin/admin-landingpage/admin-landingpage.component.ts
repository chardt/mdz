import {Component, OnInit, ViewChild} from '@angular/core';
import {LandingpageService} from './landingpage.service';
import {LandingPage} from './landingpage.model';
import {DREAMFACTORY_API_KEY} from '../../config/constants';
import {ToastsManager} from 'ng2-toastr';
import {ImageUploadComponent} from '../../common/image-upload/image-upload.component';
import {TranstoastService} from '../../common/transtoast.service';

@Component({
  selector: 'app-admin-landingpage',
  templateUrl: './admin-landingpage.component.html',
  styleUrls: ['./admin-landingpage.component.scss'],
  providers: [LandingpageService]
})
export class AdminLandingpageComponent implements OnInit {

  /**
   * Landing page Model
   * */
  private landingPage: LandingPage;

  /**
   * URL for the quote image
   */
  private citerImageUrl: string;

  /**
   * Image Upload to change the quote image
   */
  @ViewChild(ImageUploadComponent) imageUploader: ImageUploadComponent;

  constructor(private landingpageService: LandingpageService,
              private toastr: TranstoastService) {
  }

  /**
   * Will load the landing page model and the quote image
   */
  ngOnInit() {
    this.getLandingpage();
    this.getCurrentCiterImage();
  }

  /**
   * Fetches the LandingPage Model from the API
   */
  getLandingpage() {
    this.landingpageService.getEntity(LandingPage.entity).subscribe(
      response => {
        const r = response.json();
        this.landingPage = LandingPage.fromJson(r);
      }
    );
  }

  /**
   * Fetches the quoter image from the API
   */
  getCurrentCiterImage() {
    this.landingpageService.getEntity('/files/landingpage/', null, true).subscribe(
      response => {
        const image = response.json().resource[0];
        this.citerImageUrl =
          this.landingpageService.endpoint('/files/' + image.path) + '?api_key=' + DREAMFACTORY_API_KEY +
        '&_ts='+Date.now().toString();
      }
    );
  }

  /**
   * Packs the current model into a json object and PATCHes is on the API.
   * Notifies the user visually in success and error case.
   *
   * After the PATCH it also triggers the image upload.
   */
  save() {
    const pack = this.landingPage.toJson();

    this.landingpageService.patchEntity(pack, LandingPage.entity).subscribe(
      response => {
        this.toastr.success('Die Änderungen wurden gespeichert!');
      },
      errorResponse => {
        this.toastr.error(errorResponse.json().error.message);
      }
    );

    this.imageUploader.uploadImage();
  }
}
