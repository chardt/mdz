export class LandingPage {

  public static entity = 'landingpage/1';
  private cite: string;
  private citer: string;
  public welcomeTitle: string;
  public welcomeText: string;
  public descriptionTitle: string;
  public descriptionText: string;

  /**
   * Factory method, which takes a json object and makes a LandingPage from it.
   * Acts like prepared statements and uses only the specific values. No blind conversion.
   * @param json
   */
  public static fromJson(json: any): LandingPage {
    const lp = new LandingPage();
    lp.cite = json.cite;
    lp.citer = json.citer;
    lp.welcomeText = json.welcome_text;
    lp.welcomeTitle = json.welcome_title;
    lp.descriptionText = json.description_text;
    lp.descriptionTitle = json.description_title;
    return lp;
  }

  /**
   * Makes a JSON object from the data
   * @param stringify When true, the method returns a JSON string.
   * @returns {any|string}
   */
  public toJson(stringify?: boolean) {
    const j = {
      cite: this.cite,
      citer: this.citer,
      welcome_text: this.welcomeText,
      welcome_title: this.welcomeTitle,
      description_text: this.descriptionText,
      description_title: this.descriptionTitle
    };

    if (!stringify) {
      return j;
    } else {
      return JSON.stringify(j);
    }

  }
}
