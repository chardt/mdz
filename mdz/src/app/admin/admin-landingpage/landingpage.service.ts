import {Injectable} from '@angular/core';
import {BaseHttpService} from '../../common/services/BaseHttp.service';
import {Http} from '@angular/http';
import {TranstoastService} from '../../common/transtoast.service';

@Injectable()
export class LandingpageService extends BaseHttpService {

  constructor(protected http: Http,
              protected toastr: TranstoastService) {
    super(http, toastr);
  }
}
