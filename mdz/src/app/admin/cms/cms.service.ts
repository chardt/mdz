import {Injectable} from '@angular/core';
import {BaseHttpService} from '../../common/services/BaseHttp.service';
import {Http} from '@angular/http';
import {TranstoastService} from '../../common/transtoast.service';
import {CmsPage} from './cms-page.model';
import {Deferred} from '../../common/Deferred';

@Injectable()
export class CmsService extends BaseHttpService {

  /**
   * Holds the CmsPage.
   * @type {any}
   */
  private handlungsFeldInfoPopupPage: CmsPage = null;

  /**
   * Flag to remember if the request was already sent.
   * @type {boolean}
   */
  private requested = false;

  /**
   * Promise to resolve, as soon as the CmsPage has been received.
   * @type {Deferred}
   */
  private willFetchData: Deferred<CmsPage> = new Deferred();

  constructor(protected http: Http,
              protected toastr: TranstoastService) {
    super(http, toastr);
  }

  /**
   * Lazily GETs the handlungsfeld info popup content from the API.
   * @returns {Promise<T>}
   */
  public gethHandlungsFeldInfoPopupPage(): Promise<CmsPage> {

    // start request only when the data is not already present
    // and the request was not sent out before.
    if (!this.handlungsFeldInfoPopupPage && !this.requested) {

      // remember that request was sent.
      this.requested = true;

      const options = {
        filter: {
          url: 'handlungsfeld-info'
        }
      };
      this.getEntity(CmsPage.entity, options).subscribe(
        response => {
          this.handlungsFeldInfoPopupPage = CmsPage.fromJson(response.json().resource[0]);
          this.willFetchData.resolve(this.handlungsFeldInfoPopupPage);
        }
      );
    }

    // return the services promise.
    return this.willFetchData.promise;
  }
}
