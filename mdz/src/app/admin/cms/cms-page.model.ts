/**
 * Model for the CMS pages.
 */
export class CmsPage {

  /**
   * API Endpoint.
   * @type {string}
   */
  public static entity = 'cmspage';

  /**
   * Database ID.
   */
  private id: number;

  /**
   * Internal Title for the page.
   */
  private title: string;

  /**
   * HTML Content of the page.
   */
  private content: string;

  private url: string;

  /**
   * Factory method to create a CmsPage from json.
   *
   * @param json
   * @returns {CmsPage|CmsPage[]}
   */
  public static fromJson(json: any) {

    const c = new CmsPage();
    if (json.id) {
      c.id = json.id;
    }
    if (json.title) {
      c.title = json.title;
    }
    if (json.content) {
      c.content = json.content;
    }

    if(json.url){
      c.url = json.url;
    }

    return c;
  }

  /**
   * Creates an array of CmsPages from json.
   * @param json
   * @returns {CmsPage[]}
   */
  public static arrayFromJson(json: any) {

    // create the empty array
    const c: CmsPage[] = [];

    // when a resource is returned, iterate and make cms pages from that
    if (json.resource) {
      for (const entry of json.resource) {
        const cp: any = CmsPage.fromJson(entry);
        c.push(cp);
      }
      return c;
    }

    // when no resource is present, it is a single cms page.
    // put it into an array an return the array.
    const c2 = new CmsPage();
    if (json.id) {
      c2.id = json.id;
    }
    if (json.title) {
      c2.title = json.title;
    }
    if (json.content) {
      c2.content = json.content;
    }
    if(json.url){
      c2.url = json.url;
    }

    c.push(c2);
    return c;
  }

  constructor(id?: number, title?: string, content?: string, url?:string) {
    this.id = id;
    this.title = title;
    this.content = content;
    this.url = url;
  }

  /**
   * Getter for ID.
   * @returns {number}
   */
  public getId() {
    return this.id;
  }

  /**
   * Getter for Title.
   * @returns {string}
   */
  public getTitle() {
    return this.title;
  }

  /**
   * Getter for the content.
   * @returns {string}
   */
  public getContent() {
    return this.content;
  }

  public getUrl(){
    return this.url;
  }
  /**
   * Returns a JSON representation of the CmsPage.
   */
  public toJson(stringify?: boolean) {
    const j = {
      id: this.id,
      title: this.title,
      content: this.content,
      url: this.url
    };
    if (!stringify) {
      return j;
    }
    return JSON.stringify(j);
  }
}
