import {Component, OnInit} from '@angular/core';
import {CmsService} from './cms.service';
import {CmsPage} from './cms-page.model';
import swal from 'sweetalert2';
import {TranstoastService} from '../../common/transtoast.service';

@Component({
  selector: 'app-cms',
  templateUrl: './cms.component.html',
  styleUrls: ['./cms.component.scss'],
  providers: [CmsService]
})
export class CmsComponent implements OnInit {

  /**
   * Array of all available CMS pages.
   */
  private pages: CmsPage[];

  /**
   * Holds the currently active page.
   */
  private activePage: CmsPage = null;

  /**
   * ID of the page to select as active page.
   * Is set via <select>.
   */
  private selectedId: number = -1;

  constructor(private cmsService: CmsService,
              private toastr: TranstoastService) {
  }

  /**
   * GETs the CmsPages from the API on init.
   */
  ngOnInit() {
    this.loadPagesFromServer();
  }

  /**
   * Does load all the CmsPages from the API.
   */
  private loadPagesFromServer() {
    this.cmsService.getEntity(CmsPage.entity).subscribe(
      (response) => {
        this.pages = CmsPage.arrayFromJson(response.json());
      }
    );
  }

  /**
   * Event to be triggered, when the user selects an entry from the pages <select>.
   * Will read the selected id and load the related CmsPage into the editor.
   */
  private onSelectedChanged() {

    if (this.selectedId === -1) {
      this.activePage = null;
      return;
    }

    for (const page of this.pages) {
      if (page.getId() === +this.selectedId) {
        this.activePage = page;
        break;
      }
    }
  }

  /**
   * Action to cancel the current edit process.
   */
  private btnCancelPressed() {
    swal({
      title: 'Wirklich abbrechen?',
      text: 'Alle Änderungen werden verloren gehen.',
      showCancelButton: true
    }).then(() => {
      this.reloadData();
    });
  }

  private reloadData() {
    this.selectedId = -1;
    this.activePage = null;
    this.loadPagesFromServer();
  }

  /**
   * Action to save the current page.
   */
  private btnSavePressed() {
    this.cmsService.patchEntity(this.activePage, CmsPage.entity).subscribe(
      () => {
        this.toastr.success('Der Eintrag wurde gespeichert.');
        this.reloadData();
      }
    );
  }
}
