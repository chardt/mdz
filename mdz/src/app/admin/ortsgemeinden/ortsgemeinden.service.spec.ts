import { TestBed, inject } from '@angular/core/testing';

import { OrtsgemeindenService } from './ortsgemeinden.service';

describe('OrtsgemeindenService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OrtsgemeindenService]
    });
  });

  it('should be created', inject([OrtsgemeindenService], (service: OrtsgemeindenService) => {
    expect(service).toBeTruthy();
  }));
});
