import {Component, OnInit} from '@angular/core';
import {OrtsgemeindenService} from './ortsgemeinden.service';
import {Ortsgemeinde} from './ortsgemeinde.model';

@Component({
  selector: 'app-ortsgemeinden',
  templateUrl: './ortsgemeinden.component.html',
  styleUrls: ['./ortsgemeinden.component.scss']
})
export class OrtsgemeindenComponent implements OnInit {

  /**
   * Name for the new Ortsgemeinde
   */
  private newName: string; // name of the next new initiative

  /**
   * Array of all the Ortsgemeinden
   * @type {Array}
   */
  private ortsgemeinden: any = [];

  constructor(private ortsgemeindenService: OrtsgemeindenService) {
  }

  /**
   * Will load all Ortsgemeinden on init
   */
  ngOnInit() {
    this.getOrtsgemeinden();
  }

  /**
   * GETs the Ortsgemeinden from the API and stores them in {@link ortsgemeinden}
   */
  getOrtsgemeinden() {
    this.ortsgemeindenService.getEntity(Ortsgemeinde.entity).subscribe(
      response => {
        this.ortsgemeinden = Ortsgemeinde.fromResponse(response);
      },
      errorResponse => {
        console.log(errorResponse);
      }
    );
  }

  /**
   * Creates a new Ortsgemeinde from the newName variable and POSTs it to the API.
   * on success, it pushes the new Ortsgemeinde to the local array.
   */
  add() {
    const og = new Ortsgemeinde(this.newName);
    this.ortsgemeindenService.postEntity(og).subscribe(
      response => {
        if (response.statusText === 'OK') {
          const id = response.json().resource[0].id;
          og.setId(id);
          // push the previously created ortsgemeinde to the array
          this.ortsgemeinden.push(og);
        }
      },
      errorResponse => {
        console.log(errorResponse);
      }
    );
    this.newName = null;
  }

  /**
   * Event, that is triggered by a child Ortsgemeinde after it has been DELETEd from the API.
   * It looks up the the Ortsgemeinde in the local array and splices it out.
   * @param og
   */
  deleted(og: Ortsgemeinde) {
    // find index
    let i = 0;
    let found = false;
    for (const h of this.ortsgemeinden) {
      if (h.getId() === og.getId()) {
        found = true;
        break;
      }
      i++;
    }
    // remove from array
    if (found) {
      this.ortsgemeinden.splice(i, 1);
    }
  }
}
