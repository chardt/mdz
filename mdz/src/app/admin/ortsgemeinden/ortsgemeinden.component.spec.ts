import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrtsgemeindenComponent } from './ortsgemeinden.component';

describe('OrtsgemeindenComponent', () => {
  let component: OrtsgemeindenComponent;
  let fixture: ComponentFixture<OrtsgemeindenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrtsgemeindenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrtsgemeindenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
