export class Ortsgemeinde {

  /**
   * API Endpoint
   * @type {string}
   */
  public static entity = 'ortsgemeinde';

  /**
   * Factory, which creates a new Ortsgemeinde object from json data
   * @param json
   * @returns {Ortsgemeinde}
   */
  public static fromJson(json: any) {
    // a single Handlungsfeld
    return new Ortsgemeinde(
      json.name,
      json.id
    );
  }

  /**
   * Factory wich eiterh creates a single Ortsgemeinde or an array of Ortsgemeinden, depending on the content
   * of the API response which is fed into the method.
   * @param response
   * @returns {any}
   */
  public static fromResponse(response: any) {
    // a single Handlungsfeld
    if (response.json().resource === undefined) {
      return Ortsgemeinde.fromJson(response);
    }

    // an array of handlungsfelder
    const oga: Ortsgemeinde[] = [];
    for (const hf of response.json().resource) {
      oga.push(Ortsgemeinde.fromJson(hf));
    }
    return oga;
  }

  constructor(private name: string, private id?: number) {
  }

  /**
   * returns a json object with the Ortsgemeindes data
   * @returns {{id: number, name: string}}
   */
  public toJson() {
    return {
      id: this.id,
      name: this.name
    };
  }

  /**
   * Setter for the ID
   * @param id
   */
  public setId(id: number) {
    this.id = id;
  }

  /**
   * Getter for the ID
   * @returns {number}
   */
  public getId() {
    return this.id;
  }

  /**
   * Getter for the name
   * @returns {string}
   */
  public getName() {
    return this.name;
  }

}
