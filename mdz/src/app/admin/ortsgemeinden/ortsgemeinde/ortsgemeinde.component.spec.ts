import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrtsgemeindeComponent } from './ortsgemeinde.component';

describe('OrtsgemeindeComponent', () => {
  let component: OrtsgemeindeComponent;
  let fixture: ComponentFixture<OrtsgemeindeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrtsgemeindeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrtsgemeindeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
