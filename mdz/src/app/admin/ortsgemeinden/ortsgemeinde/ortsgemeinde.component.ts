import {Component, EventEmitter, Output, Input} from '@angular/core';
import {Ortsgemeinde} from '../ortsgemeinde.model';
import {OrtsgemeindenService} from '../ortsgemeinden.service';
import {TranstoastService} from "../../../common/transtoast.service";

@Component({
  selector: 'app-ortsgemeinde',
  templateUrl: './ortsgemeinde.component.html',
  styleUrls: ['./ortsgemeinde.component.scss']
})
export class OrtsgemeindeComponent {

  private edit = false;

  @Input() ortsgemeinde: Ortsgemeinde;
  @Output() onDeleted: EventEmitter<Ortsgemeinde> = new EventEmitter();

  constructor(private ortsgemeindenService: OrtsgemeindenService,
              private toastr: TranstoastService) {
  }

  onKeyUp(event){
    if(event.key === "Enter" || event.code === "Enter" || event.keyCode === 13){
      this.btnSaveClicked();
    }
  }

  btnEditClicked() {
    this.edit = !this.edit;
  }

  btnSaveClicked() {
    this.ortsgemeindenService.save(this.ortsgemeinde).subscribe(
      successResponse => {
        if (successResponse.statusText === 'OK') {
          this.edit = false;
        }
      },
      errorResponse => {
        this.toastr.error(errorResponse.json().error.message);
      }
    );
  }

  btnTrashClicked() {
    this.ortsgemeindenService.deleteEntity(this.ortsgemeinde).subscribe(
      response => {
        if (response.statusText === 'OK') {
          this.onDeleted.emit(this.ortsgemeinde);
        }else{
          this.toastr.warning('Da ist etwas schief gelaufen.');
        }
      },
      errorResponse => {
        const message = errorResponse.json().error.message;
        if (message.indexOf('Integrity') > -1) {
          this.toastr.error('Dieses Handlungsfeld ist in Benutzung und kann nicht gelöscht werden.');
        }
      }
    );
  }

  trash() {
    this.ortsgemeindenService.deleteEntity(this.ortsgemeinde).subscribe(
      () => {
        this.onDeleted.emit(this.ortsgemeinde);
      },
      errorResponse => {
        const message = errorResponse.json().error.message;
        if (message.indexOf('Integrity') > -1) {
          this.toastr.error('Diese Ortsgemeinde ist in Benutzung und kann nicht gelöscht werden.');
        }
      }
    );
  }
}
