import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {BaseHttpService} from '../../common/services/BaseHttp.service';
import {Deferred} from '../../common/Deferred';
import {Ortsgemeinde} from './ortsgemeinde.model';
import {TranstoastService} from '../../common/transtoast.service';

@Injectable()
export class OrtsgemeindenService extends BaseHttpService {

  constructor(protected http: Http,
              protected toastr: TranstoastService) {
    super(http, toastr);
  }

  /**
   * Promises to deliver all the Ortsgemeinden.
   * It GETs the Ortsgemeinden from the API.
   * If an initiative ID is given as parameter, filter options will be added to the request, so only the
   * Ortsgemeinden of that Initiative will be fetched.
   *
   * @param initiativeId
   * @returns {Promise<T>}
   */
  list(initiativeId?: number): Promise<any> {
    const d = new Deferred();
    let options = {};
    if (initiativeId) {
      options = {
        filter: {
          initiative_id: initiativeId
        }
      };
    }
    this.getEntity(Ortsgemeinde.entity, options).subscribe(
      response => {
        d.resolve(Ortsgemeinde.fromResponse(response));
      }
    );
    return d.promise;
  }

  public save(ortsgemeinde: Ortsgemeinde){
    return this.patchEntity(ortsgemeinde, Ortsgemeinde.entity);
  }
}
