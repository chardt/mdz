import {Component, OnInit} from '@angular/core';
import {InitiativeService} from '../../initiative/services/initiative.service';
import {Initiative} from '../../initiative/model/initiative.model';
import {ToastsManager} from 'ng2-toastr';
import {TranstoastService} from '../../common/transtoast.service';
import {init} from "protractor/built/launcher";

@Component({
  selector: 'initiaive-list',
  templateUrl: './initiaive-list.component.html',
  styleUrls: ['./initiaive-list.component.scss'],
  providers: [InitiativeService]
})
export class InitiaiveListComponent implements OnInit {

  /**
   * Holds all currently loaded initiatives
   */
  initiatives: Initiative[];

  private demoCounter = 0;

  /**
   * Offset for pagination.
   *
   * @type {number}
   */
  private offset = 0;

  /**
   * Limit for pagination.
   *
   * @type {number}
   */
  private limit = 10;

  /**
   * Number of available pages.
   *
   * @type {number}
   */
  private pages = 0;

  /**
   * Number of the current page.
   *
   * @type {number}
   */
  private page = 0;

  /**
   * Phrase to search in the initiatives.
   *
   * @type {any}
   */
  private searchString: string = null;

  private lastSearchString: string = null;

  constructor(private initiativeService: InitiativeService,
              private toastr: TranstoastService) {
  }

  /**
   * Will load all initiatives on init
   */
  ngOnInit() {
    this.loadInitiatives();
  }

  /**
   * Will load all initiatives from the API
   */
  loadInitiatives() {

    // see if the search string was changed. If it was, reset pagination.
    if (this.searchString !== this.lastSearchString) {
      this.offset = 0;
      this.lastSearchString = this.searchString;
    }

    const paginationOptions = {
      offset: this.offset,
      limit: this.limit
    };
    // this.initiativeService.list(paginationOptions);
    this.initiativeService.search(this.searchString, paginationOptions).subscribe(
      response => {
        const is: Initiative[] = [];
        const resource = response.json();

        // set pagination info
        this.pages = Math.ceil(resource.meta.count / paginationOptions.limit);
        this.page = this.offset / this.limit + 1;

        for (const r of resource.resource) {
          const i = Initiative.fromJson(r);
          if (i.isDemo) {
            this.demoCounter++;
          }
          is.push(i);
        }
        this.initiatives = is;
      }
    );
  }

  private onKeyUp(event) {
    // load on enter
    if (event.keyCode === 13) {
      this.loadInitiatives();
    } else {
      // also load when NOT enter, but search string is empty
      if (this.searchString.length === 0) {
        this.loadInitiatives();
      }
    }
  }

  /**
   * Pagination NEXT.
   */
  private next() {
    if (this.page < this.pages) {
      this.offset += this.limit;
      this.loadInitiatives();
    }
  }

  /**
   * Pagination PREV.
   */
  private prev() {
    if (this.page > 1) {
      this.offset -= this.limit;
      this.loadInitiatives();
    }
  }

  /**
   * Marks the given Initiative as "Demo" so it appears on the landing pages feature section.
   * If there are already 3 Initiatives marked, it will say so and stop.
   * @param initiative
   */
  markAsDemo(initiative: Initiative) {

    // count the number of the demo initiatives, break if already 3
    if (initiative.isDemo && this.demoCounter >= 3) {
      this.toastr.warning('Es sind bereits drei Initiativen markiert. Bitte entferne zunächst eine um diese Initiative hinzuzufügen.');
      initiative.isDemo = false;
      return;
    }

    const pack = initiative.toJson();
    this.initiativeService.patchEntity(pack, Initiative.entity, false, 'id').subscribe(
      () => {

        // increaase/decrease the counter
        if (initiative.isDemo) {
          this.demoCounter++;
        } else {
          this.demoCounter--;
        }

      },
      errorResponse => {
        console.log(errorResponse.json());
      }
    );
  }

  block(initiative: Initiative) {
    const pack = initiative.toJson();
    this.initiativeService.patchEntity(pack, Initiative.entity, false, 'id').subscribe(
      response => {
        this.toastr.success('Initiative ' + initiative.title + ' wurde gespeichert');
      },
      error => {
        this.toastr.error('Es liegt ein Fehler vor: ' + error.json().message);
      }
    )
  }

}
