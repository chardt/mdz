import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InitiaiveListComponent } from './initiaive-list.component';

describe('InitiaiveListComponent', () => {
  let component: InitiaiveListComponent;
  let fixture: ComponentFixture<InitiaiveListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InitiaiveListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InitiaiveListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
