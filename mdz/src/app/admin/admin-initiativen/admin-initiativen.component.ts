import {Component} from '@angular/core';
import {HandlungsfelderService} from '../handlungsfelder/handlungsfelder.service';
import {OrtsgemeindenService} from '../ortsgemeinden/ortsgemeinden.service';

@Component({
  selector: 'app-admin-initiativen',
  templateUrl: './admin-initiativen.component.html',
  styleUrls: ['./admin-initiativen.component.scss'],
  providers: [HandlungsfelderService, OrtsgemeindenService]
})
export class AdminInitiativenComponent {

  constructor() {
  }
}
