import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminInitiativenComponent } from './admin-initiativen.component';

describe('AdminInitiativenComponent', () => {
  let component: AdminInitiativenComponent;
  let fixture: ComponentFixture<AdminInitiativenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminInitiativenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminInitiativenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
