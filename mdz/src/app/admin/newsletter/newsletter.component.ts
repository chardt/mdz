import { Component, OnInit } from '@angular/core';
import {NewsletterService} from "./newsletter.service";

@Component({
  selector: 'mdz-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.scss'],
  providers: [NewsletterService]
})
export class NewsletterComponent implements OnInit {

  constructor(private newsletterService: NewsletterService) { }

  ngOnInit() {
    this.newsletterService.getRecipients();
  }

}
