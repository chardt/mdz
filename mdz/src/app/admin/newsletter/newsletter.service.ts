import {Injectable} from '@angular/core';
import {BaseHttpService} from "../../common/services/BaseHttp.service";
import {Http} from "@angular/http";
import {TranstoastService} from "../../common/transtoast.service";
import {UserProfile} from "../../user/userprofile.model";
import {OtherUsersService} from "../../user/other-users.service";
import {User} from "../../user/model/user.model";

@Injectable()
export class NewsletterService extends BaseHttpService {

  private idsOfRecipients: number[] = [];
  private recipients: User[] = [];
  private csv: string = 'Vorname, Nachname, E-Mail Adresse\n';

  constructor(protected http: Http,
              protected toastr: TranstoastService,
              private otherUsersService: OtherUsersService) {
    super(http, toastr);
  }

  public getRecipients() {
    const options = {
      fields: [
        'user_id'
      ],
      filter: {
        receives_newsletter: true
      }
    };
    const entity = UserProfile.entity;
    this.getEntity(entity, options).subscribe(
      response => {
        response.json().resource.map(entry => {
          this.idsOfRecipients.push(entry.user_id);
        });
        this.fillList();
      }
    )
  }

  private fillList() {
    this.idsOfRecipients.map(id => {
      this.otherUsersService.getUserById(id).subscribe(
        response => {
          const user = User.fromJson(response.json());
          this.recipients.push(user);

          // add user data to csv file
          const line = user.first_name+','+user.last_name+','+user.email+'\n';
          this.csv += line;
        }
      )
    });
  }

  downloadCsv(){
    let blob = new Blob(['\ufeff' + this.csv], { type: 'text/csv;charset=utf-8;' });
    let dwldLink = document.createElement("a");
    let url = URL.createObjectURL(blob);
    let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
    if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
      dwldLink.setAttribute("target", "_blank");
    }
    dwldLink.setAttribute("href", url);
    dwldLink.setAttribute("download", "Newsletter-Adressen.csv");
    dwldLink.style.visibility = "hidden";
    document.body.appendChild(dwldLink);
    dwldLink.click();
    document.body.removeChild(dwldLink);
  }
}
