import {Injectable} from '@angular/core';
import {BaseHttpService} from '../../common/services/BaseHttp.service';
import {Http} from '@angular/http';
import {TranstoastService} from '../../common/transtoast.service';

@Injectable()
export class AdminUserService extends BaseHttpService {

  constructor(toastr: TranstoastService,
              http: Http) {
    super(http, toastr);
  }

}
