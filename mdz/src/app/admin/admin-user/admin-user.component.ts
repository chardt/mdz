import {Component, OnInit} from '@angular/core';
import {AdminUserService} from './admin-user.service';

@Component({
  selector: 'app-admin-user',
  templateUrl: './admin-user.component.html',
  styleUrls: ['./admin-user.component.scss'],
  providers: [AdminUserService]
})
export class AdminUserComponent implements OnInit {

  /**
   * Array of Users
   */
  users: any[];

  /**
   * Total users registered
   */
  userCount: number;

  /**
   * Current page of the pagination
   * @type {number}
   */
  page = 1;

  /**
   * Limit for the users GET / the page size.
   * @type {number}
   */
  limit = 10;

  /**
   * Offset for the users GET / the current page
   * @type {number}
   */
  offset = 0;

  /**
   * String to be included as search parameter in the users GET
   */
  searchPhrase: string;

  /**
   * Array, which contains a number for each page
   */
  pages: number[];

  /**
   * Reflects "next" of the pagination. If false, there is no next page.
   * If true, there is.
   * @type {boolean}
   */
  nextAvaliable = false;

  /**
   * Reflects "prevoius" of the pagination. If false, there is no previous page.
   * If true, there is.
   * @type {boolean}
   */
  prevAvaliable = false;

  constructor(private userService: AdminUserService) {

  }

  /**
   * Load all users on initiation.
   * NOTE: All users will load only the first page.
   * TODO: Refactor
   */
  ngOnInit() {
    this.getAllUsers();
  }

  /**
   * Calculates the new offset for the pagination.
   * It then loads the next page.
   */
  next() {
    if (this.offset < (this.userCount - this.limit)) {
      this.offset += this.limit;
    } else {
      this.offset = this.userCount - this.limit;
    }
    this.getAllUsers();
  }

  /**
   * Calculates the new offset for the pagination.
   * It then loads the next page.
   */
  previous() {
    if (this.offset > 0) {
      this.offset -= this.limit;
    } else {
      this.offset = 0;
    }
    this.getAllUsers();
  }

  /**
   * Calculates the offset by the target page number and loads the page.
   * @param page
   */
  gotoPage(page: number) {
    this.offset = page * this.limit;
    this.getAllUsers();
  }

  /**
   * Calculates, if a next/previous page is available and sets {@link nextAvaliable} and {@link prevAvaliable}
   */
  setButtonStates() {
    this.nextAvaliable = (this.limit + this.offset < this.userCount);
    this.prevAvaliable = (this.offset > 0);
  }

  /**
   * Calculates the page count and fills the {@link pages} array.
   */
  setPagesCount() {
    const pageCount = this.userCount / this.limit;
    this.pages = [];
    for (let i = 0; i < pageCount; i++) {
      this.pages.push(i);
    }
  }

  /**
   * Action that will load the users, when the {@link searchPhrase} input was cleared
   * @param $event
   */
  listWhenEmpty($event) {
    if ($event.srcElement.value.length === 0) {
      this.getAllUsers();
    }
  }

  /**
   * Loads the users. Will respect {@link limit}, {@link offset} and {@link searchPhrase}.
   * Issues a GET on /system/user and consumes the response. Will set the {@link setButtonStates} and {@link setPagesCount}
   */
  getAllUsers() {

    const options = {
      limit: this.limit,
      includeCount: true,
      offset: this.offset
    };

    if (this.searchPhrase) {
      options['filter'] = 'username like %' + this.searchPhrase + '%';
    }

    this.userService.getEntity('/system/user', options, true).subscribe(
      response => {
        this.users = response.json().resource;
        this.userCount = response.json().meta.count;

        this.setButtonStates();
        this.setPagesCount();
      }
    );
  }

}
