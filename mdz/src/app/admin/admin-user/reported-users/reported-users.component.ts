import {Component, OnInit} from '@angular/core';
import {ReportedUser} from '../../../user/ReportUser.model';
import {AdminUserService} from '../admin-user.service';

@Component({
  selector: 'admin-reported-users',
  templateUrl: './reported-users.component.html',
  styleUrls: ['./reported-users.component.scss']
})
export class ReportedUsersComponent implements OnInit {


  /**
   * List of all reports
   */
  reportedUsers: ReportedUser[];

  /**
   * Count of all reports
   */
  reportedUserCount: number;

  userCount: number;
  page = 1;
  limit = 2;
  offset = 0;

  /* Values which prepare the list for pagination */
  searchPhrase: string;
  pages: number[];
  nextAvaliable = false;
  prevAvaliable = false;


  constructor(private userService: AdminUserService) {
  }

  /**
   * Will load the reported users on init
   */
  ngOnInit() {
    this.getReportedUsers();
  }


  /**
   * EventListener, which will be triggered after the report, which is being observed, has been resolved.
   * It then removes the report from the local list, so it will no longer be displayed.
   * @param $event is a number, which holds the report ID.
   */
  resolve($event) {
    const reportId = $event;
    for (let i = 0; i < this.reportedUsers.length; i++) {
      if (this.reportedUsers[i].getId() === reportId) {
        this.reportedUsers.splice(i, 1);
        break;
      }
    }
  }

  /**
   * Will load all open reports. Currently starting at offset 0 and without limit. TODO: implement Pagination
   */
  getReportedUsers() {

    const options = {
      // limit: this.limit,
      includeCount: true,
      offset: this.offset,
      filter: {
        open: true
      }
    };

    this.userService.getEntity(ReportedUser.entity, options).subscribe(
      response => {
        this.reportedUsers = ReportedUser.fromResource(response.json().resource);
        this.reportedUserCount = response.json().meta.count;

      }
    );
  }
}
