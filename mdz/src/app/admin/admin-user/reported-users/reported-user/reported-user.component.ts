import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {ReportedUser} from '../../../../user/ReportUser.model';
import {AdminUserService} from '../../admin-user.service';
import {User} from '../../../../user/model/user.model';
import {Comment} from '../../../../initiative/show/comments/comment/model/comment.model';
import swal from 'sweetalert2';
import {SessionService} from '../../../../user/session/services/session.service';
import {DREAMFACTORY_INSTANCE_URL} from "../../../../config/constants";

@Component({
  // tslint:disable-next-line
  selector: '[reported-user]',
  templateUrl: './reported-user.component.html',
  styleUrls: ['./reported-user.component.scss']
})
export class ReportedUserComponent implements OnInit {

  /**
   * The Report that is handled in the component instance
   */
  @Input() report: ReportedUser;

  /**
   * Event to be triggered, when the issue has been resolved
   * @type {EventEmitter}
   */
  @Output() resolved: EventEmitter<any> = new EventEmitter();

  /**
   * The user whom was reported
   */
  user: User;

  /**
   * The user who issued the report
   */
  reporter: User;

  /**
   * The comment that was reported
   */
  comment: Comment;

  constructor(private userService: AdminUserService,
              private sessionService: SessionService) {
  }


  /**
   * Will load data of the reported user, the reporting user and the reported comment
   */
  ngOnInit() {
    this.getUserData();
    this.getReporterData();
    this.getComment();
  }

  /**
   * GETs the /system/user from the reporting users ID
   */
  getReporterData() {
    const options = {
      filter: {
        id: this.report.getReporterId()
      }
    };

    this.userService.getEntity('/system/user/', options, true).subscribe(
      response => {
        this.reporter = response.json().resource[0];
      }
    );
  }

  /**
   * GETs the /system/user from the reported users ID
   */
  getUserData() {

    const options = {
      filter: {
        id: this.report.getUserId()
      }
    };

    this.userService.getEntity('/system/user/', options, true).subscribe(
      response => {
        this.user = response.json().resource[0];
      }
    );
  }

  /**
   * GETs the reported comment by it's ID
   */
  getComment() {
    const options = {
      id: this.report.getCommentId()
    };

    this.userService.getEntity(Comment.entity, options).subscribe(
      response => {
        this.comment = Comment.fromJson(response.json());
      }
    );
  }

  /**
   * Action to perform, when the user clicked the "clear comment" button.
   * The user is presented with a SweetAlert to decide, whether he want's to overwrite the comment, to clear it or to leave it be.
   */
  btnClearCommentPressed() {
    swal({
      title: 'Wenn der Kommentar überschrieben werden soll, bitte einen Text eingeben.',
      text: 'Alternativ einfach leer lassen.',
      input: 'text',
      type: 'info',
      showCancelButton: true,
      cancelButtonText: 'Abbrechen',
      confirmButtonText: 'Lösen'
    }).then(
      value => {
        this.clearComment(value);
      }
    );
  }

  /**
   * Actual action which will modify a reported comment.
   * If there is no new comment text, it will be replaced by a generic note. Otherwise it will be replaced by the given text.
   * @param newText
   */
  clearComment(newText: string) {
    if (!(newText && newText.length > 0)) {
      newText = 'Dieser Kommentar wurde entfernt.';
    }

    const patch = {
      id: this.comment.getId(),
      text: newText,
      moderated: true
    };

    this.userService.patchEntity(patch, Comment.entity + '/' + this.comment.getId()).subscribe(
      response => {
        console.log(response.json());
      }
    );
  }
  /**
   * Asks the user (SweetAlert) how the issue has been resolved.
   * Triggers {@link resolve()) with the entered value.
   */
  btnResolvePressed() {
    swal({
      title: 'Wie wurde das Problem gelöst?',
      input: 'text',
      type: 'info',
      showCancelButton: true,
      cancelButtonText: 'Abbrechen',
      confirmButtonText: 'Lösen'
    }).then(
      value => {
        this.resolve(value);
      }
    );
  }

  /**
   * Forms a resolution object (json only) and PATCHes the report, by adding the admins user id and the resolution text.
   * Will fire the {@link resolved} emitter with the report ID.
   * @param resolution
   */
  resolve(resolution: string) {

    const myId = this.sessionService.getCurrentUser().id;

    const patch = {
      id: this.report.getId(),
      resolution: resolution,
      closed_by: myId,
      open: false
    };

    this.userService.patchEntity(patch, ReportedUser.entity).subscribe(
      response => {
        this.resolved.emit(this.report.getId());
      }
    );
  }
}
