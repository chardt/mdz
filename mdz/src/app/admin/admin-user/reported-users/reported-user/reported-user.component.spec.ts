import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportedUserComponent } from './reported-user.component';

describe('ReportedUserComponent', () => {
  let component: ReportedUserComponent;
  let fixture: ComponentFixture<ReportedUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportedUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportedUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
