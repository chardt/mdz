import {Component, OnInit, Input} from '@angular/core';
import {User} from '../../../user/model/user.model';
import {OtherUsersService} from '../../../user/other-users.service';

@Component({
  selector: 'user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss']
})
export class UserCardComponent implements OnInit {

  /**
   * The {@link User} to display
   */
  @Input() user: User;

  constructor(private otherUserService: OtherUsersService) {
  }

  ngOnInit() {
  }

  /**
   * Blocks and also unblocks a user, depending on the active parameter.
   * Should be used only to block and without the second parameter, as there is a convenience method for unblocking.
   * @param user_id
   * @param active
   */
  blockUser(user_id: number, active?: boolean) {

    this.otherUserService.blockUser(user_id, active).subscribe(
      response => {
        this.user.is_active = active;
      }
    );
  }

  /**
   * Convenience method to unblock a user. It just hands the user over ti blockUser() and sets the "active" parameter to true.
   * @param user_id
   */
  unblockUser(user_id: number) {
    this.blockUser(user_id, true);
  }
}
