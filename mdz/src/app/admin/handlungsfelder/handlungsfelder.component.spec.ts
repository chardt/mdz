import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandlungsfelderComponent } from './handlungsfelder.component';

describe('HandlungsfelderComponent', () => {
  let component: HandlungsfelderComponent;
  let fixture: ComponentFixture<HandlungsfelderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandlungsfelderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandlungsfelderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
