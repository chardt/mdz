import {Component, OnInit} from '@angular/core';
import {HandlungsfelderService} from './handlungsfelder.service';
import {Handlungsfeld} from './handlungsfeld.model';

@Component({
  selector: 'app-handlungsfelder',
  templateUrl: './handlungsfelder.component.html',
  styleUrls: ['./handlungsfelder.component.scss']
})
export class HandlungsfelderComponent implements OnInit {

  /**
   * Name for the new Handlungsfeld
   */
  private newName: string; // name of the next new initiative

  /**
   * Array of all the Handlungsfelder
   * @type {Array}
   */
  private handlungsfelder: any = [];

  constructor(private handlungsfelderService: HandlungsfelderService) {
  }

  /**
   * Will load all the current Handlungsfelder on init
   */
  ngOnInit() {
    this.getHandlungsfelder();
  }

  /**
   * GETs all Handlungsfelder and stores them in {@link handlungsfelder}
   */
  getHandlungsfelder() {
    this.handlungsfelderService.getEntity(Handlungsfeld.entity).subscribe(
      response => {
        this.handlungsfelder = Handlungsfeld.fromResponse(response);
      },
      errorResponse => {
        console.log(errorResponse);
      }
    );
  }

  /**
   * Creates a new Handungsfeld from the {@link newName} variable and POSTs it to the API.
   * On success, it adds the Handlungsfeld to the local array and clears the newName variable.
   */
  add() {
    const hf = new Handlungsfeld(this.newName);
    this.handlungsfelderService.postEntity(hf).subscribe(
      response => {
        if (response.statusText === 'OK') {
          const id = response.json().resource[0].id;
          hf.setId(id);
          // push the previously create handlungsfeld to the array
          this.handlungsfelder.push(hf);
        }
      },
      errorResponse => {
        console.log(errorResponse);
      }
    );
    this.newName = null;
  }


  /**
   * Event that is triggered when a child Handlungsfeld was deleted.
   * It looks up the Handlungsfeld in the local array and splices it out, if found.
   * @param hf
   */
  deleted(hf: Handlungsfeld) {
    // find index
    let i = 0;
    let found = false;
    for (const h of this.handlungsfelder) {
      if (h.getId() === hf.getId()) {
        found = true;
        break;
      }
      i++;
    }
    if (found) {
      this.handlungsfelder.splice(i, 1);
    }
  }
}
