import {Injectable} from '@angular/core';
import {BaseHttpService} from '../../common/services/BaseHttp.service';
import {ToastsManager} from 'ng2-toastr';
import {Http} from '@angular/http';
import {Deferred} from '../../common/Deferred';
import {Handlungsfeld} from './handlungsfeld.model';
import {TranstoastService} from '../../common/transtoast.service';
import {Ortsgemeinde} from "../ortsgemeinden/ortsgemeinde.model";

@Injectable()
export class HandlungsfelderService extends BaseHttpService {

  constructor(protected http: Http,
              protected toastr: TranstoastService) {
    super(http, toastr);
  }

  /**
   * GETs the Handlungsfelder from the API.
   * If an initiaitve ID is given, the request will have a set of options, which will load only the Handlungsfelder
   * of the initiative with the given initiative ID.
   *
   * @param initiativeId
   * @returns {Promise<T>} Which delivers the requested Handlungsfelder.
   */
  list(initiativeId?: number): Promise<any> {
    const d = new Deferred();
    let options = {};
    if (initiativeId) {
      options = {
        filter: {
          initiative_id: initiativeId
        }
      };
    }
    this.getEntity(Handlungsfeld.entity, options).subscribe(
      response => {
        d.resolve(Handlungsfeld.fromResponse(response));
      }
    );
    return d.promise;
  }

  public save(handlungsfeld: Handlungsfeld){
    return this.patchEntity(handlungsfeld, Handlungsfeld.entity);
  }
}
