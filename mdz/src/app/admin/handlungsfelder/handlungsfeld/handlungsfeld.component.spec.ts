import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandlungsfeldComponent } from './handlungsfeld.component';

describe('HandlungsfeldComponent', () => {
  let component: HandlungsfeldComponent;
  let fixture: ComponentFixture<HandlungsfeldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandlungsfeldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandlungsfeldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
