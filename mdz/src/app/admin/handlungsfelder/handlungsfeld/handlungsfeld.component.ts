import {Component, Input, Output, EventEmitter, ViewChild, ElementRef} from '@angular/core';
import {Handlungsfeld} from '../handlungsfeld.model';
import {HandlungsfelderService} from '../handlungsfelder.service';
import {TranstoastService} from "../../../common/transtoast.service";

@Component({
  selector: 'app-handlungsfeld',
  templateUrl: './handlungsfeld.component.html',
  styleUrls: ['./handlungsfeld.component.scss']
})
export class HandlungsfeldComponent {

  private edit = false;

  @Input() handlungsfeld: Handlungsfeld;
  @Output() onDeleted: EventEmitter<Handlungsfeld> = new EventEmitter();

  constructor(private handlungsfelderService: HandlungsfelderService,
              private toastr: TranstoastService) {
  }

  onKeyUp(event){
    if(event.key === "Enter" || event.code === "Enter" || event.keyCode === 13){
      this.btnSaveClicked();
    }
  }

  btnEditClicked() {
    this.edit = !this.edit;
  }

  btnSaveClicked() {
    this.handlungsfelderService.save(this.handlungsfeld).subscribe(
      successResponse => {
        if (successResponse.statusText === 'OK') {
          this.edit = false;
        }
      },
      errorResponse => {
        this.toastr.error(errorResponse.json().error.message);
      }
    );
  }

  btnTrashClicked() {
    this.handlungsfelderService.deleteEntity(this.handlungsfeld).subscribe(
      response => {
        if (response.statusText === 'OK') {
          this.onDeleted.emit(this.handlungsfeld);
        }else{
          this.toastr.warning('Da ist etwas schief gelaufen.');
        }
      },
      errorResponse => {
        const message = errorResponse.json().error.message;
        if (message.indexOf('Integrity') > -1) {
          this.toastr.error('Dieses Handlungsfeld ist in Benutzung und kann nicht gelöscht werden.');
        }
      }
    );
  }
}
