export class Handlungsfeld {

  /**
   * API Endpoint
   * @type {string}
   */
  public static entity = 'handlungsfeld';


  /**
   * Takes a Dreamfactory API response and makes extracts Handlungsfelder from it.
   * Returns a single Handlungsfeld when only one was fetched.
   * Returns an array of Handlungsfeld when multiple were fetched.
   * @param json
   * @returns {any}
   */
  public static fromResponse(json: any) {
    // a single Handlungsfeld
    if (json.json().resource === undefined) {
      return Handlungsfeld.fromJson(json);
    }

    // an array of handlungsfelder
    const hfa: Handlungsfeld[] = [];
    for (const hf of json.json().resource) {
      hfa.push(Handlungsfeld.fromJson(hf));
    }
    return hfa;
  }

  /**
   * Factory, Takes a json object and makes a Handlungsfeld from it
   * @param json
   * @returns {Handlungsfeld}
   */
  public static fromJson(json: any) {
    // a single Handlungsfeld
    return new Handlungsfeld(
      json.name,
      json.id
    );
  }

  constructor(private name: string, private id?: number) {
  }

  /**
   * Makes a json object from this Handlungsfelds data
   * @returns {{id: number, name: string}}
   */
  public toJson() {
    return {
      id: this.id,
      name: this.name
    };
  }

  /**
   * Setter for id
   * @param id
   */
  public setId(id: number) {
    this.id = id;
  }

  /**
   * Getter for id
   * @returns {number}
   */
  public getId() {
    return this.id;
  }

  /**
   * Getter for the name
   * @returns {string}
   */
  public getName() {
    return this.name;
  }
}
