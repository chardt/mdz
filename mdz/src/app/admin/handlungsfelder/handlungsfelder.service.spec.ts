import { TestBed, inject } from '@angular/core/testing';

import { HandlungsfelderService } from './handlungsfelder.service';

describe('HandlungsfelderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HandlungsfelderService]
    });
  });

  it('should be created', inject([HandlungsfelderService], (service: HandlungsfelderService) => {
    expect(service).toBeTruthy();
  }));
});
