import {Component, OnInit} from '@angular/core';
import {StateService} from '@uirouter/angular';
import {SessionService} from '../user/session/services/session.service';
import {ToastsManager} from 'ng2-toastr';
import {TranstoastService} from '../common/transtoast.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  /**
   * Value to use for the account activation.
   */
  private registerId: string;

  /**
   * Email of the account that should be activated.
   */
  private userEmail: string;

  /**
   * The password that the user chose.
   */
  private newPassword: string;

  /**
   * Validation to the new password.
   */
  private newPasswordCopy: string;

  constructor(private stateService: StateService,
              private sessionService: SessionService,
              private toastr: TranstoastService) {
  }

  ngOnInit() {
    this.registerId = this.stateService.params.register_id;
    this.userEmail = this.stateService.params.email;
  }

  /**
   * Utilizes the SessionServices activation method.
   */
  activate() {

    this.sessionService
      .activateAccount(this.registerId, this.userEmail, this.newPassword, this.newPasswordCopy)
      .subscribe(
        response => {
          if (response.json().success) {
            this.sessionService.login({email: this.userEmail, password: this.newPassword})
              .then(
                user => {
                  this.stateService.go('forceprofileimage');
              });
          } else {
            this.toastr.error('Hier ist etwas schief gelaufen.');
          }
        },
        errorResponse => {
          this.toastr.error(errorResponse.json().error.message);
        }
      );
  }
}
