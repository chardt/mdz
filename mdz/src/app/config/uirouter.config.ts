import {UIRouter} from '@uirouter/angular';
import swal from 'sweetalert2';
export function uiRouterConfigFn(router: UIRouter) {

  router.urlService.rules.otherwise({state: 'landing-page'});

  // match state exits from "edit" and "create"
  const editExitMatch = {from: 'edit|create'};

  // ask the user it the transition should happen.
  const editExitFn = () => {
    if (!router.stateService.current.params.saved) {
      return new Promise<boolean>((resolve, reject) => {
        swal({
          title: 'Abbrechen?',
          text: 'Wenn du die Seite verlässt ohne zu speichern, gehen all deine Änderungen verloren.',
          showCancelButton: true,
          cancelButtonText: 'Nicht verlassen',
          confirmButtonText: 'Ja, Seite verlassen',
          type: 'question'
        }).then(resolve, reject);
      });
    }
  };

  // register match and function.
  router.transitionService.onExit(editExitMatch, editExitFn);

  // scroll to top of page after each successful transition.
  router.transitionService.onSuccess({from:'*'}, () => {
    document.body.scrollTop = document.documentElement.scrollTop = 0;
  })
}
