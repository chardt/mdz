import {ToastOptions} from 'ng2-toastr';
export class ToastConfig extends ToastOptions {
  animate = 'flyRight'; // you can override any options available
  newestOnTop = true;
  showCloseButton = true;
  // dismiss = 'click';
  toastLife = 4000;
  positionClass = 'toast-top-full-width';
}
