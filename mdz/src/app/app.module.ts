import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppComponent} from './app.component';
import {ToastModule, ToastOptions} from 'ng2-toastr/ng2-toastr';
import {ToastConfig} from './config/ToastConfig';
import {RegisterComponent} from './user/register/register.component';
import {LoginComponent} from './user/session/login/login.component';
import {LogoutComponent} from './user/session/logout/logout.component';
import {InitiativeComponent} from './initiative/discover/initiative.component';
import {SessionService} from './user/session/services/session.service';
import {InfoboxComponent} from './user/infobox/infobox.component';
import {UIRouterModule} from '@uirouter/angular';
import {ProfileComponent} from './user/profile/profile.component';
import {SingleInitiativeComponent} from './initiative/single-initiative/single-initiative.component';
import {SupporterComponent} from './initiative/supporter/supporter.component';
import {UserMenuComponent} from './navigation/user-menu/user-menu.component';
import {AdminMenuComponent} from './navigation/admin-menu/admin-menu.component';
import {TopMenuComponent} from './navigation/top-menu/top-menu.component';
import {GuestMenuComponent} from './navigation/guest-menu/guest-menu.component';
import {LandingPageComponent} from './landing-page/landing-page.component';
import {VisionComponent} from './landing-page/vision/vision.component';
import {ExplanationComponent} from './landing-page/explanation/explanation.component';
import {ParticipationComponent} from './landing-page/participation/participation.component';
import {FeaturedComponent} from './landing-page/featured/featured.component';
import {FooterComponent} from './layout/footer/footer.component';
import {BtnHomeComponent} from './common/btn-home/btn-home.component';
import {MyInitiativesComponent} from './initiative/my-initiatives/my-initiatives.component';
import {CreateComponent} from './initiative/create/create.component';
import {ShowComponent} from './initiative/show/show.component';
import {ShowService} from './initiative/show/show.service';
import {SidebarComponent} from './initiative/show/sidebar/sidebar.component';
import {HeadingComponent} from './initiative/show/heading/heading.component';
import {GalleryComponent} from './initiative/show/gallery/gallery.component';
import {TagsComponent} from './initiative/tags/tags.component';
import {CommentsComponent} from './initiative/show/comments/comments.component';
import {CommentComponent} from './initiative/show/comments/comment/comment.component';
import {uiRouterConfigFn} from './config/uirouter.config';
import {GeroComponent} from './landing-page/gero/gero.component';
import {QuotesComponent} from './landing-page/quotes/quotes.component';
import {FeaturedInitiativeComponent} from './landing-page/featured/featured-initiative/featured-initiative.component';
import {IforgotComponent} from './user/iforgot/iforgot.component';
import {FiltersComponent} from './initiative/discover/filters/filters.component';
import {AdminInitiativenComponent} from './admin/admin-initiativen/admin-initiativen.component';
import {HandlungsfelderComponent} from './admin/handlungsfelder/handlungsfelder.component';
import {HandlungsfeldComponent} from './admin/handlungsfelder/handlungsfeld/handlungsfeld.component';
import {OrtsgemeindenComponent} from './admin/ortsgemeinden/ortsgemeinden.component';
import {OrtsgemeindeComponent} from './admin/ortsgemeinden/ortsgemeinde/ortsgemeinde.component';
import {NotificationSettingsComponent} from './initiative/show/sidebar/notification-settings/notification-settings.component';
import {EditComponent} from './initiative/edit/edit.component';
import {AdminUserComponent} from './admin/admin-user/admin-user.component';
import {UserCardComponent} from './admin/admin-user/user-card/user-card.component';
import {ReportedUsersComponent} from './admin/admin-user/reported-users/reported-users.component';
import {ReportedUserComponent} from './admin/admin-user/reported-users/reported-user/reported-user.component';
import {UiSwitchModule} from 'ng2-ui-switch/dist';
import {BaseSixtyFourInputComponent} from './common/base-sixty-four-input/base-sixty-four-input.component';
import {TinymceModule} from 'angular2-tinymce';
import {CropperComponent} from './user/profile/cropper/cropper.component';
import {AdminLandingpageComponent} from './admin/admin-landingpage/admin-landingpage.component';
import {InitiaiveListComponent} from './admin/initiaive-list/initiaive-list.component';
import {AgmCoreModule} from '@agm/core';
import {ImageUploadComponent} from './common/image-upload/image-upload.component';
import {WelcomeComponent} from './welcome/welcome.component';
import {RulesComponent} from './rules/rules.component';
import {BackgroundInformationComponent} from './background-information/background-information.component';
import {NoLinkPipe} from './common/no-link.pipe';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {CmsComponent} from './admin/cms/cms.component';
import {CmsPagesComponent} from './navigation/cms-pages/cms-pages.component';
import {PageComponent} from './page/page.component';
import {FacebookModule} from 'ngx-facebook';
import {VersionComponent} from './version/version.component';
import {ResetComponent} from './user/iforgot/reset/reset.component';
import {TranstoastService} from './common/transtoast.service';
import {AvatarService} from './user/profile/avatar/avatar.service';
import {AvatarComponent} from './user/profile/avatar/avatar.component';
import {LoaderComponent} from './common/loader/loader.component';
import {CmsService} from './admin/cms/cms.service';
import {ContactComponent} from './user/profile/contact/contact.component';
import {InvitationsComponent} from './user/profile/invitations/invitations.component';
import {InvitationComponent} from './user/profile/invitations/invitation/invitation.component';
import {MultiSelectComponent} from './common/multiselect/multiselect.component';
import {MyOwnInitiativesComponent} from './initiative/my-initiatives/my-own-initiatives/my-own-initiatives.component';
import {PrivacyInfoComponent} from './common/privacy-info/privacy-info.component';
import {RetinaService} from "./common/retina.service";
import {PleaseprofileimageComponent} from './user/profile/pleaseprofileimage/pleaseprofileimage.component';
import {DateTimePickerModule} from "ng-pick-datetime";
import {DateFnsModule} from "ngx-date-fns";
import { HandbuchComponent } from './handbuch/handbuch.component';
import { NewsletterComponent } from './admin/newsletter/newsletter.component';

const states = [
  {name: 'landing-page', url: '/', component: LandingPageComponent},
  {name: 'page', url: '/page/:url', component: PageComponent},
  {name: 'login', url: '/login', component: LoginComponent},
  {name: 'logout', url: '/logout', component: LogoutComponent},
  {name: 'register', url: '/register', component: RegisterComponent},
  {name: 'iforgot', url: '/iforgot', component: IforgotComponent},
  {name: 'reset', url: '/iforgot/:code', component: ResetComponent},
  {name: 'initiatives', url: '/initiatives', component: InitiativeComponent},
  {name: 'my-initiatives', url: '/my-initiatives', component: MyInitiativesComponent},
  {name: 'create', url: '/create', params: {saved: false}, component: CreateComponent},
  {name: 'show', url: '/show/:initiativeId', component: ShowComponent},
  {name: 'edit', url: '/edit/:initiativeId', params: {saved: false}, component: EditComponent},
  {name: 'profile', url: '/profile', component: ProfileComponent},
  {name: 'user', url: '/user/:username', component: ProfileComponent},

  {name: 'admin-initiatives', url: '/admin-initiatives', component: AdminInitiativenComponent},
  {name: 'admin-landingpage', url: '/admin-landingpage', component: AdminLandingpageComponent},
  {name: 'admin-user', url: '/users', component: AdminUserComponent},
  {name: 'cms', url: '/cms', component: CmsComponent},
  {name: 'manage-newsletter', url: '/manage-newsletter', component: NewsletterComponent},

  {name: 'welcome', url: '/welcome/:register_id/:email', component: WelcomeComponent},
  {name: 'forceprofileimage', url: '/lookinggood', component: PleaseprofileimageComponent},

  {name: 'background-information', url: '/hintergrund', component: BackgroundInformationComponent},
  {name: 'rules', url: '/spielregeln', component: RulesComponent},

  {name: 'version', url: '/version', component: VersionComponent},
  {name: 'handbuch', url: '/handbuch', component: HandbuchComponent}
];


@NgModule({
  declarations: [
    AppComponent,
    // TooltipDirective,
    RegisterComponent,
    LoginComponent,
    LogoutComponent,
    InitiativeComponent,
    InfoboxComponent,
    ProfileComponent,
    SingleInitiativeComponent,
    SupporterComponent,
    UserMenuComponent,
    AdminMenuComponent,
    TopMenuComponent,
    GuestMenuComponent,
    LandingPageComponent,
    VisionComponent,
    ExplanationComponent,
    ParticipationComponent,
    FeaturedComponent,
    FooterComponent,
    BaseSixtyFourInputComponent,
    BtnHomeComponent,
    MyInitiativesComponent,
    CreateComponent,
    ShowComponent,
    SidebarComponent,
    HeadingComponent,
    GalleryComponent,
    CommentsComponent,
    TagsComponent,
    CommentComponent,
    GeroComponent,
    QuotesComponent,
    FeaturedInitiativeComponent,
    IforgotComponent,
    FiltersComponent,
    AdminInitiativenComponent,
    HandlungsfelderComponent,
    HandlungsfeldComponent,
    OrtsgemeindenComponent,
    OrtsgemeindeComponent,
    NotificationSettingsComponent,
    EditComponent,
    AdminUserComponent,
    UserCardComponent,
    ReportedUsersComponent,
    ReportedUserComponent,
    MultiSelectComponent,
    CropperComponent,
    AdminLandingpageComponent,
    InitiaiveListComponent,
    ImageUploadComponent,
    WelcomeComponent,
    RulesComponent,
    BackgroundInformationComponent,
    NoLinkPipe,
    CmsComponent,
    CmsPagesComponent,
    PageComponent,
    VersionComponent,
    ResetComponent,
    AvatarComponent,
    LoaderComponent,
    ContactComponent,
    InvitationsComponent,
    InvitationComponent,
    MyOwnInitiativesComponent,
    PrivacyInfoComponent,
    PleaseprofileimageComponent,
    HandbuchComponent,
    NewsletterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    UiSwitchModule,
    HttpModule,
    BrowserAnimationsModule,
    TinymceModule.withConfig(
      {
        language_url: '/assets/tinymce/langs/de_DE.js',
        menubar: false,
        toolbar: 'styleselect bold italic underline bullist numlist',
        style_formats: [
          {
            title: 'Überschriften', items: [
            {title: 'Ebene 1', block: 'h1'},
            {title: 'Ebene 2', block: 'h2'},
            {title: 'Ebene 3', block: 'h3'}
          ]
          },
          {
            title: 'Text', block: 'p'
          }
        ]
        // ,
        // setup: (editor) => {
        //   console.log(editor);
        //   editor.addButton('Test', {
        //     text: "Datum",
        //     onclick: () => {
        //       alert('penis');
        //     }
        //   });
        // }
      }
    ),
    ToastModule.forRoot(),
    UIRouterModule.forRoot(
      {
        states: states,
        useHash: true,
        config: uiRouterConfigFn

      }
    ),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAjHVEze6AaryVREpYKfIOn02nXtsbXMII'
    }),
    InfiniteScrollModule,
    FacebookModule.forRoot(),
    DateTimePickerModule,
    DateFnsModule
  ],
  providers: [
    TranstoastService,
    SessionService,
    ShowService,
    {provide: ToastOptions, useClass: ToastConfig},
    AvatarService,
    CmsService,
    RetinaService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
