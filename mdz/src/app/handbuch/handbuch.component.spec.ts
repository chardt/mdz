import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandbuchComponent } from './handbuch.component';

describe('HandbuchComponent', () => {
  let component: HandbuchComponent;
  let fixture: ComponentFixture<HandbuchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandbuchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandbuchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
