import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-handbuch',
  templateUrl: './handbuch.component.html',
  styleUrls: ['./handbuch.component.scss']
})
export class HandbuchComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  private scroll(anchor){
    const element = document.querySelector("#" + anchor);
    if (element) { element.scrollIntoView(element); }
  }

}
