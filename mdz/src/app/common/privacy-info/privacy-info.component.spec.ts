import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivacyInfoComponent } from './privacy-info.component';

describe('PrivacyInfoComponent', () => {
  let component: PrivacyInfoComponent;
  let fixture: ComponentFixture<PrivacyInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivacyInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivacyInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
