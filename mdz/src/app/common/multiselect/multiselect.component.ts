import {
  Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter, ElementRef,
  HostBinding, HostListener
} from '@angular/core';

@Component({
  selector: 'hyper-multiselect',
  templateUrl: './multiselect.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./multiselect.component.scss']
})

export class MultiSelectComponent implements OnInit {
  @Input() inbound: Array<any>;
  @Input() displayKey: String;
  @Input() allSelected: Boolean;
  @Input() groupBy: string;
  @Input() emptyTitle: string;

  @Output() outbound: EventEmitter<Array<any>> = new EventEmitter();
  @Output() opened: EventEmitter<any> = new EventEmitter();
  @Output() closed: EventEmitter<any> = new EventEmitter();


  groups: Array<any>;
  dropDownVisible = false;
  selectedItems: Array<any>;
  inboundSelector: Array<any>;
  filterVal: String;

  constructor(private _eref: ElementRef) {
    this.selectedItems = [];
  }

  @HostListener('document:click',['$event'])
  hostclick($event) {
    this.collapse($event);
  }

  getSelectedItems(): Array<any> {
    return this.selectedItems;
  }

  toggleSelection(item, event) {
    if (item.hyperMSIsGroup) {
      item.hyperMSSelected = !item.hyperMSSelected;
      this.inbound.forEach(subItem => {
        if (subItem[this.groupBy] === item.hyperMSName) {
          if (item.hyperMSSelected) {
            this.selectItem(subItem);
          } else {
            this.deselectItem(subItem);
          }
        }
      });
    } else {
      if (item.hyperMSSelected) {
        this.deselectItem(item);
      } else {
        this.selectItem(item);
      }
      if (this.groups.length > 0) {
        this.checkGroupSelected(item[this.groupBy]);
      }
      event.stopPropagation();
    }
    this.notifyParent();
  }

  notifyParent() {
    this.outbound.emit(this.getSelectedItems());
  }

  checkGroupSelected(groupName) {
    const group = this.groups.filter(item => item.hyperMSName === groupName)[0];
    const noCount = this.inbound.filter(item => item[this.groupBy] === groupName)
      .reduce(function (count, item) {
        // tslint:disable-next-line
        return count + !item.hyperMSSelected | 0;
      }, 0);
    group.hyperMSSelected = noCount === 0;
  }

  selectItem(item) {
    item.hyperMSSelected = true;
    this.selectedItems = [...this.selectedItems, item];
  }

  deselectItem(item) {
    item.hyperMSSelected = false;
    const index = this.selectedItems.indexOf(item);
    this.selectedItems = [
      ...this.selectedItems.slice(0, index),
      ...this.selectedItems.slice(index + 1)
    ];
  }

  selectAll() {
    this.groups.forEach(object => {
      object.hyperMSSelected = true;
    });
    this.inbound.forEach(object => {
      object.hyperMSSelected = true;
    });

    this.selectedItems = [...this.inbound];
    this.notifyParent();
  }

  selectNone() {
    this.groups.forEach(object => {
      object.hyperMSSelected = false;
    });
    this.inbound.forEach(object => {
      object.hyperMSSelected = false;
    });

    this.selectedItems = [];
    this.notifyParent();
  }

  createGroups() {
    this.groups = [];
    if (this.groupBy) {
      const groupVals = [];
      this.inbound.forEach(item => {
        if (groupVals.indexOf(item[this.groupBy].toLowerCase()) === -1) {
          groupVals.push(item[this.groupBy].toLowerCase());
        }
      });

      groupVals.forEach(group => {
        this.groups.push({hyperMSName: group, hyperMSSelected: false, hyperMSIsGroup: true});
      });
    } else {
      this.groups = [{name: 'hyperMSPlaceHolderGroup'}];
    }
  }

  showToggle() {
    this.dropDownVisible = !this.dropDownVisible;
    if (this.dropDownVisible) {
      // tell parent
      this.opened.emit(this);
    }
  }

  collapse(event) {
    // Checks to see if click is inside element; if not, collapse element
    if (!this._eref.nativeElement.contains(event.target)) {
      this.dropDownVisible = false;
      this.closed.emit(this);
    }
  }

  filterVals(filter, value, displayKey) {
    if (filter) {
      return value.filter(item => item[displayKey].indexOf(filter) !== -1);
    }
    return value;
  }

  onKey() {
    this.inboundSelector = this.filterVals(this.filterVal, this.inbound, this.displayKey);
  }

  ngOnInit() {
    this.createGroups();
    if (this.allSelected) {
      this.selectAll();
    }
    this.inboundSelector = this.filterVals('', this.inbound, this.displayKey);
  }
}
;
