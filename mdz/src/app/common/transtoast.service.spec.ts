import { TestBed, inject } from '@angular/core/testing';

import { TranstoastService } from './transtoast.service';

describe('TranstoastService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TranstoastService]
    });
  });

  it('should be created', inject([TranstoastService], (service: TranstoastService) => {
    expect(service).toBeTruthy();
  }));
});
