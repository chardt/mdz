import {Headers, Http, RequestOptions} from '@angular/http';
import * as constants from '../../config/constants';
import {Observable} from 'rxjs/Observable';
import {isNullOrUndefined} from 'util';
import {TranstoastService} from '../transtoast.service';

export class BaseHttpService {

  constructor(protected http: Http,
              protected toastr: TranstoastService) {
  }

  /**
   * Removes the token from local storage-
   */
  public clearToken() {
    localStorage.removeItem('session_token');
  }

  /**
   * Sets the token in the local storage
   * @param token to save
   */
  public setToken(token: string) {
    localStorage.setItem('session_token', token);
  }

  /**
   * Returns the token from the local storage, if it exists
   * @returns {any}
   */
  public getToken() {
    const token = localStorage.getItem('session_token');
    if (token === 'null') {
      return null;
    }
    return token;
  }

  /**
   * Creates the default HTTP headers that are used throughout the API connector class.
   * @returns {Headers}
   */
  protected getHeaders(): Headers {

    const queryHeaders = new Headers();
    queryHeaders.append('Content-Type', 'application/json; charset=utf-8');
    queryHeaders.append('X-DreamFactory-API-Key', constants.DREAMFACTORY_API_KEY);

    const token = this.getToken();
    if (token !== null) {
      queryHeaders.append('X-DreamFactory-Session-Token', token);
    }

    return queryHeaders;
  }

  /**
   * Creates the default HTTP Request Options (see getHeaders)
   * @returns {RequestOptions}
   */
  public getDefaultHttpOptions(): RequestOptions {
    return new RequestOptions({headers: this.getHeaders()});
  }

  /**
   * Takes a String and creates a Dreamfactory endpoint URL from it.
   * @param endpoint
   * @returns string
   */
  public endpoint(endpoint: string): string {
    if (!endpoint) {
      return null;
    }
    return constants.DREAMFACTORY_INSTANCE_URL + '/api/v2' + endpoint;
  }

  /**
   * Takes a String, that is the name of a mysql table and creates an endpoint URL from it.
   * @param endpoint
   * @returns string
   */
  protected entityFor(endpoint: string): string {
    if (!endpoint) {
      return null;
    }
    return constants.DREAMFACTORY_INSTANCE_URL + '/api/v2/mysql/_table/' + endpoint;
  }

  /**
   * Performs a GET request to the API.
   *
   * @param entityName name of the table to fetch.
   * @param options config object that is converted into a query string.
   * @param systemCall true, when the call goes to the dreamfactory system, false if to the other services.
   * @returns {Observable<Response>}
   */
  public getEntity(entityName: string, options?: any, systemCall?: boolean): Observable<any> {

    let endpoint: string;

    if (!systemCall) {
      endpoint = this.entityFor(entityName);
    } else {
      endpoint = this.endpoint(entityName);
    }

    // modify the request url with these options
    if (options) {

      // add the ID of the entity to fetch
      if (options.id) {
        endpoint += '/' + options.id;
      }

      // begin the modifier query
      let query = '?';

      if (options.related !== undefined) {
        query += '&related=';
        for (let relationIndex = 0; relationIndex < options.related.length; relationIndex++) {
          const relation = options.related[relationIndex];
          query += relation;
          if (relationIndex < options.related.length - 1) {
            query += ',';
          }
        }
      }

      // filter the result by these attributes:
      if (options.filter !== undefined) {

        // handle a filter string
        if (typeof options.filter === 'string') {
          query += '&filter=' + options.filter;
        } else {
          // handle a filter object
          query += '&filter=(';
          let i = 1;
          const numberOfFilters = Object.keys(options.filter).length;
          // tslint:disable-next-line
          for (const key in options.filter) {
            if (typeof options.filter[key] !== 'object') {
              query += '(' + key + '=' + options.filter[key] + ')';
            }

            // evaluate filters with options
            // tslint:disable-next-line
            if (typeof options.filter[key] === 'object') {
              const filterConf = options.filter[key];
              query += '(' + key + filterConf.type + filterConf.value + ')';
            }

            // see if there are more filters
            if (i < numberOfFilters && numberOfFilters > 1) {
              query += 'and';
            }
            i++;
          }
          query += ')';
        }
      }

      if (options.search) {

        const filterExists = (query.indexOf('&filter=') > -1);

        let filter = (!filterExists) ? '&filter=' : 'and(';


        let i = 1;
        const numberOfFilters = Object.keys(options.search).length;
        // tslint:disable-next-line
        for (const key in options.search) {
          filter += '(' + key + '%20like%20%25' + options.search[key] + '%25)';
          if (i < numberOfFilters && numberOfFilters > 1) {
            filter += 'or';
          }
          i++;
        }
        if (filterExists) {
          filter += ')';
        }
        query += filter;
      }

      // transform the list of IDs to a filter query to avoid http500
      if (options.ids !== undefined) {

        const filterExists = (query.indexOf('&filter=') > -1);
        let filter = (!filterExists) ? '&filter=' : 'and(';

        let ids = options.ids;
        if (typeof ids === 'string') {
          ids = ids.split(',');
        }
        let i = 1;
        const numberOfIds = Object.keys(ids).length;
        for (const id of ids) {
          filter += '(id=' + id + ')';
          if (i < numberOfIds && numberOfIds > 1) {
            filter += 'or';
          }
          i++;
        }

        if (filterExists) {
          filter += ')';
        }
        query += filter;
        // query += '&ids=' + options.ids;
      }

      if (options.limit !== undefined) {
        query += '&limit=' + options.limit;
      }

      if (options.offset !== undefined) {
        query += '&offset=' + options.offset;
      }

      if (options.includeCount === true) {
        query += '&include_count=true';
      }

      if (options.countOnly === true) {
        query += '&count_only=true';
      }

      // order the result as specified
      if (options.order !== undefined) {
        query += '&order=' + options.order.by;
        if (options.order.direction !== undefined) {
          query += ' ' + options.order.direction;
        }
      }

      if (options.id_field !== undefined) {
        query += '&id_field=';
        const fieldKeys = Object.keys(options.id_field);

        for (let fieldIndex = 0; fieldIndex < fieldKeys.length; fieldIndex++) {
          const key = fieldKeys[fieldIndex];
          query += key + '=' + options.id_field[key];
          if (fieldIndex < options.id_field.length - 1) {
            query += ',';
          }
        }
      }

      if (options.fields !== undefined && options.fields.length > 0) {
        query += '&fields=';
        for (let fieldIndex = 0; fieldIndex < options.fields.length; fieldIndex++) {
          query += options.fields[fieldIndex];
          if (fieldIndex + 1 < options.fields.length) {
            query += ',';
          }
        }
      }


      if (options.continue === true) {
        query += '&continue=true';
      }

      // when the query has filters or order instructions, its no longer
      // just a question mark. Add it to the request.
      if (query !== '?') {
        endpoint += query;
      }


    }
    return this.http.get(
      endpoint,
      this.getDefaultHttpOptions());
  }

  /**
   * Performs a POST request to the API.
   *
   * @param object to POST.
   * @param endpoint to POST the object to.
   * @param serviceCall true if the deamfactory system is called.
   * @returns {Observable<Response>}
   */
  public postEntity(object: any, endpoint?: string, serviceCall?: boolean): Observable<any> {

    if (serviceCall) {
      endpoint = constants.DREAMFACTORY_INSTANCE_URL + '/api/v2/' + endpoint;
      return this.http.post(endpoint, object, this.getDefaultHttpOptions());
    }

    const resource = {
      resource: [object]
    };
    if (!endpoint) {
      endpoint = object.constructor.name.toLocaleLowerCase();
    }
    return this.http.post(
      this.entityFor(endpoint),
      resource,
      this.getDefaultHttpOptions()
    );
  }

  /**
   * Performs a PUT request to the API.
   *
   * @param object to PUT.
   * @param endpoint to PUT the object to.
   * @returns {Observable<Response>}
   */
  public putEntity(object: any, endpoint?: string): Observable<any> {
    const resource = {
      resource: [object]
    };
    if (!endpoint) {
      endpoint = object.constructor.name.toLocaleLowerCase();
    }

    if (object.id) {
      endpoint += '/' + object.id;
    }

    return this.http.put(
      this.entityFor(endpoint),
      resource,
      this.getDefaultHttpOptions()
    );
  }

  /**
   * Takes an endpoint string and decides, whether this request is directed to a collection or a specific Entity.
   * If the Endpoint ends with "/23" or "/Number" it is directed to an entity. If not, the to a collection.
   * Prior to any tests, the query string is removed (Everything after "?")
   *
   * @param endpoint
   * @returns {boolean}
   */
  private requestEndsWithTargetId(endpoint: string) {

    // variables needed to check if the request ends with an ID or not
    let dontWrap = false;
    let endIstheID = false;
    let splitOccured = false;


    // get the Endpoint without the query
    const endpoint_string = endpoint.split('?');
    // wrap only if last element is NOT a number -> if the ID is passed in the URL, it is the last element.
    const elements = endpoint_string[0].split('/');

    // if no split was done (resulting array counts only one) there was no string to split on.
    // thus, the last element cannot be the ID.
    splitOccured = (elements.length > 1);

    // a split was performed successfully. Now check if the last element is a number.
    if (splitOccured) {
      endIstheID = (!isNaN(Number(elements[elements.length - 1])));
    }

    dontWrap = (splitOccured && endIstheID);

    return dontWrap;
  }

  /**
   * Performs a PATCH request to the API.
   *
   * @param object with the fields/values to patch.
   * @param endpoint of the entity to patch.
   * @param systemCall true if the deamfactory system is called.
   * @param idField (optional) name of the field that contains the entity id.
   * @returns {Observable<Response>}
   */
  public patchEntity(object: any, endpoint?: string, systemCall?: boolean, idField?: string): Observable<any> {

    const options = this.getDefaultHttpOptions();

    let resource;

    if (systemCall) {
      endpoint = this.endpoint(endpoint);
    } else {
      endpoint = this.entityFor(endpoint);
    }

    if (!endpoint) {
      endpoint = object.constructor.name.toLocaleLowerCase();
    }

    if (idField) {
      endpoint += '/' + object[idField];
      endpoint += '?id_field=' + idField;
    }

    // if the last element is the ID OR the idField is given, take the object directly as resource.
    // if not, there is no specific entity that shout be patched and the request goes to the general endpoint.
    // Then we must wrap the object into a resource object.
    if (this.requestEndsWithTargetId(endpoint) || !isNullOrUndefined(idField)) {
      resource = object;
    } else {
      resource = {
        resource: [object]
      };
    }

    return this.http.patch(
      endpoint,
      resource,
      options
    );
  }

  /**
   * Performs a DELETE request to the API.
   * @param object to delete.
   * @param endpoint to delete the object from (optional).
   * @returns {Observable<Response>}
   */
  public deleteEntity(object: any, endpoint?: string): Observable<any> {

    if (!endpoint) {
      endpoint = object.constructor.name.toLocaleLowerCase();
    }
    endpoint += '/' + object.id;
    endpoint = this.entityFor(endpoint);

    return this.http.delete(endpoint, this.getDefaultHttpOptions());
  }

  /**
   * Convenience Method to access the error toaster.
   * @param errorResponse
   */
  protected error(errorResponse) {
    this.toastr.error(errorResponse.json().error.message);
  }
}
