import {Injectable} from '@angular/core';

@Injectable()
export class RetinaService {

  private retina = false;

  constructor() {
    const mediaQuery = '(-webkit-min-device-pixel-ratio: 1.5), ' +
      '(min--moz-device-pixel-ratio: 1.5), ' +
      '(-o-min-device-pixel-ratio: 3/2), ' +
      '(min-resolution: 1.5dppx)';
    if (window.devicePixelRatio > 1) {
      this.retina = true;
    }

    this.retina = (window.matchMedia && window.matchMedia(mediaQuery).matches);
  }

  public src(url: string) {

    if (!this.retina) {
      return url;
    }

    let pathParts = url.split('/');
    let file = pathParts.pop();
    let fileParts = file.split('.');

    if (fileParts.length < 2) {
      return url + '@2x';
    }
    fileParts[fileParts.length - 2] += '@2x';
    pathParts.push(fileParts.join('.'));
    return pathParts.join('/');
  }
}
