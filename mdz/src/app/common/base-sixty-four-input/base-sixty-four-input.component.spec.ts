import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseSixtyFourInputComponent } from './base-sixty-four-input.component';

describe('AppBaseSixtyFourInputComponent', () => {
  let component: BaseSixtyFourInputComponent;
  let fixture: ComponentFixture<BaseSixtyFourInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseSixtyFourInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseSixtyFourInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
