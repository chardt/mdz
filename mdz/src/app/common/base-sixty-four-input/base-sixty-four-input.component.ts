import {Component, Output, EventEmitter, Input} from '@angular/core';

@Component({
  selector: 'base-sixty-four-input',
  templateUrl: './base-sixty-four-input.component.html',
  styleUrls: ['./base-sixty-four-input.component.scss']
})
export class BaseSixtyFourInputComponent {

  /**
   * Holds the entire File, which has been selected, in base64 representation
   * @type {string}
   */
  private base64textString = '';

  private reader: FileReader = null;

  /**
   * A string that is added to the input ID and label, so it can be clearly identified.
   * This is optional for single inputs. If multiple instances of the input are used simultaneously,
   * the designator must be not null and unique for each instance.
   */
  @Input() desgination: string;

  /**
   * Event Emitter to be fired, when the selected image was been converted into bse64 representation.
   * @type {EventEmitter}
   */
  @Output() imageConverted: EventEmitter<string> = new EventEmitter();

  /**
   * Event Emitter to be fired, when a new file has been selected.
   * @type {EventEmitter}
   */
  @Output() fileInfoChanged: EventEmitter<any> = new EventEmitter();


  /**
   * Will be fired by the input when a (new) file has been selected.
   * It chooses the first file from the input and trigger the base64 conversion.
   * @param evt
   */
  onSelectedFileChanged(evt) {
    const files = evt.target.files;
    const file = files[0];

    if (files && file) {
      this.fileInfoChanged.emit(evt);
      this.reader = new FileReader();

      /**
       *  IE 11 users won't have readAsBinaryString.
       *  Polyfills didn't work, se we divert manually.
       */
      if (FileReader.prototype.readAsBinaryString) {
        this.reader.onload = this._handleReaderLoaded.bind(this);
        this.reader.readAsBinaryString(file);
      } else {
        // make the conversion in IE11
        this.blobToBinaryStringIE11(file);
      }
    }
  }

  /**
   * Converts a string to base64.
   * Here it's used to convert files (images), that are loaded with a image/plain MIME type.
   * @param str
   * @returns {string}
   */
  base64Encode(str) {
    var CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    var out = "", i = 0, len = str.length, c1, c2, c3;
    while (i < len) {
      c1 = str.charCodeAt(i++) & 0xff;
      if (i == len) {
        out += CHARS.charAt(c1 >> 2);
        out += CHARS.charAt((c1 & 0x3) << 4);
        out += "==";
        break;
      }
      c2 = str.charCodeAt(i++);
      if (i == len) {
        out += CHARS.charAt(c1 >> 2);
        out += CHARS.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
        out += CHARS.charAt((c2 & 0xF) << 2);
        out += "=";
        break;
      }
      c3 = str.charCodeAt(i++);
      out += CHARS.charAt(c1 >> 2);
      out += CHARS.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
      out += CHARS.charAt(((c2 & 0xF) << 2) | ((c3 & 0xC0) >> 6));
      out += CHARS.charAt(c3 & 0x3F);
    }
    return out;
  }

  /**
   * Takes a File from the FileReader (as blob) and converts in to base64.
   * It does this by "downloading" it from the local computer with an http request.
   * The MIME Type is overwritten to image/plain, so the file will be received just
   * as text.
   * Then, see function base64Encode.
   * @param blob
   */
  blobToBinaryStringIE11(blob) {
    let blobURL = URL.createObjectURL(blob);
    let xhr = new XMLHttpRequest;
    xhr.open("get", blobURL);
    xhr.overrideMimeType("image/plain; charset=x-user-defined");
    xhr.onload = () => {
      let binary = xhr.response;
      // convert
      this.base64textString = this.base64Encode(binary);
      // tell parent about conversion
      this.imageConverted.emit(this.base64textString);

    };
    xhr.send();
  }

  /**
   * Triggered by the FileReader from {@link onSelectedFileChanged}. Does the actual bas64 conversion and writes the
   * result in the {@link base64textString} variable and fires the imageConverted EventEmitter.
   * @param readerEvt
   * @private
   */
  _handleReaderLoaded(readerEvt) {
    let binaryString;
    if (!readerEvt) {
      binaryString = (this.reader as any).content;
    } else {
      binaryString = readerEvt.target.result;
    }
    this.base64textString = btoa(binaryString);
    this.imageConverted.emit(this.base64textString);
  }
}
