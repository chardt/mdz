import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import {DREAMFACTORY_API_KEY} from '../../config/constants';
import {SessionService} from '../../user/session/services/session.service';
import {User} from '../../user/model/user.model';
import {UserProfile} from '../../user/userprofile.model';
import {BaseHttpService} from '../services/BaseHttp.service';
import {Http, RequestOptions} from '@angular/http';
import {Md5} from 'ts-md5/dist/md5';
import {TranstoastService} from '../transtoast.service';

@Injectable()
export class ProfileImageService extends BaseHttpService {

  /**
   * Holds the current profile image URL
   * @type {BehaviorSubject<string>}
   */
  private url: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  /**
   * API for the profile image as an observable for other modules to observe.
   * @type {"../../Observable".Observable<T>}
   */
  public url$: Observable<string> = this.url.asObservable();

  /**
   * The current user.
   * @type {any}
   */
  private currentUser: User = null;

  private cacheKiller: string;

  /**
   * Subscribes to the User from the {@link sessionService} and binds it to the local user.
   * Also subscribes to the UserProfile and handles the profile image from that.
   * @param http
   * @param toastr
   * @param sessionService
   */
  constructor(protected http: Http,
              protected toastr: TranstoastService,
              private sessionService: SessionService) {
    super(http, toastr);

    this.cacheKiller = '' + Date.now();

    sessionService.user$.subscribe(
      (user: User) => {
        this.currentUser = user;
      }
    );

    sessionService.profile$.subscribe(
      (profile: UserProfile) => {
        if (profile) {

          // test if avatar is present. If so, use the avatar image url.
          let url = profile.getProfileImageUrl();

          if (profile.getAvatarId()) {
            url = profile.getAvatarId();
          }

          this.setProfileImageUrl(url);
        } else {
          this.setProfileImageUrl(null);
        }
      }
    );

  }

  /**
   * Setter for the user profile image.
   * Takes a API relative URL and composes an absolute URL to fetch the actual image.
   * If the url was null, the path is not composed and NULL is put as the next image url.
   * @param url
   */
  setProfileImageUrl(url) {

    if (url) {
      // set a new cache killer
      this.cacheKiller = Date.now() + '';
      const fullPath = this.composePath(url);
      this.url.next(fullPath);
    } else {
      this.url.next(null);
    }
  }

  /**
   * POSTs a profile image to the API
   *
   * @param uploadPackage
   * @param url
   * @returns {Observable<Response>}
   */
  public postImage(uploadPackage, url?: string): Observable<any> {

    const httpOptions: RequestOptions = this.getDefaultHttpOptions();

    if (!url) {
      url = this.getProfileImageUrl();
    }

    return this.http.post(url, uploadPackage, httpOptions);
  }


  /**
   * Takes an API relative path and composes an absolute URL.
   * The new URL will have the token and API key as parameters.
   *
   * @param imagePath
   * @returns {string}
   */
  public composePath(imagePath): string {

    let url = this.sessionService.endpoint('/files/');
    const token = this.sessionService.getToken();
    url += imagePath + '?ts_=' + this.cacheKiller + '&api_key=' + DREAMFACTORY_API_KEY + '&session_token=' + token;

    return url;
  }

  /**
   * Takes a FileInfo object to extract the file extension from.
   * It hashes the user ID and creates a filename for the profile image.
   *
   * @param fileInfo
   * @returns {string}
   */
  public getHashForProfileImage(fileInfo): string {

    const hashBase = this.sessionService.getCurrentUser().id.toString();
    let hash = Md5.hashStr(hashBase);

    const fileExtensionArr = fileInfo.type.split('/');
    const fileExtension = fileExtensionArr[fileExtensionArr.length - 1];

    hash += ('.' + fileExtension);

    return hash + '';
  }

  /**
   * Returns an absolute Url to the profile images endpoint
   * @returns {string}
   */
  public getProfileImageUrl(): string {
    return this.endpoint('/files/profiles/images/');
  }
}
