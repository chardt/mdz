import {Injectable} from '@angular/core';
import {BaseHttpService} from '../services/BaseHttp.service';
import {ToastsManager} from 'ng2-toastr';
import {Http} from '@angular/http';
import {TranstoastService} from '../transtoast.service';

@Injectable()
export class ImageuploadService extends BaseHttpService {

  constructor(protected toastr: TranstoastService,
              protected http: Http) {
    super(http, toastr);
  }

}
