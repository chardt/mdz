import {Component, Input, Output, EventEmitter} from '@angular/core';
import {ProfileImageService} from '../profile-image/profile-image.service';
import {ToastsManager} from 'ng2-toastr';
import {ImageuploadService} from './imageupload.service';
import {TranstoastService} from '../transtoast.service';
import {Deferred} from '../Deferred';
import {Md5} from "ts-md5/dist/md5";

@Component({
  selector: 'image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.scss'],
  providers: [ImageuploadService]
})
export class ImageUploadComponent {

  /**
   * This is going to be the name of the file when it will be uploaded.
   */
  @Input() fileName: string;

  /**
   * The URL where the image should be uploaded.
   */
  @Input() uploadTarget: string;

  /**
   * Optional, A message that is presented to the user after a successful upload.
   */
  @Input() successMessage: string;

  /**
   * URL to the image to display. Can be a URI or base64 data URL.
   */
  @Input() imgUrl: string;

  /**
   * Original File type - needed to construct the correct URL to delete an old image from the API.
   */
  @Input() originalFileType: string;

  /**
   * Optional, Unique identifier for multiple image uploads, that are used in parallel.
   */
  @Input() desgination: string;

  /**
   * Flag that leads to the upload button to be visible or hidden.
   * Defaults to false. Add this as @ViewChild('viewChildName') in the parent and trigger
   * the upload by calling viewChildName.uploadImage();
   *
   * @type {boolean}
   */
  @Input() autoUpload = false;

  @Input() isProfileImageUploader = false;

  /**
   * Is fired when the upload has been successfull.
   * @type {EventEmitter}
   */
  @Output() uploadDone: EventEmitter<any> = new EventEmitter();

  /**
   * The upload is somehow triggered twice.
   * This is a brake to make sure the component attempts the upload oly once.
   * @type {boolean}
   * */
  private uploaded = false;

  /**
   * Is fired when a new image has been selected.
   * @type {EventEmitter}
   */
  @Output() fileInfoChanged: EventEmitter<any> = new EventEmitter();

  /**
   * Flag to manually set the precense of the file.
   */
  private filePresenceOverride = false;

  /**
   * To track when the file info was changed once. Parent needs to know only if it has changed, not how often.
   * @type {boolean}
   */
  private fileInfoChangedBefore = false;

  /**
   * Holds the selected image in base64 representation.
   */
  private base64Image: string;

  /**
   * Holds the information of the first file of the input element.
   */
  private fileInfo;

  /**
   * A file reader to be used to read files from the input
   */
  private fileReader: FileReader;

  constructor(private profileImageService: ProfileImageService,
              private toastr: TranstoastService,
              private imageUploadService: ImageuploadService) {

  }

  /**
   * Triggered when a (new) file was selected. Reads the input file as URL and writes the resulting
   * URL in imgUrl so it is available for preview.
   * @param $event
   */
  onFileInfoChanged($event) {
    let element;
    if ($event.srcElement) {
      element = $event.srcElement;
    } else {
      element = $event.target;
    }
    const fileInfo = element.files[0];

    this.fileInfo = fileInfo;

    this.fileReader = new FileReader();

    this.fileReader.addEventListener('load', () => {
      this.imgUrl = this.fileReader.result;
    });
    this.fileReader.readAsDataURL(fileInfo);

    if (!this.fileInfoChangedBefore) {
      this.fileInfoChangedBefore = true;
      this.fileInfoChanged.emit();
    }
  }

  /**
   * Triggered by the child base64input. The parameter is the image in base64 representation.
   * @param base64
   */
  onImageConverted(base64) {
    const filesize = base64.length * 0.75 / 1000000;
    if (filesize > 2) {
      this.toastr.error('Dieses Bild ist leider zu groß. Bitte wähle ein Bild, dass kleiner als 2MB groß ist.');
      this.fileInfo = null;
      this.base64Image = null;
      this.imgUrl = null;
      return;
    }
    this.base64Image = base64;
    if (this.autoUpload) {
      this.uploadImage();
    }
  }

  /**
   * Forms a file name from the fileName parameter and the original file extension.
   * If not fileName was given it creates an md5 hash from the file information and uses that as a file name.
   * The goal here is to create arbitrary file names.
   *
   * POSTs the image to the API, notifies the user with the successMessage and emits the uploadDone event.
   *
   * @param clear defines whether the existing image should be deleted before posting a new one.
   * @param imageID database ID of the image relation to be updated
   */
  uploadImage(clear?: boolean, imageID?: number) {
    const d = new Deferred();
    // abort if no file info is given (empty input element)
    if (!this.fileInfo) {
      d.resolve();
      return d.promise;
    }

    /**
     * The upload is somehow triggered twice.
     * This is a brake to make sure the component attempts the upload oly once.
     * */
    if(this.uploaded){
      d.resolve();
      return d.promise;
    }
    this.uploaded = true;

    // if clear is set, first remove the old image, then upload the new one.
    if (clear) {
      this.clearExistingImage(imageID).then(() => {
        this.actualUpload(d);
      });
      return d.promise;
    } else {
      // image does not need to be cleared, proceed with the upload.
      this.actualUpload(d);
      return d.promise;
    }
  }

  /**
   * The actual Upload method. this.uploadImage() prepares the upload and call actualUpload, when all is ready.
   * @param d the deferred promise to tell when the upload has finished.
   */
  private actualUpload(d: Deferred<any>) {
    const fileExtensionArr = this.fileInfo.type.split('/');
    const fileExtension = fileExtensionArr[fileExtensionArr.length - 1];
    let filename = this.fileInfo.name + '.' + fileExtension;

    // either use the given name or assume it's a profile pic. then get the hash for it.
    if (this.fileName) {
      filename = this.fileName + '.' + fileExtension;
    } else {
      if (!this.isProfileImageUploader) {
        filename = Md5.hashStr(filename) + '.' + fileExtension;
      } else {
        filename = this.profileImageService.getHashForProfileImage(this.fileInfo);
      }
    }

    const uploadPackage = {
      'resource': [
        {
          name: filename,
          type: 'file',
          is_base64: true,
          content: this.base64Image
        }
      ]
    };

    // when the url is null, postImage() assumes a profile picture.
    let url = null;
    if (this.uploadTarget) {
      url = this.imageUploadService.endpoint(this.uploadTarget);
    }

    this.profileImageService.postImage(uploadPackage, url).subscribe(
      response => {
        let message = 'Erfolg!';
        if (this.successMessage) {
          message = this.successMessage;
        }
        // this.toastr.success(message);
        this.uploadDone.emit(response);
        d.resolve();
      },
      errorResponse => {
        this.toastr.error(errorResponse.json().error.message);
        this.uploadDone.emit(errorResponse);
        d.resolve();
      }
    );
  }

  /**
   * Returns true if the file has been changed, false otherwise.
   * @returns {any}
   */
  public filePresent() {
    return ((this.fileInfo) || this.filePresenceOverride);
  }

  /**
   * Activates the file presence override, so the component assumes a file is already present.
   */
  public setFilePresent() {
    this.filePresenceOverride = true;
  }

  private clearExistingImage(imageId: number) {
    const d = new Deferred();
    // get the relation id of the initiative / image relation
    this.profileImageService.deleteEntity({id: imageId}, 'initiative_has_image').subscribe(
      () => {
        this.uploadImage().then(() => {
          d.resolve();
        });
      }
    );
    return d.promise;
  }
}
