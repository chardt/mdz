import {Injectable} from '@angular/core';
import {ToastsManager} from 'ng2-toastr';

@Injectable()
/**
 * This Service decorates the ToastsManager.
 * It takes the messages and tries to translating, using the internal dictionary.
 * It then calls the ToastsManager with the translated strings.
 * If there is not translation available, the original string is used.
 */
export class TranstoastService {

  /**
   * DE/EN translation Dictionary
   * @type {{}}
   */
  private dict = {
    'server message': 'deutsche übersetzung',
    'Unauthorized. User is not authenticated.': 'Du bist nicht authorisiert.',
    'Invalid credentials supplied.': 'Du hast ungültige Anmeldedaten angegeben.',
    'Login request is missing required email.': 'Bitte gib Deine E-Mail Adresse an.',
    'Login request is missing required password.': 'Bitte gib dein Passwort ein.',
    'Validation failed': 'Überprüfung fehlgeschlagen',
    'The email has already been taken.': 'Diese E-Mail Adresse ist bereits im System vorhanden.',
    'The username has already been taken.': 'Dieser Benutzername ist bereits vergeben.',
    'The supplied email was not found in the system.': 'Die angegebene E-Mail Adresse wurde nicht gefunden.',
    'The username must be at least 6 characters.': 'Der Benutzername muss mindestens 6 Zeichen lang sein.',
    'The username format is invalid.': 'Bitte verwende weder Leer- noch Sonderzeichen im Benutzernamen.',
    'email': 'E-Mail Adresse',
    'The supplied email and/or confirmation code were not found in the system.': 'Für dieses Konto wurde kein Vorgang' +
    ' gefunden. Wenn Du dein Passwort zurücksetzen möchtest, gehe zur Anmeldung und klicke auf "Passwort vergessen".',
    'Token has expired: Session expired. Please refresh your token (if still within refresh window) or re-login.': '' +
    'Deine Sitzung ist abgelaufen. Bitte melde dich erneut an.',
    'Invalid data supplied. The email must be a valid email address.': 'Die angegebene E-Mail Adresse ist ungütlig.',
    'Confirmation code expired.': 'Der Registrierungszeitraum ist abgelaufen. Bitte melde dich beim Support.',
    'Invalid token: Wrong number of segments':'Es liegt ein Problem mit dem Sicherheits Token vor. Bitte melde dich erneut an.',
    'Response with status 500 Internal Server Error for URL:https//marktplatz-sg.de:444/api/v2/mysql/_table/user_supports_initiative':'Es ist ein Fehler aufgetreten.',
    'The token has been blacklisted: Session terminated. Please re-login.':'Bitte melde dich erneut an.'
    // Bitte melden Sie sich erneut an.'
  };

  constructor(private toastr: ToastsManager) {
  }

  /**
   * Decorator for the success message.
   * @param message
   * @param title
   */
  public success(message: string, title?: string) {

    if (this.dict[message]) {
      message = this.dict[message];
    }
    if (this.dict[title]) {
      title = this.dict[title];
    }

    if (title) {
      this.toastr.success(message, title);
    } else {
      this.toastr.success(message);
    }
  }

  /**
   * Decorator for the info message.
   * @param message
   * @param title
   */
  public info(message: string, title?: string) {
    if (this.dict[message]) {
      message = this.dict[message];
    }
    if (this.dict[title]) {
      title = this.dict[title];
    }

    if (title) {
      this.toastr.info(message, title);
    } else {
      this.toastr.info(message);
    }
  }

  /**
   * Decorator for the error message.
   * @param message
   * @param title
   */
  public error(message: string, title?: string) {
    if (this.dict[message]) {
      message = this.dict[message];
    }
    if (this.dict[title]) {
      title = this.dict[title];
    }

    if (title) {
      this.toastr.error(message, title);
    } else {
      this.toastr.error(message);
    }
  }

  /**
   * Decorator for the warning message.
   * @param message
   * @param title
   */
  public warning(message: string, title?: string) {
    if (this.dict[message]) {
      message = this.dict[message];
    }
    if (this.dict[title]) {
      title = this.dict[title];
    }

    if (title) {
      this.toastr.warning(message, title);
    } else {
      this.toastr.warning(message);
    }
  }
}
