import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'noLink'})
export class NoLinkPipe implements PipeTransform {
  transform(value: string): string {
    if (!value) {
      return value;
    }
    return value.replace(/<a\b[^>]*>/i, '').replace(/<\/a>/i, '');
  }
}
