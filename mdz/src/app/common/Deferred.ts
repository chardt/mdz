export class Deferred<T> {

  /**
   * A promise that is being delayed
   */
  promise: Promise<T>;

  /**
   * Function that is mapped to the promises resolve action
   */
  resolve: (value?: T | PromiseLike<T>) => void;

  /**
   * Function that is mapped to the promises resolve action
   */
  reject: (reason?: any) => void;

  /**
   * Creates a new promise and maps the resolve/reject methods
   */
  constructor() {
    this.promise = new Promise<T>((resolve, reject) => {
      this.resolve = resolve;
      this.reject = reject;
    });
  }
}
