import {Component, Input} from '@angular/core';

@Component({
  selector: 'loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent {

  @Input() small = false;
  @Input() loading = false;
  @Input() message = '';

  constructor() {
  }
}
