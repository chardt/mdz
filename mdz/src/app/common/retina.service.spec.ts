import { TestBed, inject } from '@angular/core/testing';

import { RetinaService } from './retina.service';

describe('RetinaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RetinaService]
    });
  });

  it('should be created', inject([RetinaService], (service: RetinaService) => {
    expect(service).toBeTruthy();
  }));
});
