import { TestBed, inject } from '@angular/core/testing';

import { OtherUsersService } from './other-users.service';

describe('OtherUsersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OtherUsersService]
    });
  });

  it('should be created', inject([OtherUsersService], (service: OtherUsersService) => {
    expect(service).toBeTruthy();
  }));
});
