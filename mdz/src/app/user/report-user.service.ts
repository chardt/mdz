import {Injectable} from '@angular/core';
import {BaseHttpService} from '../common/services/BaseHttp.service';
import {ToastsManager} from 'ng2-toastr';
import {Http} from '@angular/http';
import {TranstoastService} from '../common/transtoast.service';

@Injectable()
export class ReportUserService extends BaseHttpService {

  constructor(http: Http,
              toastr: TranstoastService) {
    super(http, toastr);
  }
}
