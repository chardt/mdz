import {Component, Input, OnInit} from '@angular/core';
import {User} from '../model/user.model';
import {SessionService} from '../session/services/session.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-infobox',
  templateUrl: './infobox.component.html',
  styleUrls: ['./infobox.component.css']
})
export class InfoboxComponent implements OnInit {

  user: User = null;
  userSubscription: Subscription;

  constructor(private sessionService: SessionService) {

  }

  ngOnInit() {
    this.userSubscription = this.sessionService.user$.subscribe(
      (user: User) => {
        this.user = user;
      }
    );
  }
}
