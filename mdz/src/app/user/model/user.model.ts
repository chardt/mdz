import {Initiative} from '../../initiative/model/initiative.model';
export class User {

  public static entity = '/user/profile';

  public initiatives: Initiative[];

  public isInitiator: boolean;

  static fromJson(json: any) {
    const user = new User(
      // tslint:disable-next-line
      parseInt(json.id),
      json.email,
      json.first_name,
      json.last_name,
      json.username,
      json.last_login_date,
      json.role,
      // tslint:disable-next-line
      parseInt(json.role_id),
      // tslint:disable-next-line
      parseInt(json.support_id),
      json.is_active
    );

    return user;
  }

  constructor(public id: number,
              public email: string,
              public first_name: string,
              public last_name: string,
              public username: string,
              public last_login_date: string,
              public role: string,
              public role_id: number,
              public support_id: number,
              public is_active: boolean) {
  }

  public toJson(stringyfiy?: boolean) {
    const j = {
      id: this.id,
      email: this.email,
      first_name: this.first_name,
      last_name: this.last_name,
      username: this.username,
      last_login_date: this.last_login_date,
      role: this.role,
      role_id: this.role_id,
      support_id: this.support_id,
      is_active: this.is_active
    };

    if (!stringyfiy) {
      return j;
    }

    return JSON.stringify(j);
  }
}
