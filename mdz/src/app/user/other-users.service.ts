import {Injectable} from '@angular/core';
import {BaseHttpService} from '../common/services/BaseHttp.service';
import {Observable} from 'rxjs/Observable';
import {Http} from '@angular/http';
import {UserProfile} from './userprofile.model';
import {TranstoastService} from '../common/transtoast.service';

@Injectable()
export class OtherUsersService extends BaseHttpService {

  constructor(protected http: Http,
              protected toastr: TranstoastService) {
    super(http, toastr);
  }

  /**
   * GETs a User from the API by it's ID.
   *
   * @param id
   * @returns {Observable<Response>}
   */
  public getUserById(id: number): Observable<any> {
    const endpoint = this.endpoint('/system/user/' + id);
    return this.http.get(endpoint, this.getDefaultHttpOptions());
  }

  /**
   * GETs a User from the API by it's username.
   * @param username
   * @returns {Observable<Response>}
   */
  public getUserByUsername(username: string): Observable<any> {
    const options = {
      filter: {
        username: username
      },
      fields: ['id', 'last_name', 'first_name', 'username']
    };

    return this.getEntity('/system/user', options, true);
  }

  /**
   * GETs the UserProfile of a specific user by the User ID.
   *
   * @param userId
   * @returns {Observable<any>}
   */
  public getProfile(userId: number) {
    const options = {
      filter: {
        user_id: userId
      }
    };

    return this.getEntity(UserProfile.entity, options);
  }

  /**
   * Blocks or unblocks a User, depending on the active flag.
   * By default it blocks the user.
   *
   * @param user_id
   * @param active
   * @returns {Observable<any>}
   */
  public blockUser(user_id: number, active?: boolean) {
    if (!active) {
      active = false;
    }

    const systemcall = true;
    const patch = {
      id: user_id,
      is_active: active
    };
    return this.patchEntity(patch, '/system/user/' + user_id, systemcall);
  }
}
