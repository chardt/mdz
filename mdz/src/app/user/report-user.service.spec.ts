import { TestBed, inject } from '@angular/core/testing';

import { ReportUserService } from './report-user.service';

describe('ReportUserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReportUserService]
    });
  });

  it('should be created', inject([ReportUserService], (service: ReportUserService) => {
    expect(service).toBeTruthy();
  }));
});
