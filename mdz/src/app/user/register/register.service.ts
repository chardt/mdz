import {Injectable} from '@angular/core';
import {BaseHttpService} from '../../common/services/BaseHttp.service';
import {Observable} from 'rxjs/Observable';
import {Http} from '@angular/http';
import {TranstoastService} from '../../common/transtoast.service';

@Injectable()
export class RegisterService extends BaseHttpService {

  constructor(protected http: Http,
              protected toastr: TranstoastService) {
    super(http, toastr);
  }

  public register(user: any): Observable<any> {
    const options = this.getDefaultHttpOptions();
    const endpoint = this.endpoint('/user/register');
    const payload = JSON.stringify(user);
    return this.http.post(endpoint, payload, options);
  }
}
