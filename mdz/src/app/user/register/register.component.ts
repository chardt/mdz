import {Component} from '@angular/core';
import {StateService} from '@uirouter/angular';
import {SessionService} from '../session/services/session.service';
import {RegisterService} from './register.service';
import {TranstoastService} from '../../common/transtoast.service';

@Component({
  selector: 'app-register',
  templateUrl: 'register.component.html',
  styleUrls: ['register.component.scss'],
  providers: [RegisterService]
})
export class RegisterComponent {

  user: {
    username: string,
    first_name: string,
    last_name: string,
    email: string,
    password: string,
    password2: string,
    accepts_datenschutz: boolean,
    accepts_rules: boolean
    // accepts_agb: boolean
  };

  disable = false;

  constructor(private registerService: RegisterService,
              private stateService: StateService,
              private sessionService: SessionService,
              private toastr: TranstoastService) {
    this.user = {
      username: '',
      first_name: '',
      last_name: '',
      email: '',
      password: '',
      password2: '',
      accepts_datenschutz: false,
      accepts_rules: false
      // accepts_agb: false
    };
  }

  err(message: string) {
    this.toastr.error(message);
  }

  validate() {
    let aOk = true;
    let message = '';
    //
    // if (this.user.password !== this.user.password2) {
    //   message += '• Die Passwörter müssen ubereinstimmen\r';
    //   aOk = false;
    // }

    if (!this.user.accepts_rules) {
      message += '• Sie müssen die Spielregeln akzeptieren\r';
    }

    // if (!this.user.accepts_agb) {
    //   message += '• Sie müssen den AGB zustimmen\r';
    //   aOk = false;
    // }

    if (!this.user.accepts_datenschutz) {
      message += '• Sie müssen der Datenschutzvereinbarung zustimmen.\r';
      aOk = false;
    }

    if (this.user.username === '') {
      message += '• Bitte wählen Sie einen Benutzernamen\r';
      aOk = false;
    }

    if (this.user.email === '') {
      message += '• Bitte geben Sie eine E-Mail Adresse an.\r';
      aOk = false;
    }

    if (!aOk) {
      this.err(message);
    }
    return aOk;
  }

  register() {
    if (!this.validate()) {
      return;
    }

    this.disable = true;
    this.registerService.register(this.user).subscribe(
      (response) => {
        if (response.json().success) {
          this.toastr.success('Erfolg, bitte prüfe Dein Postfach.');

        }

        // let loginUser = {
        //   email: this.user.email,
        //   password: this.user.password
        // };
        // this.sessionService.login(loginUser).then(
        //   (response) => {
        //     this.stateService.go('profile');
        //   },
        //   (errorResponse) => {
        //     this.toastr.error(errorResponse.json().error.message);
        //   });
      },
      (errorResponse) => {
        this.disable = false;
        const e = errorResponse.json().error;
        this.toastr.error(e.message);
        if (e.context) {
          const keys = Object.keys(e.context);
          for (const key of keys) {
            this.toastr.error(e.context[key], key);
          }
        }
      }
    );
  }
}
