import {Component, OnInit, ViewChild} from '@angular/core';
import {SessionService} from '../session/services/session.service';
import {User} from '../model/user.model';
import {UserProfile} from '../userprofile.model';
import swal from 'sweetalert2';
import {ProfileImageService} from '../../common/profile-image/profile-image.service';
import {StateService} from '@uirouter/angular';
import {OtherUsersService} from '../other-users.service';
import {Comment} from '../../initiative/show/comments/comment/model/comment.model';
import {DEFAULT_USER_IMAGE_URL} from '../../config/constants';
import {Http} from '@angular/http';
import {TranstoastService} from '../../common/transtoast.service';
import {Deferred} from '../../common/Deferred';
import {AvatarService} from './avatar/avatar.service';
import {Avatar} from './avatar/avatar.model';
import {InitiativeService} from '../../initiative/services/initiative.service';
import {Initiative} from '../../initiative/model/initiative.model';
import {ContactComponent} from './contact/contact.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  providers: [InitiativeService]
})
export class ProfileComponent implements OnInit {

  /**
   * ViewChild Reference to the contact form.
   */
  @ViewChild('contact') contact: ContactComponent;

  /**
   * The current User.
   */
  private user: User;

  /**
   * Profile Data of that user.
   */
  private profile: UserProfile;

  /**
   * URI for the profile Image.
   */
  private profileImageUrl: string;

  /**
   * Show/hide the avatars.
   */
  private showAvatars = false;

  /**
   * Trigger to show/hide the edit UI
   * @type {boolean}
   */
  private edit = false;

  /**
   * When editing, we need to know what has been edited. This is the original email address.
   * @type {string}
   */
  private originalEmail = '';

  /**
   * When editing, we need to know what has been edited. This is the original first name.
   * @type {string}
   */
  private originalFirstName = '';

  /**
   * When editing, we need to know what has been edited. This is the original last name.
   * @type {string}
   */
  private originalLastName = '';

  /**
   * When editing, we need to know what has been edited. This is the original username.
   * @type {string}
   */
  private originalUsername = '';

  /**
   * Holds the username.
   * @type {string}
   */
  private username = '';

  private startedInitiatives: Initiative[] = [];

  private supportedInitiatives: Initiative[] = [];
  /**
   *
   * @type {boolean}
   */
  private foreignProfile = false;

  /**
   * The user has a few key points of interest that are tracked throughout the app. The are represented here.
   * @type {{started: number; supported: number; commented: number; shared: number}}
   */
  private kpis: any = {
    started: 0,
    supported: 0,
    commented: 0,
    shared: 0
  };

  constructor(private sessionService: SessionService,
              private profileImageService: ProfileImageService,
              private statesService: StateService,
              private otherUserService: OtherUsersService,
              private http: Http,
              private toastr: TranstoastService,
              private initiativeService: InitiativeService,
              private avatarService: AvatarService) {

  }

  /**
   * Adds this the the session/user subscribers. Will load the profile, as soon as a user is served.
   * Does the same with the users profile image.
   */
  ngOnInit() {

    // load other users profile
    if (this.statesService.params.username) {
      this.foreignProfile = true;
      this.loadOtherUserProfile(this.statesService.params.username);
      return;
    }

    // load my own profile
    this.loadCurrentUserProfile();
  }

  /**
   * Init routine for another users profile, that should be displayed.
   */
  private loadOtherUserProfile(username: string) {
    // get the users id
    this.otherUserService.getUserByUsername(username).subscribe(
      response => {
        const data = response.json().resource[0];
        this.user = User.fromJson(data);

        this.checkProfile();
        this.getKPIs(this.user.id);

        this.initiativeService.getMyInitiatives(false, this.user.id).then(
          observable => {
            observable.subscribe(
              initiativeIDsResponse => {
                for (const i of initiativeIDsResponse.json().resource) {
                  this.initiativeService.publishedInitiativesByID(i.initiative_id).subscribe(
                    initiativeResponse => {
                      const initiative = initiativeResponse.json().resource[0];
                      this.startedInitiatives.push(Initiative.fromJson(initiative));
                    }
                  );
                }
              }
            );
          }
        );

        this.initiativeService.getMyInitiatives(true, this.user.id).then(
          observable => {
            observable.subscribe(
              initiativeIDsResponse => {
                for (const i of initiativeIDsResponse.json().resource) {
                  this.initiativeService.publishedInitiativesByID(i.initiative_id).subscribe(
                    initiativeResponse => {
                      const initiative = initiativeResponse.json().resource[0];
                      this.supportedInitiatives.push(Initiative.fromJson(initiative));
                    }
                  );
                }
              }
            );
          }
        );

      }
    );
  }

  /**
   * Init routine for the profile when the users visits the own profile.
   */
  private loadCurrentUserProfile() {
    this.sessionService.user$.subscribe(
      (user: User) => {
        if (user) {
          this.user = user;

          this.originalEmail = this.user.email;
          this.originalLastName = this.user.last_name;
          this.originalFirstName = this.user.first_name;

          this.getMyselfAsOtherUser(user.id);
          this.checkProfile();
          this.getKPIs();
        }
      }
    );

    this.profileImageService.url$.subscribe(
      url => {
        if (!url) {
          url = DEFAULT_USER_IMAGE_URL;
        }
        this.profileImageUrl = url;
      }
    );
  }

  /**
   * GETs the userdata from system/users to supplement the User object with the username, which is not being returned by
   * the session call.
   * @param id
   */
  private getMyselfAsOtherUser(id: number) {
    this.otherUserService.getUserById(id).subscribe(
      response => {
        const j = response.json();
        this.originalUsername = j.username;
        this.user.username = j.username;
      }
    );
  }

  /**
   * Sets the Avatar as current profile image and saves the profile.
   *
   * @param avatar Avatar
   */
  private btnSelectAvatarPressed(avatar: Avatar) {
    this.profile.setAvatarId(avatar.getPath());
    this.profileImageService.setProfileImageUrl(avatar.getPath());
    this.saveProfile();
  }

  /**
   * Removes the avatar ID and restores the original image.
   */
  private btnRemoveAvatarPressed() {
    this.profile.setAvatarId('');
    this.profileImageService.setProfileImageUrl(this.profile.getProfileImageUrl());
    this.saveProfile();
  }

  /**
   * PATCHes the profile entity on the API.
   */
  private saveProfile(onlyNewsletter?: boolean) {
    let payload;
    if (onlyNewsletter) {
      payload = {
        id: this.profile.getId(),
        receives_newsletter: this.profile.receives_newsletter
      }
    } else {
      payload = this.profile.toJson();
    }
    this.profileImageService.patchEntity(payload, UserProfile.entity).subscribe(
      response => {
        console.log(response.json());
      },
      error => {
        console.log(error.json());
      }
    );
  }

  /**
   * GETs the profile image from the profile and gives that to the profile image service.
   */
  getProfileImageUrl() {
    let url = this.profile.getProfileImageUrl();
    if (this.profile.getAvatarId()) {
      url = this.profile.getAvatarId();
    }
    if (!this.foreignProfile) {
      this.profileImageService.setProfileImageUrl(url);
    }
  }

  /**
   * Lazily returns the full URL to the other users profile image.
   * @returns {string}
   */
  private getOtherProfileImageUrl() {
    if (this.profile) {
      if (!this.profileImageUrl) {
        const url = this.profileImageService.composePath(this.profile.getProfileImageUrl());
        this.profileImageUrl = url;
      }
      return this.profileImageUrl;
    }
  }

  /**
   * Resets the user data to whatever was previously returned from the API.
   * Used when the user cancels changes of the changes are invalid.
   */
  private restore() {
    this.user.first_name = this.originalFirstName;
    this.user.last_name = this.originalLastName;
    this.user.email = this.originalEmail;
    this.user.username = this.originalUsername;
  }

  /**
   * Eventlistener, when the user cancels the changes.
   */
  private btnCancelPressed() {
    this.edit = false;
    this.restore();
  }

  /**
   * Eventlistener, when the user wants to edit the profile.
   */
  private btnEditPressed() {
    this.edit = true;
  }

  /**
   * Eventlistener, when the user wants to save that changes to the profile.
   */
  private btnSavePressed() {

    this.checkIfEmailIsAcceptable().then(
      () => {
        this.checkIfUsernameIsAcceptable().then(
          () => {
            const endpoint = this.sessionService.endpoint(User.entity);
            const options = this.sessionService.getDefaultHttpOptions();

            this.http.post(endpoint, this.user.toJson(true), options).subscribe(
              response => {
                console.log(response.json());
                this.edit = false;
              },
              error => {
                this.toastr.error(error.json().error.message);
              }
            );
          }
          ,
          () => {
            this.toastr.error('Diesen Benutzernamen kannst Du leider nicht verwenden.');
          }
        );
      },
      () => {
        this.toastr.error('Diese E-Mail Adresse kannst Du leider nicht verwenden.');
      }
    );
  }

  /**
   * Eventlistener, when the user changed his mind on whether he want's to receive the newsletter, or not.
   * Takes whatever is in profile.receives_newsletter and PATCHes that to the API.
   */
  private onNewsletterChanged() {
    setTimeout(() => this.saveProfile(true), 400);
  }

  /**
   * Will test if the changed username is available for use in the system.
   * Promise will be resolved if the username can be used and rejected otherwise.
   * @returns {Promise<T>}
   */
  private checkIfUsernameIsAcceptable(): Promise<any> {
    const d = new Deferred();

    // cancel if username was left unedited
    if (this.user.username === this.originalUsername) {
      d.resolve();
      return d.promise;
    }

    // prepare test request
    const testOptions = {
      filter: {
        username: this.user.username
      },
      fields: ['id']
    };

    this.performTestRequest(testOptions, d);

    return d.promise;
  }


  /**
   * Will test if the changed email is available for use in the system.
   * Promise will be resolved if the address can be used and rejected otherwise.
   * @returns {Promise<T>}
   */
  private checkIfEmailIsAcceptable() {

    const d = new Deferred();

    // cancel check if e-mail was not edited.
    if (this.user.email === this.originalEmail) {
      d.resolve();
      return d.promise;
    }

    const testOptions = {
      filter: {
        email: this.user.email
      },
      fields: ['id']
    };

    this.performTestRequest(testOptions, d);

    return d.promise;
  }

  /**
   * Takes a config/options object, which defines what should be tested for.
   * Takes a deferred promise to reject/resolve, depending on the test result.
   *
   * Test ist done by calling the API and counting received objects. If 0 -> test is positive, if > 0 -> test is negavtive.
   * @param options
   * @param deferred
   */
  private performTestRequest(options: any, deferred: Deferred<any>) {
    const systemCall = true;

    this.sessionService.getEntity('/system/user', options, systemCall).subscribe(
      response => {
        const resource = response.json().resource;
        if (resource.length > 0) {
          deferred.reject();
        } else {
          deferred.resolve();
        }
      },
      error => {
        console.log(error.json());
        deferred.reject();
      }
    );
  }

  /**
   * Check if the user already has a profile. If not, create one.
   * */
  checkProfile() {

    const profile = new UserProfile(this.user.id);

    const payload = {
      filter: {
        user_id: this.user.id
      }
    };

    // load the profile
    this.sessionService.getEntity(UserProfile.entity, payload).subscribe(
      response => {

        const resource = response.json().resource;

        if (resource.length === 0) {

          // there is no profile, create it
          this.sessionService.postEntity(profile.toJson(), UserProfile.entity).subscribe(
            postResponse => {
              this.profile = UserProfile.fromJson(postResponse.json().resource[0]);
            },
            postErrorResponse => {
              console.log(postErrorResponse.json());
            }
          );
        } else {
          // there IS a profile, use it
          this.profile = UserProfile.fromJson(resource[0]);

          // load the url of the profile image
          this.getProfileImageUrl();
        }
      },
      errorResponse => {
        console.log(errorResponse.json());
      }
    );
  }

  /**
   * GETs the tracked KPIs from the API.
   */
  getKPIs(userId?: number) {
    // started:
    // http://hardtmedia.de:81/api/v2/mysql/_table/user_owns_initiative?filter=user_id%3D2&count_only=true
    const options = {
      filter: {
        user_id: (userId) ? userId : this.sessionService.getCurrentUser().id
      },
      countOnly: true
    };
    this.sessionService.getEntity('user_owns_initiative', options).subscribe(
      response => {
        const amount = response.json();
        this.kpis.started = amount;
      }
    );

    // supports:
    // http://hardtmedia.de:81/api/v2/mysql/_table/user_supports_initiative?filter=user_id%3D2&count_only=true
    this.sessionService.getEntity('user_supports_initiative', options).subscribe(
      response => {
        const amount = response.json();
        this.kpis.supported = amount;
      }
    );
    // commented:
    // http://hardtmedia.de:81/api/v2/mysql/_table/comment?filter=user_id%3D232&count_only=true
    this.sessionService.getEntity(Comment.entity, options).subscribe(
      response => {
        const amount = response.json();
        this.kpis.commented = amount;
      }
    );
    // shared ... schema to do
  }

  /**
   * User clicks "change password" button
   */
  onChangePasswordPressed() {
    swal({
      title: 'Passwort ändern',
      text: 'Bitte gib dein aktuelles und dein neues Passwort ein',
      html: '<label for="current-pw"><span>Aktuelles Passwort:</span>' +
      '<input #currentPw id="current-pw" type="password" class="swal2-input"></label>' +
      '<label for="new-pw"><span>Neues Passwort:</span>' +
      '<input id="new-pw" type="password" class="swal2-input"></label>' +
      '<label for="new-pw2"><span>Wiederholen:</span><input id="new-pw2" type="password" class="swal2-input"></label>',
      preConfirm: () => {

        let message = '';

        // get the password values
        let currentPw: string = (<HTMLInputElement>document.getElementById('current-pw')).value;
        let newPw: string = (<HTMLInputElement>document.getElementById('new-pw')).value;
        let newPw2: string = (<HTMLInputElement>document.getElementById('new-pw2')).value;

        // make empty string to null so it's easier to validate
        if (currentPw === '') {
          currentPw = null;
        }
        if (newPw === '') {
          newPw = null;
        }
        if (newPw2 === '') {
          newPw2 = null;
        }


        if ((currentPw !== null && newPw !== null && newPw2 !== null) && (newPw === newPw2)) {

          const payload = {current: currentPw, newPw: newPw, newPw2: newPw2};
          return Promise.resolve(payload);
        } else {
          if (!currentPw) {
            message = 'Bitte gib dein aktuelles Passwort an.';
          }

          if (newPw === null) {
            message = 'Bitte gib ein neues Passwort ein.';
          }

          if (newPw !== newPw2) {
            message = 'Die Passwörter stimmen nicht überein.';
          }

          return Promise.reject(message);
        }
      },
      showCancelButton: true,
      cancelButtonText: 'Abbrechen',
      confirmButtonText: 'Ändern'
    }).then(
      resolved => {
        this.sessionService.changePassword(
          resolved.current,
          resolved.newPw
        );
      },
      rejected => {
        console.log(rejected);
      }
    );
  }

  /**
   * Eventlistener for when the user clicks the "close account" button
   */
  onCloseAccountPressed() {
    swal({
      title: 'Konto deaktivieren',
      text: 'Möchtest du dein Konto wirklich deaktivieren? Danach wirst du keinen Zugriff mehr auf das Portal und deine Daten haben.',
      showCancelButton: true,
      cancelButtonText: 'Nein, ich möchte bleiben.',
      confirmButtonText: 'Ich möchte mein Konto deaktiveren',
      type: 'question'
    }).then(
      () => {
        swal({
          title: 'Auf wiedersehen!',
          text: 'Dein Konto wird nun deaktiviert. Du bist jederzeit herzlich willkommen.',
          type: 'success'
        }).then(
          () => {

            this.otherUserService.blockUser(this.sessionService.getCurrentUser().id).subscribe(
              () => {
                this.sessionService.localLogout();
                this.statesService.go('landing-page');
              }
            );
          }
        );
      },
      () => {
        swal({
          title: 'Schön das du bleibst!',
          text: 'Wenn du Verbesserungsvorschläge oder Fragen hast, kannst du dich gerne an unseren Support wenden.',
          type: 'success'
        });
      }
    );
  }

  /**
   * Puts the image url into the users profile.
   * @param response
   */
  setImageInProfile(response) {
    const resource = response.json().resource;
    const path = resource[0].path;
    if (path) {

      this.profile.setProfileImageUrl(path);
      const systemCall = false;
      this.sessionService.patchEntity(this.profile.toJson(), UserProfile.entity, systemCall, 'user_id').subscribe(
        () => {
          this.btnRemoveAvatarPressed();
          this.profileImageService.setProfileImageUrl(path);
        },
        errorResponse => {
          console.log('erred at patch', errorResponse.json());
        }
      );
    }
  }

  private contactUser() {
    this.contact.popUp();
  }
}
