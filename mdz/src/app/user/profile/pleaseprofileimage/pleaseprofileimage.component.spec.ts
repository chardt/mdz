import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PleaseprofileimageComponent } from './pleaseprofileimage.component';

describe('PleaseprofileimageComponent', () => {
  let component: PleaseprofileimageComponent;
  let fixture: ComponentFixture<PleaseprofileimageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PleaseprofileimageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PleaseprofileimageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
