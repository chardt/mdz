import {Component, OnInit} from '@angular/core';
import {AvatarService} from "../avatar/avatar.service";
import {Avatar} from "../avatar/avatar.model";
import {StateService} from '@uirouter/angular';
import {UserProfile} from "../../userprofile.model";
import {ProfileImageService} from "../../../common/profile-image/profile-image.service";
import {User} from "../../model/user.model";
import {SessionService} from "../../session/services/session.service";
import {DREAMFACTORY_INSTANCE_URL, DREAMFACTORY_API_KEY} from "../../../config/constants";
import swal from 'sweetalert2';

@Component({
  selector: 'pleaseprofileimage',
  templateUrl: './pleaseprofileimage.component.html',
  styleUrls: ['./pleaseprofileimage.component.scss']
})
export class PleaseprofileimageComponent implements OnInit {

  /**
   * Profile Data of that user.
   */
  private profile: UserProfile = null;

  private profileImageUrl: string = null;

  constructor(private avatarService: AvatarService,
              private stateService: StateService,
              private profileImageService: ProfileImageService,
              private sessionService: SessionService) {
  }

  ngOnInit() {
    this.checkProfile();
  }

  private okEnter() {
    setTimeout(()=> {
      swal({
        title: 'Du siehst gut aus!',
        text: 'Bist du mit dem Bild zufrieden?',
        showCancelButton: true,
        cancelButtonText: 'Nein',
        confirmButtonText: 'Ja',
        // toast: true,
        position: 'top',
        type: 'success'
      }).then(() => {
        this.stateService.go('my-initiatives');
      });
    }, 1000);

  }

  /**
   * Puts the image url into the users profile.
   * @param response
   */
  setImageInProfile(response) {
    const resource = response.json().resource;
    const path = resource[0].path;
    if (path) {

      this.profile.setProfileImageUrl(path);

      // set the user id for the profile.
      this.profile.setUserId(this.sessionService.getCurrentUser().id);


      const systemCall = false;
      this.sessionService.patchEntity(this.profile.toJson(), UserProfile.entity, systemCall, 'user_id').subscribe(
        () => {
          this.btnRemoveAvatarPressed();
          this.profileImageService.setProfileImageUrl(path);
        },
        () => {
          // console.log('erred at patch', errorResponse.json());
        }
      );
    }
  }

  /**
   * Removes the avatar ID and restores the original image.
   */
  private btnRemoveAvatarPressed() {
    this.profile.setAvatarId('');
    this.profileImageService.setProfileImageUrl(this.profile.getProfileImageUrl());
    this.saveProfile();
  }

  private btnSelectAvatarPressed(avatar: Avatar) {
    this.profile.setAvatarId(avatar.getPath());
    const token = this.sessionService.getToken();
    const quickpath =
      DREAMFACTORY_INSTANCE_URL + '/api/v2/files/' +
      avatar.getPath() + '?ts_=1513288432592' +
      '&api_key=' + DREAMFACTORY_API_KEY + '&session_token=' + token;

    this.profileImageUrl = quickpath;

    this.profileImageService.setProfileImageUrl(avatar.getPath());
    this.saveProfile();
  }

  /**
   * PATCHes the profile entity on the API.
   */
  private saveProfile() {
    this.profileImageService.patchEntity(this.profile.toJson(), UserProfile.entity).subscribe(
      response => {
        this.okEnter();
      },
      error => {
        console.log(error.json());
      }
    );
  }

  /**
   * Check if the user already has a profile. If not, create one.
   * */
  checkProfile() {
    this.sessionService.profile$.subscribe(
      profile => {
        this.profile = profile;
      }
    );
  }
}
