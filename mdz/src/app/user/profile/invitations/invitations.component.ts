import {Component, OnInit, Input} from '@angular/core';
import {User} from '../../model/user.model';
import {InvitationsService} from './invitations.service';
import {Invitation} from './invitation.model';

@Component({
  selector: 'invitations',
  templateUrl: './invitations.component.html',
  styleUrls: ['./invitations.component.scss'],
  providers: [InvitationsService]
})
export class InvitationsComponent implements OnInit {

  @Input() user: User;

  private invitations: Invitation[] = null;

  constructor(private invitationsService: InvitationsService) {
  }

  ngOnInit() {
    this.invitationsService.getOpenInvitations(this.user.email).subscribe(
      response => {
        this.invitations = Invitation.fromResponse(response);
      }
    );
  }

}
