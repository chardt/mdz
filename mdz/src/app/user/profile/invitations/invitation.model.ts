/**
 * Created by hardtmedia on 13.12.17.
 */
export class Invitation {

  public static entity = 'invitation';

  public static fromJson(json: any) {
    const i = new Invitation(
      json.id,
      json.initiative_id,
      json.initiative_name,
      json.email,
      json.message,
      json.open
    );
    return i;
  }

  public static fromResponse(response: any) {
    const i: Invitation[] = [];
    const json = response.json();
    if (json) {
      const resource = json.resource;
      if (resource) {
        for (const inv of resource) {
          i.push(Invitation.fromJson(inv));
        }
      }
    }
    return i;
  }

  constructor(private id: number,
              private initiative_id: number,
              private initiative_name: string,
              private email: string,
              private message: string,
              private open: boolean) {

  }

  public close() {
    this.open = false;
  }

  public getInitiativeId() {
    return this.initiative_id;
  }

  public getId() {
    return this.id;
  }

  public toJson(stringify?: boolean) {
    const json = {
      id: this.id,
      initiative_id: this.initiative_id,
      initiative_name: this.initiative_name,
      email: this.email,
      message: this.message,
      open: this.open
    };
    return (stringify) ? JSON.stringify(json) : json;
  }
}
