import {Injectable} from '@angular/core';
import {BaseHttpService} from '../../../common/services/BaseHttp.service';
import {TranstoastService} from '../../../common/transtoast.service';
import {Http} from '@angular/http';
import {Invitation} from './invitation.model';

@Injectable()
export class InvitationsService extends BaseHttpService {

  constructor(protected toastr: TranstoastService,
              protected http: Http) {
    super(http, toastr);
  }

  public getOpenInvitations(email: string) {
    const options = {
      filter: {
        email: email,
        open: true
      }
    };
    return this.getEntity(Invitation.entity, options);
  }

  public closeInvitation(id: number) {
    const patch = {
      open: false
    };
    const ep = Invitation.entity + '/' + id;
    return this.patchEntity(patch, ep);
  }
}
