import {Component, Input} from '@angular/core';
import {Invitation} from '../invitation.model';
import {SupporterService} from '../../../../initiative/services/supporter.service';
import {NotificationSettingsService} from '../../../../initiative/show/sidebar/notification-settings/notification-settings.service';
import {TranstoastService} from '../../../../common/transtoast.service';
import {InvitationsService} from '../invitations.service';
import {StateService} from '@uirouter/angular';

@Component({
  selector: 'invitation',
  templateUrl: './invitation.component.html',
  styleUrls: ['./invitation.component.scss'],
  providers: [SupporterService, NotificationSettingsService]
})
export class InvitationComponent {

  @Input() invitation: Invitation;

  constructor(private supporterService: SupporterService,
              private notificationSettingsService: NotificationSettingsService,
              private invitationsService: InvitationsService,
              private state: StateService,
              private toastr: TranstoastService) {
  }

  /**
   * Navigates to the intitaive, the user has been invited to.
   */
  goto() {
    this.state.go('show', {initiativeId: this.invitation.getInitiativeId()});
  }

  /**
   * Lets the current User declare support for the Initiative.
   */
  btnSupportPressed() {

    // start support
    this.supporterService.support(this.invitation.getInitiativeId()).then(
      () => {

        // if supprt possible, set up notifications
        this.notificationSettingsService.prime(this.invitation.getInitiativeId());
        this.notificationSettingsService.createInitialSettings();

        // close the invitation
        this.closeInvitation();
      },
      rejected => {

        // support not possible, give notice and close invitation
        this.toastr.info(rejected);
        // this.closeInvitation();
      }
    );
  }

  /**
   * Closes the invitation on the API.
   */
  private closeInvitation() {
    // close invitation on server
    this.invitationsService.closeInvitation(this.invitation.getId()).subscribe(
      () => {
        // close it in frontend
        this.invitation.close();
      }
    );
  }
}
