import {Component, Input, ViewEncapsulation} from '@angular/core';
import {Avatar} from './avatar.model';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'avatar',
  // encapsulation: ViewEncapsulation.Native, // <-- kills firefox
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss']
})
export class AvatarComponent {

  /**
   * The avatar to display.
   */
  @Input() avatar: Avatar;

  /**
   * Turns off (visual) clickability and makes it display big.
   */
  @Input() big: boolean;

  constructor(private domSanitizer: DomSanitizer) {
  }

  /**
   * Bypasses CSS security.
   *
   * @returns {SafeStyle}
   */
  private path() {
    return this.domSanitizer.bypassSecurityTrustStyle('url(' + this.avatar.getFullPath() + ')');
  }
}
