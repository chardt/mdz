import {DREAMFACTORY_INSTANCE_URL, DREAMFACTORY_API_KEY} from '../../../config/constants';
export class Avatar {

  private fullPath: string = null;

  public static fromJson(json: any) {
    return new Avatar(json.path);
  }

  public static fromResponse(json: any) {
    if (json.resource) {
      const avatars: Avatar[] = [];
      for (const entry of json.resource) {
        avatars.push(new Avatar(entry.path));
      }

      return avatars;
    }
  }

  constructor(private path: string) {
    this.composePath();
  }

  public getPath() {
    return this.path;
  }

  public getFullPath() {
    return this.fullPath;
  }

  private composePath() {
    const path = DREAMFACTORY_INSTANCE_URL + '/api/v2/files/' + this.path + '?api_key=' + DREAMFACTORY_API_KEY;
    this.fullPath = path;
  }
}
