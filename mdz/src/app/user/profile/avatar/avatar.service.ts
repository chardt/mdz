import {Injectable} from '@angular/core';
import {SessionService} from '../../session/services/session.service';
import {BaseHttpService} from '../../../common/services/BaseHttp.service';
import {TranstoastService} from '../../../common/transtoast.service';
import {Http} from '@angular/http';
import {UserProfile} from '../../userprofile.model';
import {Avatar} from './avatar.model';

@Injectable()
export class AvatarService extends BaseHttpService {

  /**
   * Current User Profile
   * @type UserProfile
   */
  private profile: UserProfile = null;

  private avatars: Avatar[] = null;

  constructor(private sessionService: SessionService,
              protected http: Http,
              protected toastr: TranstoastService) {
    super(http, toastr);
    this.sessionService.profile$.subscribe(
      profile => {
        if (profile) {
          this.profile = profile;
        }
      }
    );

    this.fetchAvatars();
  }

  public selectAvatar(avatarId: string) {
    this.profile.setAvatarId(avatarId);
  }

  public getAvatar() {
    return this.profile.getAvatarId();
  }

  public fetchAvatars() {
    // GET avatar
    const endpoint = this.endpoint('/files/avatar/');
    // files/avatar
    this.http.get(endpoint, this.getDefaultHttpOptions()).subscribe(
      response => {
        this.avatars = Avatar.fromResponse(response.json());
      }
    );
  }
}
