import {Component, Input} from '@angular/core';
import {User} from '../../model/user.model';
import {ContactService} from './contact.service';
import swal from 'sweetalert2';
import {TranstoastService} from "../../../common/transtoast.service";

@Component({
  selector: 'contact-user',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  providers: [ContactService]
})
export class ContactComponent {

  @Input() user: User = null;

  private show = false;

  private message: string = null;

  constructor(private contactService: ContactService,
              private toastr: TranstoastService) {
  }

  public popUp() {
    this.show = true;
  }

  private hide() {
    this.show = false;
  }

  private send() {
    const payload = {
      receiving_user: this.user.username,
      message: this.message
    };

    this.contactService.sendMessage(payload).subscribe(
      response => {
        const data = response.json();
        if (data.count === 1) {
          swal({
            type: 'success',
            title: 'Vielen Dank!',
            text: 'Deine Nachricht wurde versandt.'
          }).then(
            () => {
              this.hide();
            }
          );
        } else {
          this.toastr.warning('Ups, da ist leider etwas schief gelaufen.');
        }
      }
    );

  }
}
