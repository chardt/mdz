import {Injectable} from '@angular/core';
import {BaseHttpService} from '../../../common/services/BaseHttp.service';
import {TranstoastService} from '../../../common/transtoast.service';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ContactService extends BaseHttpService {

  constructor(protected http: Http,
              protected toastr: TranstoastService) {
    super(http, toastr);
  };

  public sendMessage(config): Observable<any> {
    return this.postEntity(config, 'contact', true);
  }
}
