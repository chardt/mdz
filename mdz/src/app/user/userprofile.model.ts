export class UserProfile {

  /**
   * API Endpoint.
   * @type {string}
   */
  public static entity = 'profile';

  /**
   * URI to the users profile image.
   */
  private profileImageUrl: string;

  /**
   * Name of the avatar to use, if not null.
   */
  private avatarId: string = null;

  /**
   * Whether the user receives the newsletter, or not.
   */
  public receives_newsletter = false;

  /**
   * Factory, that creates a new UserProfile from a JSON object.
   * @param json
   * @returns {UserProfile}
   */
  public static fromJson(json: any) {
    const up = new UserProfile(json.user_id);
    if (json.image) {
      up.setProfileImageUrl(json.image);
    }
    if (json.id) {
      up.id = json.id;
    }
    if (json.avatar_id) {
      up.avatarId = json.avatar_id;
    }
    if(json.receives_newsletter){
      up.receives_newsletter = json.receives_newsletter;
    }
    return up;
  }

  constructor(private user_id: number,
              private id?: number) {

  }

  /**
   * Setter for the users user id.
   * @param id
   */
  public setUserId(id: number){
    this.user_id = id;
  }
  /**
   * Setter for the avatar image id.
   * @param id
   */
  public setAvatarId(id: string) {
    this.avatarId = id;
  }

  /**
   * Getter for the avatar image id.
   */
  public getAvatarId() {
    return this.avatarId;
  }

  /**
   * Setter for the profile image URL.
   * @param url
   */
  public setProfileImageUrl(url: string) {
    this.profileImageUrl = url;
  }

  /**
   * Getter for the profile image URL.
   * @returns {string}
   */
  public getProfileImageUrl() {
    return this.profileImageUrl;
  }

  /**
   * Getter for the ID.
   * @returns {number}
   */
  public getId() {
    return this.id;
  }

  /**
   * Returns a JSON object, representing the UserProfiles data.
   * @returns {{}} | string
   */
  public toJson(stringify?: boolean) {

    let json = {};

    if (this.user_id) {
      json['user_id'] = this.user_id;
    }
    if (this.profileImageUrl) {
      json['image'] = this.profileImageUrl;
    }
    if (this.id) {
      json['id'] = this.id;
    }
    if (this.avatarId !== undefined) {
      json['avatar_id'] = this.avatarId;
    }

    // always
    json['receives_newsletter'] = this.receives_newsletter;

    if (stringify) {
      json = JSON.stringify(json);
    }

    return json;
  }
}
