import {Component, OnInit} from '@angular/core';
import {SessionService} from '../services/session.service';
import {StateService} from '@uirouter/angular';
import {TranstoastService} from '../../../common/transtoast.service';
import {UserProfile} from "../../userprofile.model";


@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.scss']
})
export class LoginComponent implements OnInit {

  /**
   * User Object Template for the HTTP Request payload.
   */
  user: {
    email: string,
    password: string,
    remember_me: boolean
  };

  constructor(private sessionService: SessionService,
              private stateService: StateService,
              private toastr: TranstoastService) {

    this.user = {
      email: '',
      password: '',
      remember_me: true
    };
  }

  /**
   * Redirects the user to the logout page when he is already logged in and requests the login page.
   */
  ngOnInit() {
    if (this.sessionService.getCurrentLoginStatus() === true) { // as in "logged in"
      this.stateService.go('logout');
    }
  }

  /**
   * Requests the login on the SessionService.
   * When the login was successfull, admins are redirected to the landing page config,
   * normal users are redirected to 'my initiatives'.
   */
  login() {
    this.sessionService.login(this.user).then(
      (user) => {
        if (user.getValue().role === 'mdz-admin') {
          this.stateService.go('admin-landingpage');
        } else {
          this.sessionService.profile$.subscribe(
            (profile: UserProfile) => {
              if (profile) {
                if (!profile.getProfileImageUrl() && !profile.getAvatarId()
                ) {
                  this.stateService.go('forceprofileimage');
                } else {
                  this.stateService.go('my-initiatives');
                }
              }
            }
          );
        }

      },
      (response) => {
        this.toastr.error(response.json().error.message);
      });
  }

  btnRegisterPressed() {
    this.stateService.go('register');
  }
}
