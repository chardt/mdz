import {Component, Inject} from '@angular/core';
import {SessionService} from '../services/session.service';
import {StateService} from '@uirouter/angular';
import {ToastsManager} from 'ng2-toastr';
import {TranstoastService} from '../../../common/transtoast.service';

@Component({
  selector: 'app-logout',
  templateUrl: 'logout.component.html',
  styleUrls: ['logout.component.css']
})
export class LogoutComponent {

  constructor(@Inject(SessionService) private sessionService: SessionService,
              private stateService: StateService,
              private toastr: TranstoastService) {

  }

  /**
   * Requests the logout on the SessionService and redirects to login.
   */
  logout() {
    this.sessionService.logout().then((success) => {
      if (success) {
        this.stateService.go('login');
      }
    }, (response) => {
      this.toastr.error(response.json().error.message);
    });
  }
}
