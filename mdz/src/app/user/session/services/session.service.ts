import {Injectable} from '@angular/core';
import * as constants from '../../../config/constants';
import {User} from '../../../user/model/user.model';
import {Deferred} from '../../../common/Deferred';
import 'rxjs/add/operator/map';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {BaseHttpService} from '../../../common/services/BaseHttp.service';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {UserProfile} from '../../userprofile.model';
import {StateService} from '@uirouter/angular';
import {TranstoastService} from '../../../common/transtoast.service';

@Injectable()
export class SessionService extends BaseHttpService {

  /**
   * The currently logged in User.
   *
   * @type {BehaviorSubject<User>}
   */
  private user: BehaviorSubject<User> = new BehaviorSubject<User>(null);

  /**
   * Observable representation of the logged in User.
   *
   * @type {Observable<T>}
   */
  public user$ = this.user.asObservable();

  /**
   * The current UserProfile of the currently logged in User.
   * @type {BehaviorSubject<UserProfile>}
   */
  private profile: BehaviorSubject<UserProfile> = new BehaviorSubject<UserProfile>(null);

  /**
   * Observable representation of the Users UsersProfile.
   *
   * @type {Observable<T>}
   */
  public profile$ = this.profile.asObservable();

  constructor(protected http: Http,
              protected toastr: TranstoastService,
              private stateService: StateService) {
    super(http, toastr);
    this.refreshToken();
    this.getUserData();
  }

  private refreshToken() {
    // refresh with native JS to get syncronous code
    const oldToken = localStorage.getItem('session_token');
    if (oldToken) {

      const endpoint = this.endpoint('/user/session?session_token=' + oldToken);
      let xmlHttp = new XMLHttpRequest();
      xmlHttp.open('PUT', endpoint, false);
      xmlHttp.setRequestHeader('Content-Type', 'text/plain');
      xmlHttp.onreadystatechange = () => {
        if (xmlHttp.readyState == XMLHttpRequest.DONE) {
          const response = JSON.parse(xmlHttp.responseText);
          this.setToken(response.session_token);
        }
      };
      xmlHttp.send();
    }
  }

  /**
   * If there is a user, it returns the users true, if not, then false.
   *
   * @returns {boolean}
   */
  public getCurrentLoginStatus() {
    return this.user.getValue() !== null;
  }

  /**
   * Returns the current users data.
   *
   * @returns {User}
   */
  public getCurrentUser() {
    return this.user.getValue();
  }

  /**
   * Setter for the current User.
   *
   * @param user
   */
  private setUser(user: User) {
    this.user.next(user);
  }

  /**
   * GETs the UserProfile of the given User from the API and sets it as _next_ in the BehaviourSubject.
   * @param userId
   */
  public getProfile(userId: number) {

    const q = new Deferred();

    const payload = {
      filter: {
        user_id: userId
      }
    };
    // load the profile
    this.getEntity(UserProfile.entity, payload).subscribe(
      response => {
        if (response.json().resource.length > 0) {
          const profile: UserProfile = UserProfile.fromJson(response.json().resource[0]);
          if (profile) {
            this.profile.next(profile);
            q.resolve();
          }
        } else {

          // there is no profile, create one.
          const profile = new UserProfile(userId);
          this.postEntity(profile.toJson(), UserProfile.entity).subscribe(
            postResponse => {
              const profile = UserProfile.fromJson(postResponse.json().resource[0]);
              this.profile.next(profile);
              q.resolve();
            }
          );
        }
      }
    );

    return q.promise;
  }

  /**
   * Will recover the session if a token is present and the application was reloaded.
   * Will invalidate the token, if it has expired.
   */
  public recoverSession() {

    const token = this.getToken();

    // token present, try to get user and profile from API
    if (token) {
      this.getUserData().then(
        (userId: number) => {
          this.getProfile(userId);
        },
        (rejected) => {
          console.log(rejected);
        });
    }
  }

  /**
   * Without a sessionToken, this method returns null.
   * Otherwise, it GETs the User from the Session on the API.
   * If the GET returns an Error, the user is redirected to the landing-page component.
   * It also sets the token and the user to null.
   *
   * @returns {null}
   */
  public getUserData() {

    const q = new Deferred();

    if (!this.getToken()) {
      // q.reject('');
      return q.promise;
    }

    const endpoint = '/api/v2/user/session';

    this.http
      .get(constants.DREAMFACTORY_INSTANCE_URL + endpoint, this.getDefaultHttpOptions())
      .subscribe((response) => {
        // store the user
        const user = User.fromJson(response.json());
        this.user.next(user);
        q.resolve(user.id);

      }, () => {
        // this.toastr.error(errorResponse.json().error.message);
        this.clearToken();
        this.user.next(null);
        this.profile.next(null);

        this.stateService.go('landing-page');
        this.clearToken();
        q.reject();
      });
    return q.promise;
  }

  /**
   * Logs in the User on the API, GETs the UserProfile and resolves the returned promise in case of success and
   * rejects it, otherwise.
   *
   * @param user
   * @returns {Promise<T>}
   */
  login(user): Promise<any> {

    const d = new Deferred();


    const endpoint = this.endpoint('/user/session');

    this.http.post(
      endpoint, JSON.stringify(user), this.getDefaultHttpOptions()
    ).subscribe((data) => {

      this.localLogin(data);

      this.getProfile(this.user.getValue().id).then(
        () => {
          d.resolve(this.user);
        }
      );


    }, (response) => {
      d.reject(response);

    });

    return d.promise;
  }

  /**
   * Is called after the user has been logged in on the API.
   * It sets the User data and session token in the SessionService for further use.
   *
   * @param loginData
   */
  localLogin(loginData) {
    this.setUser(User.fromJson(loginData.json()));

    localStorage.setItem('userId', loginData.json().id);
    this.setToken(loginData.json().session_token);
    // this.setLoginStatus(true);
  }

  /**
   * Requests a logout of the current User on the API.
   *
   * @returns {Promise<T>}
   */
  logout(): Promise<any> {

    const d = new Deferred();

    const endpoint = '/api/v2/user/session';

    this.http
      .delete(constants.DREAMFACTORY_INSTANCE_URL + endpoint, this.getDefaultHttpOptions())
      .subscribe(
        (response) => {
          this.localLogout();
          d.resolve(response);
        },
        (errorResponse) => {
          this.localLogout();
          d.reject(errorResponse);
        }
      );

    return d.promise;
  }

  /**
   * Gets called after the logout on the API to reset the User, Profile and Token.
   */
  localLogout() {
    this.clearToken();
    // this.setLoginStatus(false);
    this.profile.next(null);
    this.user.next(null);
  }


  /**
   * Issues a password reset request to the API.
   *
   * @param email
   * @returns {Observable<Response>}
   */
  requestNewPassword(email: string): Observable<any> {
    const endpoint = this.endpoint('/user/password?reset=true');
    const options = {
      email: email,
      reset: true
    };
    return this.http.post(endpoint, options, this.getDefaultHttpOptions());

  }

  resetPassword(email: string, code: string, pw1: string, pw2: string): Observable<any> {
    const endpoint = this.endpoint('/user/password');
    const payload = {
      email: email,
      code: code,
      new_password: pw2
    };
    const options = this.getDefaultHttpOptions();
    return this.http.post(endpoint, payload, options);
  }

  /**
   * Resets the password on the API.
   *
   * @param oldpassword
   * @param newpassword
   */
  changePassword(oldpassword: string, newpassword: string) {
    const endpoint = this.endpoint('/user/password');
    const payload = {
      old_password: oldpassword,
      new_password: newpassword
    };
    const options = this.getDefaultHttpOptions();
    this.http.post(endpoint, payload, options).subscribe(
      response => {
        if (response.ok) {
          this.toastr.success('Dein Password wurde geändert.');
        } else {
          this.toastr.error('Ein Fehler ist vorgefallen - mehr können wir zu diesem Zeitpunkt nicht sagen.');
        }
      },
      errorResponse => {
        this.toastr.error(errorResponse.json().error.message);
      }
    );
  }

  /**
   * POSTs a request to activate a new account to the API.
   *
   * @param key
   * @param email
   * @returns {Observable<Response>}
   */
  activateAccount(key: string, email: string, password: string, password_copy: string): Observable<any> {
    const payload = {
      email: email,
      code: key,
      new_password: password,
      verify_password: password_copy
    };
    return this.http.post(this.endpoint('/user/password'), payload, this.getDefaultHttpOptions());
  }
}
