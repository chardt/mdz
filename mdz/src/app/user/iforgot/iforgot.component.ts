import {Component, OnInit} from '@angular/core';
import {SessionService} from '../session/services/session.service';
import {ToastsManager} from 'ng2-toastr';
import {StateService} from '@uirouter/angular';
import {TranstoastService} from '../../common/transtoast.service';

@Component({
  selector: 'app-iforgot',
  templateUrl: './iforgot.component.html',
  styleUrls: ['./iforgot.component.scss', '../session/login/login.component.scss']
})
export class IforgotComponent implements OnInit {

  private email: string = null;
  private received = false;

  constructor(private sessionService: SessionService,
              private toastr: TranstoastService,
              private stateService: StateService) {
  }

  ngOnInit() {
  }

  onReceivedBannerClicked() {
    this.received = false;
    this.stateService.go('login');
  }

  request() {

    if (!this.email) {
      this.toastr.error('Bitte gibt deine E-Mail Adresse an.');
      return;
    }
    this.sessionService.requestNewPassword(this.email).subscribe(
      (response) => {
        const body = response.json();
        if (body.success) {
          // this.toastr.success('Anfrage eingegangen');
          this.received = true;
        }
      },
      (errorResponse) => {
        console.log(errorResponse);
        this.toastr.error(errorResponse.json().error.message);
      }
    );
  }
}
