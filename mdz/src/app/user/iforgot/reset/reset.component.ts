import {Component, OnInit} from '@angular/core';
import {StateService} from '@uirouter/angular';
import {SessionService} from '../../session/services/session.service';
import {TranstoastService} from '../../../common/transtoast.service';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss', '../../session/login/login.component.scss']
})
export class ResetComponent implements OnInit {

  private code: string;
  private email: string;
  private pw1: string;
  private pw2: string;

  constructor(private stateService: StateService,
              private sessionService: SessionService,
              private toastr: TranstoastService) {
  }


  ngOnInit() {
    this.code = this.stateService.params.code;
  }

  save() {

    let ok = true;


    if (!this.pw2) {
      this.toastr.error('Bitte gib dein neues Passwort ein zweites mal ein.');
      ok = false;
    }

    if (!this.pw1) {
      this.toastr.error('Bitte wähle ein neues Passwort.');
      ok = false;
    }

    if (this.pw1 !== this.pw2) {
      this.toastr.error('Die Passwörter stimmen nicht überein.');
      ok = false;
    }

    if (!this.code) {
      this.toastr.error('Es liegt ein Problem vor. Bitte klicke auf den Link der E-Mail zum zurücksetzen deines Passwortes.');
      ok = false;
    }

    if (!this.email) {
      this.toastr.error('Bitte gibt noch die E-Mail Adresse an, welche mit deinem Konto verknüpft ist.');
      ok = false;
    }

    if (!ok) {
      return;
    }

    this.sessionService.resetPassword(this.email, this.code, this.pw1, this.pw2).subscribe(
      response => {
        if (response.ok) {
          this.toastr.success('Dein Password wurde geändert.');
          this.stateService.go('login');
        } else {
          this.toastr.error('Ein Fehler ist vorgefallen - mehr können wir zu diesem Zeitpunkt nicht sagen.');
        }
      },
      errorResponse => {
        this.toastr.error(errorResponse.json().error.message);
      }
    );
  }
}
