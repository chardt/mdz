export class ReportedUser {

  /**
   * API Endpoint.
   *
   * @type {string}
   */
  public static entity = 'reported_user';

  /**
   * Database ID of the Report.
   */
  private id: number;
  // user_id: number;
  // comment_id: number;
  // message: string;

  /**
   * TRUE when the Report is open, resp. not closed.
   */
  private open: boolean;

  /**
   * ID of the Administrator, that closed the Report.
   */
  private closed_by: number;

  /**
   * ID of the User that created the Report.
   */
  private reported_by: number;

  /**
   * Description of how the Issue has been resolved.
   */
  private resolution: string;

  public initiative_id: number;

  /**
   * Factory, that creates a new Report from a JSON object.
   *
   * @param json
   * @returns {ReportedUser}
   */
  public static fromJson(json: any) {
    const ru = new ReportedUser(json.user_id, json.comment_id, json.message);
    if (json.id) {
      ru.id = json.id;
    }
    if (json.open === true) {
      ru.open = true;
    }
    if (json.open === false) {
      ru.open = false;
    }
    if (json.resolution) {
      ru.resolution = json.resolution;
    }
    if (json.reported_by) {
      ru.reported_by = json.reported_by;
    }

    if (json.initiative_id) {
      ru.initiative_id = json.initiative_id;
    }
    return ru;
  }

  /**
   * Factory, that creates either a single Report or an array of Reports from a API response.
   *
   * @param jsonArray
   * @returns {ReportedUser[]}
   */
  public static fromResource(jsonArray: any): ReportedUser[] {
    const users: ReportedUser[] = [];
    for (const user of jsonArray) {
      const u = ReportedUser.fromJson(user);
      users.push(u);
    }
    return users;
  }

  constructor(private user_id: number,
              private comment_id: number,
              private message: number,
              id?: number,
              initiative_id?: number) {
    if (id) {
      this.id = id;
    }
    if(initiative_id){
      this.initiative_id = initiative_id;
    }
  }

  /**
   * Creates a JSON object that reflects the Reports data.
   * Either in String from or Object form.
   *
   * @param stringify
   * @returns {any}
   */
  public toJson(stringify?: boolean) {

    const json = {
      id: this.id,
      user_id: this.user_id,
      comment_id: this.comment_id,
      message: this.message,
      open: this.open,
      closed_by: this.closed_by,
      reported_by: this.reported_by,
      resolution: this.resolution,
      initiative_id: this.initiative_id
    };

    if (stringify) {
      return JSON.stringify(json);
    } else {
      return json;
    }
  }

  /**
   * Setter for the Reporter ID.
   *
   * @param id
   */
  setReporter(id: number) {
    this.reported_by = id;
  }

  /**
   * Setter for the Report ID.
   *
   * @param id
   */
  setId(id: number) {
    this.id = id;
  }

  /**
   * Getter for the Report ID.
   *
   * @returns {number}
   */
  getId() {
    return this.id;
  }

  /**
   * Sets the open flag to "open".
   */
  setOpen() {
    this.open = true;
  }

  /**
   * Sets the open flag to "closed".
   */
  setClosed() {
    this.open = false;
  }

  /**
   * Closes the issue with a definitive resolution.
   * TODO: Warn when no resolution was set.
   *
   * @param admin_id
   * @param resolution
   */
  close(admin_id: number, resolution: string) {
    this.closed_by = admin_id;
    this.resolution = resolution;
  }

  /**
   * Returns the ID of the reported User.
   *
   * @returns {number}
   */
  public getUserId() {
    return this.user_id;
  }

  /**
   * Returns the ID of the reporting User.
   *
   * @returns {number}
   */
  public getReporterId() {
    return this.reported_by;
  }

  /**
   * Returns the ID of the reported comment.
   *
   * @returns {number}
   */
  public getCommentId() {
    return this.comment_id;
  }

}
