import {Component, Input, OnInit} from '@angular/core';
import {SessionService} from "../../user/session/services/session.service";

@Component({
  selector: 'landingpage-participation',
  templateUrl: './participation.component.html',
  styleUrls: ['./participation.component.scss']
})
export class ParticipationComponent implements OnInit {

  /**
   * Controls whether the participation block should be displayed in default or small variation.
   */
  @Input() small: boolean;

  /**
   * True, if the user is logged in.
   * @type {boolean}
   */
  private loggedIn = false;

  constructor(private sessionService: SessionService) { }

  ngOnInit(){
    this.sessionService.user$.subscribe(
      user => {
        if(user){
          this.loggedIn = true;
        }
      }
    )
  }
}
