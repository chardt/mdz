import {Component, OnInit} from '@angular/core';
import {LandingPage} from "../admin/admin-landingpage/landingpage.model";
import {LandingpageService} from "../admin/admin-landingpage/landingpage.service";

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss'],
  providers: [LandingpageService]
})
export class LandingPageComponent implements OnInit{

  /**
   * The LandingPage model
   */
  private landingPage: LandingPage;

  constructor(private landingpageService: LandingpageService) {
  }

  /**
   * GETs the LandingPage on init.
   * Also GETs the Image for the quote.
   */
  ngOnInit() {
    this.landingpageService.getEntity(LandingPage.entity).subscribe(
      response => {
        const r = response.json();
        this.landingPage = LandingPage.fromJson(r);
      }
    );
  }
}
