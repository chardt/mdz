import {Component, Input} from '@angular/core';
import {Initiative} from '../../../initiative/model/initiative.model';
import {DomSanitizer} from '@angular/platform-browser';
import {DREAMFACTORY_API_KEY, DREAMFACTORY_INSTANCE_URL} from '../../../config/constants';
import {StateService} from '@uirouter/angular';

@Component({
  selector: 'landingpage-featured-initiative',
  templateUrl: './featured-initiative.component.html',
  styleUrls: ['./featured-initiative.component.scss']
})
export class FeaturedInitiativeComponent {

  /**
   * The featured Initiative itself
   */
  @Input() initiative: Initiative;

  constructor(private sanitizer: DomSanitizer,
              private state: StateService) {
  }

  /**
   * Bypasses HTML so it can be displayed from inside a variable
   * @param html
   * @returns {SafeHtml}
   */
  public okHtml(html: string) {
    return this.sanitizer.bypassSecurityTrustHtml(html);
  }


  private fullPathFromPath(path: string, notForCss?: boolean) {
    let fullPath =
      DREAMFACTORY_INSTANCE_URL + '/api/v2/files/' +
      path + '?api_key=' + DREAMFACTORY_API_KEY; // + '&session_token=' + this.sessionService.getToken();
    if (!notForCss) {
      fullPath = 'url(' + fullPath + ')';
    }
    return fullPath;
  }

  path(path: string) {
    return this.sanitizer.bypassSecurityTrustStyle(this.fullPathFromPath(path));
  }

  open() {
    this.state.go('show', {initiativeId: this.initiative.id});
  }
}
