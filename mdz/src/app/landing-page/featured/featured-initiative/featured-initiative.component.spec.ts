import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturedInitiativeComponent } from './featured-initiative.component';

describe('FeaturedInitiativeComponent', () => {
  let component: FeaturedInitiativeComponent;
  let fixture: ComponentFixture<FeaturedInitiativeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeaturedInitiativeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturedInitiativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
