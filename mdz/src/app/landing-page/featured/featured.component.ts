import {Component, OnInit} from '@angular/core';
import {InitiativeService} from '../../initiative/services/initiative.service';
import {Initiative} from '../../initiative/model/initiative.model';

@Component({
  selector: 'landingpage-featured',
  templateUrl: './featured.component.html',
  styleUrls: ['./featured.component.scss'],
  providers: [InitiativeService]
})
export class FeaturedComponent implements OnInit {

  /**
   * Controls the disply state (Full or small).
   *
   * @type {boolean}
   */
  show = false;

  /**
   * Amount of characters to be left when a string is truncated.
   * @type {number}
   */
  private truncateLength = 800;

  /**
   * The Initiatives to display.
   *
   * @type {Array}
   */
  private initiatives: Initiative[] = [];

  constructor(private initiativeService: InitiativeService) {
  }

  /**
   * Loads the featured Initiatives from the Initiative Service and pushes
   * then to the initiatives Array.
   */
  ngOnInit() {
    this.initiativeService.getFeatured().subscribe(
      response => {
        const r = response.json().resource;
        for (const i of r) {
          const ini = Initiative.fromJson(i);
          ini.description = this.truncateDescription(ini.description);
          this.initiatives.push(ini);
        }
      }
    );
  }

  /**
   * Strips the given String to a maximum of 800 characters.
   * If stripped, '...' are appended.
   *
   * @param text
   * @returns {string}
   */
  private truncateDescription(text: string) {
    if (text.length > this.truncateLength) {
      text = text.substring(0, this.truncateLength) + ' ...';
    }
    return text;
  }

  /**
   * Toggles the "show" flag.
   */
  toggleShow() {
    this.show = !this.show;
  }
}
