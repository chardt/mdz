import {Component, Input} from '@angular/core';
import {LandingPage} from "../../admin/admin-landingpage/landingpage.model";

@Component({
  selector: 'landingpage-gero',
  templateUrl: './gero.component.html',
  styleUrls: ['./gero.component.scss']
})
export class GeroComponent {

  /**
   * The LandingPage model
   */
  @Input() landingPage: LandingPage;

  constructor() { }

}
