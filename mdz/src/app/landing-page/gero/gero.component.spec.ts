import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeroComponent } from './gero.component';

describe('GeroComponent', () => {
  let component: GeroComponent;
  let fixture: ComponentFixture<GeroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
