import {Component, Input, OnInit} from '@angular/core';
import {LandingpageService} from '../../admin/admin-landingpage/landingpage.service';
import {LandingPage} from '../../admin/admin-landingpage/landingpage.model';
import {DREAMFACTORY_API_KEY} from '../../config/constants';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'landingpage-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.scss']
})
export class QuotesComponent {

  /**
   * The LandingPage model
   */
  @Input() landingPage: LandingPage;

  /**
   * URI of an image that is used as the cite image
   */
  private citerImagePath: string;

  constructor(private domSanitizer: DomSanitizer,
              private landingpageService: LandingpageService) {
  }

  /**
   * GETs the LandingPage on init.
   * Also GETs the Image for the quote.
   */
  ngOnInit() {
    this.getCiterImage();
  }

  /**
   * Bypasses CSS security.
   *
   * @param path
   * @returns {SafeStyle}
   */
  path(path: string) {
    return this.domSanitizer.bypassSecurityTrustStyle('url(' + path + ')');
  }

  /**
   * Gets the LandingPage image filename and composes the absolute URI to the image.
   */
  getCiterImage() {
    this.landingpageService.getEntity('/files/landingpage/', null, true).subscribe(
      response => {
        const image = response.json().resource[0];
        this.citerImagePath = this.landingpageService.endpoint('/files/' + image.path) + '?api_key=' + DREAMFACTORY_API_KEY
        +'&_ts='+Date.now().toString();
      }
    );
  }

}
