import {Component, Input} from '@angular/core';
import {LandingPage} from "../../admin/admin-landingpage/landingpage.model";

@Component({
  selector: 'landingpage-explanation',
  templateUrl: './explanation.component.html',
  styleUrls: ['./explanation.component.scss']
})
export class ExplanationComponent {

  /**
   * The LandingPage model
   */
  @Input() landingPage: LandingPage;

  constructor() {
  }
}
