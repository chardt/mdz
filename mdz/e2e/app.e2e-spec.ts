import { MdzPage } from './app.po';

describe('mdz App', () => {
  let page: MdzPage;

  beforeEach(() => {
    page = new MdzPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
